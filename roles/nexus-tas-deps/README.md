Role Name
=========

Install service component modules, 3rd parties components. 

Requirements
------------

 * 

Role Variables
--------------

 
Dependencies
------------

 * none

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: nexus-tas-deps

           

License
-------

Quasar Software Research (c) 2018


