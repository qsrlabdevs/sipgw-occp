Role Name
=========

It installs TAS service component 

Requirements
------------

 * 

Role Variables
--------------

* nexus_install_path - specify path where package is deployed /opt/quasar
 
Dependencies
------------

 * none

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: nexus-tas-sipservice

           

License
-------

Quasar Software Research (c) 2018


