Role Name
=========

It installs SipService component on TAS. 

Requirements
------------

 * 

Role Variables
--------------

 * redis_url: specifies redis queuedb database
 * redis_queue: specifies egress_queue used by camel service for sending events
 * redis_pool_size: specifies redis pool size
 * svc_file: specifies service ear file which will be deployed on TAS server
 * sip_ip: specifies SIP IP used by sip_provider that will be configured on vTAS
 * sip_port: specifies SIP port used by sip_provider that will be configured on vTAS
 * svc_file: specifies the SipService ear file
 * svc_link: specifies SipService.ear sym link file, default SipService.ear
 * imscapp_file: specifies SipService component configuration file needed by vTAS 
 * icui_file: specifies file that contains SIP service announcements definition - SipService.xml
 * instance_id: specifies see instances that host the SipService
 * mrf_url: specifies MRF URI - sip:msml@mydomain.com;transport=tcp;lr 
 
Dependencies
------------

 * none

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: sip-servicelogic
           redis_url: redis://redisip:26380/queuedb
           redis_queue: sip_events
           redis_pool_size: 10
           svc_file: sip-servicelogic.ear
           svc_link: SipService.ear
           imscapp_file: imscapp-SipService.properties
           icui_file: SipService.xml
           instance_id: 2
           mrf_url: sip:msml@mydomain.com;transport=tcp;lr

           

License
-------

Quasar Software Research (c) 2018


