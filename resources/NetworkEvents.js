var OCCPSIP;
(function (OCCPSIP) {
    var Capabilities;
    (function (Capabilities) {
        Capabilities["REL1XX"] = "REL1XX";
        Capabilities["FORKING"] = "FORKING";
        Capabilities["PRECONDITION"] = "PRECONDITION";
        Capabilities["PEM"] = "PEM";
        Capabilities["UPDATE"] = "UPDATE";
    })(Capabilities || (Capabilities = {}));
    var Annotype;
    (function (Annotype) {
        Annotype["CONNECT"] = "CONNECT";
        Annotype["RINGING"] = "RING";
    })(Annotype || (Annotype = {}));
    var CallPollActionType;
    (function (CallPollActionType) {
        CallPollActionType["Accept"] = "accept";
        CallPollActionType["Forward"] = "forward";
        CallPollActionType["Reject"] = "reject";
    })(CallPollActionType || (CallPollActionType = {}));
    var CallStartActionType;
    (function (CallStartActionType) {
        CallStartActionType[CallStartActionType["Abort"] = 0] = "Abort";
        CallStartActionType[CallStartActionType["Forward"] = 1] = "Forward";
        CallStartActionType[CallStartActionType["RejectMrf"] = 2] = "RejectMrf";
    })(CallStartActionType || (CallStartActionType = {}));
    var MediaOperationActionType;
    (function (MediaOperationActionType) {
        MediaOperationActionType["PerformMediaOperation"] = "performMediaOperation";
    })(MediaOperationActionType || (MediaOperationActionType = {}));
    var MediaOperationContentType;
    (function (MediaOperationContentType) {
        MediaOperationContentType["MSML"] = "application/msml+xml";
    })(MediaOperationContentType || (MediaOperationContentType = {}));
})(OCCPSIP || (OCCPSIP = {}));
