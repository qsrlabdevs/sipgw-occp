
namespace Responses {
  export interface ResultCode {
    resultCode : string ;
  }
}

namespace HTTP{

    export interface Request {
        uri: string;
        method: string;
        headers: any;
        body: any;
    }

    export interface Response{
        status: string;
        headers: any;
        body: any;
    }
}

namespace OCCP {
  export interface AddressParams {
    presentation?: string ;
    screening? : string ;
    "number-qualifier-ind" : string ;
    "network-context" : string ;
  }
  export interface Address {
    scheme : string ;
    params: AddressParams;
    user: string;
  }

  export interface PlayPrompt {
      media : string ;
      lang : string ;
  }

  export interface PromptCollect {
      media : string ;
      lang : string ;
  }

  export interface ReleaseLeg {
      cause : string ;
  }

  type PlayType = PlayPrompt | PromptCollect;

  export interface LegAction {
      type:number ,
      legaction : string ;
      playPrompt? : PlayPrompt;
      promptCollect? : PromptCollect;
      release? : ReleaseLeg;
    }

    export interface Duration {
        amount : number;
        final : number;
    }


    export interface Forward {
    calling_number?:Address ;
    type:number ;
    uri:Address;
    callingPartyCategory?: number;
    duration? : Duration;
    requestedInformation? : [string];
    fci : string ;
  }



  export interface Action {
      callid : string ,
      as : string ,
      queue : string ,
      eventime: number ,
      starttime : number ,
      id : number ,
      eventname : string ,
      action : LegAction
    }

    export interface CollectComplete {
      collectedDigits: string ;
      cause : string;
    }

    export interface ChargingUnit {
      tariffTimeChange: number ;
      amount: number ;
      unit: string ;
      tariffChangeUsage: string ;
      direction: string ;
      reportingReason?: string ;
    }
    export interface Charging {
      usedUnits? : ChargingUnit;
    }

    export interface CS {
      lastNetworkEvent:number;
      tcapEndMode: string ;
      disconnectedFlag: boolean ;
      extnTeleServiceCode: string ;
      rawBearerCapability: string ;
      rawRedirectionInformation: string ;
      rawReleaseCallCause: string ;
      imsi: string ;
      connectedCallingNumber:Address;
      connectedNumber:Address;
    }

    export interface Common {
      networkTimeZoneOffSet: number ;
      callAttemptTime: number ;
      networkCallType: string ;
      redirectingParty: Address ;
      mediaType: string ;
      callReleaseTime: number ;
      serviceKey: string ;
      connectionTimeout: number ;
      callConnectedTime: number ;
    }

    export interface Camel {
      charging : Charging;
      cs : CS ;
      common : Common;
      sccpCdPa: Address ;
      sccpCgPa : Address ;
      cgPaCategory: number ;
      callReferenceNumber: string ;
      highLayerCharacteristics: number ;
      gsmFwdPending: boolean;
      redirectReason:string ;
      eventTypeBCSM: number ;
      mscAddress: Address ;
      origCalledPartyID: Address ;
      bearerServiceCode : number ;
      locationNumber : Address ;
      networkProtocol : string ;
    }

    export interface Event {
      name : string ;
      id : number ;
      collectComplete? : CollectComplete;
    }


}


namespace OCCPSIP {


  export interface Leg {
    address : string;
    name : string ;
  }
  export interface Call {
    state : number
  }
  enum Capabilities {
    REL1XX = "REL1XX",
    FORKING= "FORKING",
    PRECONDITION="PRECONDITION",
    PEM="PEM",
    UPDATE="UPDATE"
  }
  export interface Message {
    method : [string];
    type : [string];
    body : [string] ;
  }
  export interface SIP {
    capabilities : [Capabilities];
    message : Message;
    From : From ;
    To : To ;
    "R-URI": any;
    "P-Asserted-Identity" : PAI;
    "P-Charging-Vector" : PCV;
    "P-Early-Media" : PEM;
    "Call-ID" : Header;
    Allow : Allow;
    Route: [Route];
    Supported: Supported;
    CSeq : CSeq;
    content : Content;

  }

  export interface CallStart {
    contact : string;
    cause: string;
    leg : string ;
  }

  export interface CallPoll {
    name: string;
    type: string;
    leg: string ;
  }

  export interface Event {
    name : string;
    callStart? : CallStart;
    callPoll? : CallPoll ;
  }


  export interface OCCPEvent {
    callid : string;
    call : Call;
    as : string;
    eventtime : number;
    SIP : SIP;
    event: Event ;
  }

  interface Events {
    SuccessResponsePollEvent? : string;
    RawContentPollEvent? : string;
    InfoPollEvent?: string
  }
  /**
    Define header variables used by application
  */
  interface HeaderVars {
    disableSendDefaultReason? : string;
    disableSendNoAnswerReason? : string
  }

  enum Annotype {
    CONNECT = "CONNECT",
    RINGING = "RING"
  }

  interface RingingTone {
    anno_name : string;
    anno_type : Annotype
  }

  interface Session {
    log : any;
    inCapabilities : [Capabilities];
    outCapabilities? : string;
    events? : string;
    headerrulevar? : string;
    headerrulesselect? : string;
    ringingtones? : string;
    sendAction? : string ;
    SIPHelper : any;
    SIPInitialInvite? : any;
    SIPMessage? : any;
    SIPMessageType? : any
  }


  enum CallPollActionType {
   Accept = "accept",
   Forward = "forward",
   Reject = "reject",
  }

  enum CallStartActionType {
   Abort = 0,
   Forward = 1,
   RejectMrf = 2
  }



  /**
   Set action for CallStart event
  */
  interface CallStartAction {
   type : CallStartActionType;
   errorcode : number;
   cause : string ;
   uri : string;
   earlymedia : number;
   legname : string
  }

  /**
   Set action for CallPoll event
  */
  interface CallPollAction {
   type : CallPollActionType
  }


  /**
   Application can set action for a specific leg, this is applicable for MRF contact
   */
  interface LegAction {
   type : CallStartActionType ;
   legaction : string
  }

  enum MediaOperationActionType {
   PerformMediaOperation = "performMediaOperation"
  }

  enum MediaOperationContentType {
   MSML = "application/msml+xml"
  }

  export interface PerformMediaOperation {
   ContentType : MediaOperationContentType;
   Content : string
  }

  export interface MediaOperationAction {
   type : number;
   legaction : MediaOperationActionType;
   performMediaOperation : PerformMediaOperation
  }

  export interface Action {
   action : any
  }


  export interface Header {
   header : string;
   value : string;
  }

  export interface QHeader {
   Privacy : string;
   Reason : string ;
  }

  export interface Parameter {
   transport : string;
   user : string ;
   lr : string ;
   type : string ;
   origdlgid : string;
   orig : string ;
  }

  export interface URI {
   scheme : string;
   authority : string ;
   gr : string ;
   host: string ;
   lr : string ;
   maddr : string ;
   port : number  ;
   user : string ;
   parameters : Parameter;
   qheader : QHeader;
  }

  export interface Address {
   displayName : string;
   uri : URI
  }

  export interface HistoryInfo implements Header {
   address : Address;
   index : string
  }

  export interface PAI implements Header  {
   address : Address
  }

  export interface From implements Header {
   address : Address;
   tag : string ;
  }

  export interface To implements Header {
   address : Address;
   tag : string;
  }

  export interface PCV implements Header {
   icid_generated_at: string ;
   icid_value : string ;
   oaid : string;
   taid : string;
   osid : string;
   tsid : string;
  }

  export interface Route implements Header {
   address : Address;
  }

  export interface RequireHeader {
    header : string ;
    value : [string];
  }

  export interface MSMLPlayPrompt {
      dialogstart:{
        name:string ;
        target:string  ;
        type: string ;
        play:{
          interval:string ;
          iterate: string ;
          cleardb: string ;
          maxtime : string ;
          barge: string ;
          audio:{
            uri:string ;
          }
          playexit:{
            exit:{
              namelist:string ;
            }
          }
        }
        dtmf:{
          cleardb:string ;
          fdt:string ;
          idt: string ;
          pattern:{
            digits:string ;
          }
          noinput:any;
          nomatch:any ;
          dtmfexit:{
            exit:{
              namelist:string ;
            }
          }
        }
      }
      version:string;
    }

    export interface CSeq {
      method: string ;
      header: string ;
      seqNumber : number ;
      value : number ;
    }

    export interface Supported {
      header : string ;
      value : [string];
    }

    export interface Content {
      "Content-Type" : string ;
      "Content-SubType" : string ;
      json : SDP;
    }
    export interface SDP {
      session : string ;
      media : [any];
    }
    export interface Allow {
      header : string ;
      value : [string] ;
    }
    export interface PEM {
      header : string ;
      value : string ;
    }




}

namespace SIPSIM {
  export interface SIPMessage  {
    status : [string];
    method : [string];
    type : [string] ;
    body : [string] ;
  }
  export interface SIP {
    message : SIPMessage;
  }

  export interface SIPSIMEvent {
    callid : string ;
    eventid : number ;
    "ua-type" : string ;
    session : string ;
    "event-type" : string ;
    "event-name" : string ;
    "ua-mode" : string ;
    type : string ;
    queue : string ;
    direction : string ;
    SIP : any;
  }



}
