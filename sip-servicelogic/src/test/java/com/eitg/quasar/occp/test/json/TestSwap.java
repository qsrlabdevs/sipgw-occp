package com.eitg.quasar.occp.test.json;

import com.eitg.quasar.occp.sip.headers.XHeader;
import com.eitg.quasar.occp.sip.headers.XHeaderParser;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.TerminalContact;
import org.junit.Test;
import javax.sip.SipFactory;
import javax.sip.header.Header;
import javax.sip.header.HeaderFactory;
import java.time.Instant;
import java.util.Map;

public class TestSwap {

	class TestClass {
		public TestClass ref;
		public String property;
	}
	
	public TestClass swap(TestClass c1, TestClass c2){		
		return c1;
	}
	
//	@Test
	public void testSwap(){
		TestClass inst1, inst2;
		inst1 = new TestClass();
		inst2= new TestClass();
		inst1.property="value1";

		inst2.property="value2";
		inst2.ref=inst1;

		System.out.println("inst1: ["+inst1.property+" ref:"+inst1.ref+"] inst2: ["+ inst2.property+" ref:"+inst2.ref+"]");

		inst1=swap(inst2, (inst2=inst1));

		System.out.println("inst1: ["+inst1.property+" ref:"+inst1.ref+"] inst2: ["+ inst2.property+" ref:"+inst2.ref+"]");


	}

	@Test
	public void testUri(){
//		String uri="sip:+123@mama.com,user=phone";
//		Address address = new Address(uri);
//		TerminalContact termContact = new TerminalContact(address);
//		System.out.println("new contact:"+termContact.toString());

//		String mytext ="   mama  asas tata		\nlala a" ;
//		String trimmed = mytext.trim().replaceAll("\\s+"," ");
//		//trimmed = trimmed.replaceAll(" +"," ");
//
//		System.out.println("trimmed:["+trimmed+"]");


		try {
			SipFactory factory = SipFactory.getInstance();
			HeaderFactory hf = factory.createHeaderFactory();
			Header myHeader = hf.createHeader("X-VTAS-VPN", "mama;cgn=tata");

			System.out.println(myHeader);
		} catch (Exception e){
			e.printStackTrace();
		}

//		String str = "mmtel.b52-t.ims.mnc014.mcc232.3gppnetwork.org;641726715;1309;577";
//		System.out.println(str.replaceAll("\\.|-|;",""));
	}

	@Test
	public void check2009(){

		//#{java.time.Instant.now().toEpochMilli()+500}

		String orig="sip.sessionStart.OPTIONS";
		String pattern="(.*)OPTIONS(.*)";
		System.out.println(orig.matches(pattern));

	}

	@Test
	public void testParseSIPHeader(){
		try {
			String headerValue = "rrsi;cdn=\"436608795107\";uri=\"sip:+436608795107@ims.mnc014.mcc232.3gppnetwork.org\";sdba=\"ActKey=FIX|MOB_OWN|POSTPAID,app=sdbAction,dc=apd\";a=\"1010\";m=\"EXACT-MATCH\";ported=\"00\";ann=\"nixtralala\";vms=\"null\";reason=\"null\";info=\"null\";tl=\"MARC\";tkgactio=\"null\";barringzone=\"null\"";
			//String headerValue="vvpn;cdn=\"436608795107\";vid=\"null\";cvpn=\"null\";vd=\"null\";vt=\"null\";vb=\"null\";cbar=\"null\"";
			String headerName = "X-VTAS-RSI";
			XHeaderParser xHeaderParser = new XHeaderParser(headerValue);
			xHeaderParser.setHeaderName(headerName);
			XHeader xHeader = (XHeader) xHeaderParser.parse();
			Map<String,Object> hdr = SIPJsonFactory.getHeader(xHeader);
			System.out.println("Header:"+hdr);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
