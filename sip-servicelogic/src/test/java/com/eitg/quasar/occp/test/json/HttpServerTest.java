package com.eitg.quasar.occp.test.json;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class HttpServerTest {

    private static final String url="http://localhost:3000";

    private static final String lpushEvent = "{\"callid\":\"tas-apps_4;1-2229933@172.31.11.142\",\"call\":{\"legs\":[{\"address\":\"<Bparty> tel\n" +
            ":+972000001\",\"name\":\"Uac\"}],\"state\":\"START\"},\"as\":\"tas-apps_4\",\"session\":\"tas-apps_4;1-2229933@172.31.11.142\",\"event-type\":\"sip\",\"event-name\":\"sip.callStart.DISCONNECTED\",\"eventtime\":1702904680366,\"starttime\":1702904680366,\"event\":{\"name\":\"callStart\",\"ca\n" +
            "llStart\":{\"contact\":\"<Bparty> tel:+972000001\",\"cause\":\"DISCONNECTED\",\"leg\":\"<Bparty> tel:+972000001\"},\"id\":4},\"queue\":\"tas-apps_4\",\"timestamp\":1702904680366,\"events-stack\":[\"sip.callStart.NONE\",\"sip.callEarlyMediaAnswered.SipCallLegUaClient\",\"sip.callEar\n" +
            "lyMediaAnswered.SipCallLegUaClient\",\"sip.callAnswered.SipCallLegUaClient\",\"sip.callStart.DISCONNECTED\"]}";

    @Test
    public void getDate(){
        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        simpleDateFormat.format(new Date());

        System.out.println("Data:"+(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")).format(new Date()));


    }

    @Test
    public void startHttpPostClient() throws Exception {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100);
        cm.setDefaultMaxPerRoute(20);

        // Create an HttpClient with the connection manager
        try (CloseableHttpClient httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .build()) {

            // JSON Mapper
            // Send multiple POST requests
            for (int i = 0; i < 1000; i++) {
                HttpPost postRequest = new HttpPost(url);

                // Create JSON payload
                String jsonPayload = lpushEvent;

                // Set JSON payload
                StringEntity requestEntity = new StringEntity(jsonPayload, "UTF-8");
                requestEntity.setContentType("application/json");
                postRequest.setEntity(requestEntity);

                try (CloseableHttpResponse response = httpClient.execute(postRequest)) {
                    // Handle the response
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        String result = EntityUtils.toString(entity);
                        System.out.println("Response: " + result);
                    }
                }

                // Sleep to limit the request rate
                Thread.sleep(1000);
            }
        }
    }



    @Test
    public void startHttpClient() throws Exception {

        // Create a connection manager with custom configuration
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100); // Set max total connections
        cm.setDefaultMaxPerRoute(20); // Set max connections per route

        // Create an HttpClient with the connection manager
        try (CloseableHttpClient httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .build()) {

            // Run multiple requests
            for (int i = 0; i < 1000000; i++) {
                HttpGet request = new HttpGet("http://localhost:3000");

                try (CloseableHttpResponse response = httpClient.execute(request)) {
                    // Handle the response
                    HttpEntity entity = response.getEntity();
                    if(entity!=null) {
                        //String result = EntityUtils.toString(entity);
                        System.out.println("Response: " + entity.toString());
                    }
                }

                // Sleep for a short duration to simulate multiple requests per second
                Thread.sleep(200); // Adjust this to control the request rate
            }
        }
    }
        // Create a custom executor to manage threads
//        ExecutorService executor = Executors.newFixedThreadPool(10);
//
//        // Create an HttpClient with the custom executor
//        HttpClient httpClient = HttpClient.newBuilder()
//                .executor(executor)
//                .build();
//
//        // Prepare the request
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create("http://example.com"))
//                .build();
//
//        // Send multiple requests
//        for (int i = 0; i < 1000; i++) {
//            httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
//                    .thenAccept(response -> {
//                        System.out.println("Response: " + response.body());
//                    });
//
//            // Sleep to throttle the request rate
//            Thread.sleep(1000); // Adjust to control the request rate
//        }
//
//        // Shutdown the executor
//        executor.shutdown();
//        if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
//            executor.shutdownNow();
//        }
//    }

    @Test
    public void startHttpServer(){
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(3000), 0);
            server.createContext("/", new MyHandler());
            server.setExecutor(Executors.newCachedThreadPool());
            server.start();
            while(true){
                Thread.sleep(1000);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        String[] events =new java.lang.String[] {"uri1","uri2"} ;
    }



    static class MyHandler implements HttpHandler {
        public static String readStringFromInputStream(InputStream inputStream) {
            String result = "";
            try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
                result = br.lines().collect(Collectors.joining(System.lineSeparator()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "Hello world!";


            System.out.println("Exchange request:"+t.getRequestMethod()+" body:"+t.getRequestBody().toString());
            System.out.println("Body:"+readStringFromInputStream(t.getRequestBody()));
            t.sendResponseHeaders(200, response.length());
            t.getResponseHeaders()
                    .add("Content-Type","application/json");
            OutputStream os = t.getResponseBody();
            os.write(lpushEvent.getBytes());
            os.close();
        }
    }
}
