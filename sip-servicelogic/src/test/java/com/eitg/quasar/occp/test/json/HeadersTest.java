package com.eitg.quasar.occp.test.json;

import com.eitg.quasar.occp.sip.headers.XHeader;
import com.eitg.quasar.occp.sip.headers.XHeaderParser;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import org.junit.Test;

import javax.sip.SipStack;
import javax.sip.header.Header;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HeadersTest {

    @Test
    public void testDate(){

    }

    @Test
    public void testHistoryInfo(){
        String histInfo="<sip:+436609709808@ims.mnc005.mcc232.3gppnetwork.org>;index=1,<sip:+43660303060@ims.mnc005.mcc232.3gppnetwork.org;user=phone;cause=302>;index=1.1";


        System.out.println(histInfo.contains("sip"));
        try {

            Header header = SipServiceImslet.getInstance().getHeaderFactory().createHeader((String) "History-Info", histInfo);
            System.out.println("Test:"+header);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void testArray(){
        List<String> myVals = new LinkedList<>();
        myVals.add("mamama");
        String [] s = new String[myVals.size()];
        myVals.toArray(s);
        System.out.println(s);
    }

    @Test
    public void displayAnnos(){
        String annoTest = "{\"annid\":\"annoPromptCollect\"," +
                "\"interval\":100," +
                "\"iterate\":1," +
                "\"cleardb\":1," +
                "\"maxtime\":1500," +
                "\"barge\":1," +
                "\"path\":\"file:///tmp/annos/\"," +
                "\"prompt\":\"testAnno.wav\"," +
                "\"variable\":\"\"," +
                "\"dtmf\":{" +
                "\"cleardb\":1,"+
                "\"idt\":1," +
                "\"fdt\":1," +
                "\"min\":1," +
                "\"max\":1," +
                "\"rtf\":\"*\"," +
                "\"cancel\":\"#\"" +
                "}" +
                "}" ;

        String annoTest2 = "{\"annid\":\"annoTest\"," +
                "\"interval\":100," +
                "\"iterate\":1," +
                "\"cleardb\":1," +
                "\"maxtime\":1500," +
                "\"barge\":\"1\"," +
                "\"path\":\"file:///tmp/annos/\"," +
                "\"prompt\":\"testAnno.wav\"," +
                "\"variable\":\"\"" +
                "\"dtmf\":{" +
                "\"cleardb\":1,"+
                "\"idt\":1," +
                "\"fdt\":1," +
                "\"min\":1," +
                "\"max\":1," +
                "\"rtf\":\"*\"" +
                "\"cancel\":\"#\"" +
                "}" +
                "}" ;
        System.out.println(annoTest);
    }


    @Test
    public void testParseHeader() {
        String headerValue = "value,,,...:asas:asa:as";
        //String headerValue="7zs4rm3;id=agwvUCUlQXc0qD8TL95MQGsC";
        String headerName = "P-Test";
        XHeaderParser xHeaderParser = new XHeaderParser(headerValue);
        xHeaderParser.setHeaderName(headerName);
        try{
            XHeader xHeader = (XHeader) xHeaderParser.parse();
            Map<String, Object> hdr = SIPJsonFactory.getHeader(xHeader);
            System.out.println("hdr:"+hdr);
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
