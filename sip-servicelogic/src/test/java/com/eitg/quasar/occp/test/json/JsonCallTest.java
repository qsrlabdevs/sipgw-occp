package com.eitg.quasar.occp.test.json;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import javax.sip.PeerUnavailableException;
import javax.sip.SipFactory;
import javax.sip.address.SipURI;
import javax.sip.header.FromHeader;
import javax.sip.header.Header;
import javax.sip.header.HeaderFactory;


import com.eitg.quasar.nexus.middleware.typescript.libraries.Loader;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import gov.nist.javax.sip.header.From;
import gov.nist.javax.sip.header.HeaderFactoryImpl;
import gov.nist.javax.sip.header.SIPHeader;
import gov.nist.javax.sip.parser.FromParser;
import org.junit.Test;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.utils.JsonSCIFRequest;
import com.eitg.quasar.occp.utils.JsonSCIFResponse;
import com.eitg.quasar.occp.utils.JsonSCIFResponseCallStart;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.ImscException;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.imscapi.imslet.ImsletContext;
import com.hp.opencall.imscapi.imslet.ImsletTimer;
import com.hp.opencall.imscapi.imslet.ProtocolSession;
import com.hp.opencall.ngin.cdrlog.Cdr;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.CallUser;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.InformationalEvent;
import com.hp.opencall.ngin.scif.MessageInterceptor;
import com.hp.opencall.ngin.scif.ParameterSet;
import com.hp.opencall.ngin.scif.ScifEvent;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.TerminalCallLeg;
import com.hp.opencall.ngin.scif.TerminalContact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;

import gov.nist.javax.sip.message.SIPRequest;



public class JsonCallTest {

	private SipFactory sipFactory;
	
	public JsonCallTest(){
		sipFactory = SipFactory.getInstance();
	}
	
	private String getInitialINVITE(){
		String ret = "INVITE sip:+94004366087962090@172.20.208.26:5060;user=phone SIP/2.0\n" +
				"Via: SIP/2.0/UDP 172.16.223.169:5060;branch=z9hG4bK04B005251b3fbe09c07\n" +
				"From: <sip:+4319081091@172.16.223.169;user=phone>;tag=gK04007d29\n" +
				"To: <sip:+94004366087962090@172.20.208.26;user=phone>\n" +
				"Call-ID: 262208_133036001@172.16.223.169\n" +
				"CSeq: 779976 INVITE\n" +
				"Max-Forwards: 70\n" +
				"Allow: INVITE,ACK,CANCEL,BYE,REGISTER,REFER,INFO,SUBSCRIBE,NOTIFY,PRACK,UPDATE,OPTIONS,MESSAGE,PUBLISH\n" +
				"Accept: application/sdp, application/isup, application/dtmf, application/dtmf-relay, multipart/mixed\n" +
				"Contact: <sip:+4319081091@172.16.223.169:5060>\n" +
				"P-Asserted-Identity: <sip:+4319081091@172.16.223.169:5060;user=phone>\n" +
				"P-Charging-Vector: icid-value=123456\n" +
				"Diversion: <sip:+43130396327@172.20.208.26:5060>;privacy=off;screen=yes;noa=4;reason=no-answer;counter=5\n" +
				"Diversion: <sip:+43130396327@172.20.208.26:5060>;privacy=off;noa=4;reason=unconditional\n" +
				"P-Sig-Info: cntrct=43130396327\n" +
				"Supported: timer,100rel,precondition,replaces\n" +
				"Session-Expires: 3600\n" +
				"Min-SE: 90\n" +
				"P-Served-User: <sip:+43130396327@test.festnetz.drei.at>;regstate=unreg;sescase=orig\n" +
				"Content-Length: 478\n" +
				"Content-Disposition: session; handling=required\n" +
				"Content-Type: application/sdp\n" +
				"\n" +
				"v=0\n" +
				"o=Sonus_UAC 130365 900720 IN IP4 172.16.223.169\n" +
				"s=SIP Media Capabilities\n" +
				"c=IN IP4 172.16.224.147\n" +
				"t=0 0\n" +
				"m=audio 1502 RTP/AVP 8 97 98 99 18 101\n" +
				"a=rtpmap:8 PCMA/8000/1\n" +
				"a=rtpmap:97 AMR-WB/16000\n" +
				"a=fmtp:97 mode-set=0,1,2; mode-change-capability=2; max-red=0\n" +
				"a=rtpmap:98 AMR/8000\n" +
				"a=fmtp:98 mode-set=0,4,7; mode-change-capability=2; max-red=0\n" +
				"a=rtpmap:99 GSM-EFR/8000\n" +
				"a=rtpmap:18 G729/8000\n" +
				"a=fmtp:18 annexb=no\n" +
				"a=rtpmap:101 telephone-event/8000\n" +
				"a=fmtp:101 0-15\n" +
				"a=sendrecv\n" +
				"a=ptime:20\n";
		return ret;
	}


	private String getInitialRegister(){
		String ret = "REGISTER sip:mpx.sit.as.drei.at SIP/2.0\n" +
				"From: <sip:scscf.b52-t.ims.mnc014.mcc232.3gppnetwork.org>;tag=ztesipVYwszgO_Htpx3v*3-3-16648*fdjd.3\n" +
				"P-Charging-Vector: icid-value=KuTSx238Yo-004452bb-09-0000354d-005;orig-ioi=S-CSCF-SIT\n" +
				"To: <sip:+4319081032@test.festnetz.drei.at>\n" +
				"Expires: 0\n" +
				"P-Access-Network-Info: 3GPP-FIXED;utran-cell-id-3gpp=00000000;\"ue-ip=77.116.91.112\";\"ue-port=40342\";\"access-domain=pcscf.b52-t.test.festnetz.drei.at\"\n" +
				"Content-Type: multipart/mixed;boundary=4be227062af3zte\n" +
				"Content-Length: 2999\n" +
				"P-Visited-Network-ID: pcscf.b52-t.test.festnetz.drei.at\n" +
				"Call-ID: NjM3NTk5ODJzaXA6KzQzMTkwODEwMzJAdGVzdC5mZXN0bmV0ei5kcmVpL\n" +
				"P-Served-User: <sip:+43130396327@test.festnetz.drei.at>;regstate=unreg;sescase=orig\n" +
				"CSeq: 1 REGISTER\n" +
				"Max-Forwards: 69\n" +
				"Via: SIP/2.0/UDP 172.20.208.3;branch=z9hG4bK3ef1.1a1f7f74a439c832e26ad9a0ada79231.0;rport\n" +
				"Via: SIP/2.0/UDP 172.20.210.4:5065;branch=z9hG4bK*3-3-16648-965-15-841-0*42aqxsET3bhgag.3\n" +
				"Contact: <sip:scscf.b52-t.ims.mnc014.mcc232.3gppnetwork.org:5065>\n" +
				"Path: <sip:172.20.208.3;lr;received=sip:172.20.210.4:5065>\n" +
				"\n" +
				"--4be227062af3zte\n" +
				"Content-Type: message/sip\n" +
				"Content-Length: 1082\n" +
				"\n" +
				"SIP/2.0 200 OK\n" +
				"Date: Mon, 27 Jul 2020 12:53:48 GMT\n" +
				"Service-Route: <sip:orig_000303026326b187b0c76e928a@scscf.b52-t.ims.mnc014.mcc232.3gppnetwork.org;lr>\n" +
				"P-Associated-URI: <sip:+4319081032@test.festnetz.drei.at>\n" +
				"Contact: <sip:+4319081032@77.116.91.112:40342;transport=udp>;audio;language=\"en,fr\";+g.oma.sip-im;+g.3gpp.smsip;+g.oma.sip-im.large-message;+g.3gpp.cs-voice;expires=0;+g.3gpp.icsi-ref=\"urn%3Aurn-7%3A3gpp-application.ims.iari.gsma-vs\"\n" +
				"Path: <sip:TERM_9_455c_4665117a_0@pcscf.b52-t.test.festnetz.drei.at:5060;lr>\n" +
				"P-Charging-Vector: icid-value=KuTSx238Yo-004452bb-09-0000354d-005;orig-ioi=SBC-VOBB;term-ioi=H3A\n" +
				"Call-ID: f1a05373-6633-00b7-7356-c9ece3fb4d16\n" +
				"CSeq: 72236591 REGISTER\n" +
				"From: <sip:+4319081032@test.festnetz.drei.at>;tag=1056404499\n" +
				"To: <sip:+4319081032@test.festnetz.drei.at>;tag=ztesipRxhpJr0LW1DRW*3-3-16640*fdja.3\n" +
				"Via: SIP/2.0/UDP 172.20.210.4:5066;received=172.20.210.4;branch=z9hG4bK*3-3-16648-949-15-551-0*-2WYB7-T3bhgae.3,\n" +
				"\tSIP/2.0/UDP 172.20.210.13:5060;received=172.20.210.13;branch=z9hG4bK-*9*-1-51b3b6960b3af7b89319\n" +
				"Content-Length:    0\n" +
				"\n" +
				"\n" +
				"--4be227062af3zte\n" +
				"Content-Type: message/sip\n" +
				"Content-Length: 1801\n" +
				"\n" +
				"REGISTER sip:scscf.b52-t.ims.mnc014.mcc232.3gppnetwork.org SIP/2.0\n" +
				"To: <sip:+4319081032@test.festnetz.drei.at>\n" +
				"From: <sip:+4319081032@test.festnetz.drei.at>;tag=1056404499\n" +
				"Call-ID: f1a05373-6633-00b7-7356-c9ece3fb4d16\n" +
				"CSeq: 72236591 REGISTER\n" +
				"Route-key:000303\n" +
				"Max-Forwards: 68\n" +
				"Contact: <sip:+4319081032@77.116.91.112:40342;transport=udp>;+g.oma.sip-im;language=\"en,fr\";+g.3gpp.smsip;+g.oma.sip-im.large-message;audio;+g.3gpp.icsi-ref=\"urn%3Aurn-7%3A3gpp-application.ims.iari.gsma-vs\";+g.3gpp.cs-voice;expires=0\n" +
				"P-Access-Network-Info: 3GPP-FIXED;utran-cell-id-3gpp=00000000;\"access-domain=pcscf.b52-t.test.festnetz.drei.at\";\"ue-ip=77.116.91.112\";\"ue-port=40342\"\n" +
				"Require: path\n" +
				"Path: <sip:TERM_9_455c_4665117a_0@pcscf.b52-t.test.festnetz.drei.at:5060;lr>\n" +
				"P-Preferred-Identity: <sip:+4319081032@test.festnetz.drei.at>\n" +
				"User-Agent: IM-client/OMA1.0 android-ngn-stack/v2.548.870 (doubango r870 - SM-G930F)\n" +
				"Authorization: Digest username=\"+4319081032@test.festnetz.drei.at\",realm=\"festnetz.drei.at\",nonce=\"7e2f3cb49b9e6e5b0b69de9aaaa79313\",uri=\"sip:test.festnetz.drei.at\",response=\"9eaf0dd7e89048bf98f379f1539d60dd\",cnonce=\"7d2513fb2c58b4898540ee1e52614e0b\",algorithm=MD5,opaque=\"enRlX29wYXF1ZV8wMDAzMDAwMw==\",qop=auth,nc=00000002,integrity-protected=ip-assoc-pending\n" +
				"Privacy: none\n" +
				"P-Visited-Network-ID: pcscf.b52-t.test.festnetz.drei.at\n" +
				"Allow: INVITE,ACK,CANCEL,BYE,MESSAGE,OPTIONS,NOTIFY,PRACK,UPDATE,REFER\n" +
				"Content-Length:    0\n" +
				"Via: SIP/2.0/UDP 172.20.210.4:5066;received=172.20.210.4;branch=z9hG4bK*3-3-16648-949-15-551-0*-2WYB7-T3bhgae.3,\n" +
				"\tSIP/2.0/UDP 172.20.210.13:5060;received=172.20.210.13;branch=z9hG4bK-*9*-1-51b3b6960b3af7b89319\n" +
				"P-Charging-Vector: icid-value=KuTSx238Yo-004452bb-09-0000354d-005;orig-ioi=SBC-VOBB\n" +
				"X-ZTE-Cookie: 7zs4rm1;id=f1a05373-6633-00b7-7356-c9ece3fb4d16\n" +
				"\n" +
				"\n" +
				"--4be227062af3zte--\n";
		return ret;
	}

	@Test
	public void testJsonSession() {
		try {

			Loader l;

			HeaderFactory hf = sipFactory.createHeaderFactory();
			Header header = hf.createHeader((String) "History-Info", "<sip:unknown@mpbx_unknown.invalid;user=phone;cause=404>;index=1");
			System.out.println("Header:"+header);



			SIPRequest sipRequest = (SIPRequest) sipFactory.createMessageFactory().createRequest(getInitialRegister());


			SIPHeader h = (SIPHeader) sipRequest.getHeader("P-Access-Network-Info");
			System.out.println("h:"+h.toString());

			Map<String, Object> container = new HashMap<>();
			SIPJsonFactory.getMessageFactory();
			SIPJsonFactory.getHeaderFactory();

			SIPJsonFactory.fillSipJsonFields((javax.sip.message.Message) sipRequest, container);

			System.out.println("container:"+container);

			ObjectMapper mapper = new ObjectMapper();
			try {
				String json = mapper.writeValueAsString(container);
				System.out.println("JSON:"+json);


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}


	//@Test
	public void testJsonCall(){

		try {
			SIPRequest sipRequest = (SIPRequest) sipFactory.createMessageFactory().createRequest(getInitialINVITE());
			HeaderFactory hf = sipFactory.createHeaderFactory();
			HeaderFactoryImpl hfimpl;

			ListIterator li = sipRequest.getHeaderNames();
			while(li.hasNext()){
				System.out.println(li.next().toString());
			}

			System.out.println("Unrecongized headers");
			ListIterator ln =  sipRequest.getUnrecognizedHeaders();
			while(ln.hasNext()){
				System.out.println(ln.next().toString());
			}

			Call myCall = new TestCall(sipRequest);
			CallLeg leg = new TestTerminalCallLeg();
			
			TerminalContact termContact = new TerminalContact(new Address("tel:+40723111000"));
			JsonSCIFRequest jsonCall = new JsonSCIFRequest(myCall,"");
			Map<String, Object> event = new HashMap<>();
			event.put("name", "callStart");
			Map<String, Object> callStart = new HashMap<>();
			callStart.put("cause", Cause.NONE);
			callStart.put("leg", leg.getLegAddress().toString());
			callStart.put("contact", termContact.getAddress());
			event.put("callStart", callStart);
			
			Map<String, Object> message = jsonCall.getJsonCallStart(Cause.NONE, leg, termContact, null );
			
			System.out.println("Message:"+message);

			ObjectMapper mapper = new ObjectMapper();
			try {
				String json = mapper.writeValueAsString(message);
				System.out.println("JSON:"+json);
				
				HashMap<String, Object> objFromJson = mapper.readValue(json, HashMap.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//prepare JsonResponse
			Map<String, Object> jsonResp = prepareJsonResponse();
			ApplicationSession appSession = new TestApplicationSession();
			JsonSession jsonSession = new JsonSession(appSession);
			
			JsonSCIFResponse scifResp = new JsonSCIFResponseCallStart(myCall, jsonResp, jsonSession, null);
			scifResp.execute();


			SIPJsonFactory.getXHeadersToJson(sipRequest,jsonResp);

			Map<String, Object> map = new HashMap<>();

//			//parse X-Headers
//			{
//				DiversionParser parser = new DiversionParser("<sip:+43130396327@172.20.208.26:5060>;privacy=off;screen=yes;noa=4;reason=no-answer;counter=5\n");
//				FromHeader h = (From)parser.parse();
//
//
//
//				System.out.println("Private parameters:"+((SipURI)h.getAddress().getURI()).toString());

//			}

//			List<String> deleteH = new ArrayList<>();
//			deleteH.add("X-VTAS");
//			SIPJsonFactory.deleteHeaders(deleteH,sipRequest);
//
//
//			System.out.println("After delete:"+sipRequest);




		} catch (PeerUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	class TestCall implements Call {

		private SIPRequest initialInvite;
		private SipParameterSet sipPset;
		private CommonParameterSet commPset;
		
		public TestCall(SIPRequest initialRequest){
			initialInvite = initialRequest;
			sipPset = new TestSipParameterSet(initialInvite);
			commPset = new TestCommonParameterSet();
		}
		
		@Override
		public void abortCall(Cause aCause) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void continueCall() throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void forwardCall(Contact aForwardContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Cdr getCdr() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getId() {
			// TODO Auto-generated method stub
			return "1234";
		}

		@Override
		public CallLeg[] getLegs() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getNetworkTechnology() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T extends ParameterSet> T getParameterSet(Class<T> aParameterSet) {
			// TODO Auto-generated method stub
			
			if (aParameterSet.getName()==SipParameterSet.class.getName() ){
				return (T)sipPset;
			} else if (aParameterSet.getName()==CommonParameterSet.class.getName()) {
				return (T) commPset;
			}
			
			return null;
			
		}

		@Override
		public State getState() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void joinCall(Call aJoinee) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void leaveCall(Cause aCause, Contact aForwardContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void makeCall(Address aBehalf, Contact aFirstContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void makeCall(Address aBehalf, Contact aFirstContact, Contact aSecondContact, Properties aCallOptions)
				throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void splitCall(Call aNewCall) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void transferCall(TransferMode aMode, CallLeg aClosedLeg, Contact aDestination) throws ScifException {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	class TestSipParameterSet implements SipParameterSet {

		SIPRequest initialInvite;
		
		public TestSipParameterSet(SIPRequest invite){
			initialInvite=invite;
		}
		
		@Override
		public void enable181(boolean anEnable) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void enableRawMessageStorage(boolean aEnable) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Collection<Capability> getCapabilities(CallLeg aCallLeg) {
			// TODO Auto-generated method stub
			Collection<Capability> col = new ArrayList<>();
			for( Capability cap : Capability.values()){
				col.add(cap);
			}
			return col;
		}

		@Override
		public int getErrorCode() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getHeaderRulesVariableValue(String variableName) throws ScifException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getInitialInvite() {
			// TODO Auto-generated method stub
			return initialInvite;
		}

		@Override
		public Object getInitialRequest() {
			// TODO Auto-generated method stub
			return initialInvite;
		}

		@Override
		public String getMessageBody() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getMessageBodyUa1() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getMessageBodyUa2() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public SipRawMessageContent getRawMessageContent(CallLeg aLeg) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSDP() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSDPAnswer(CallLeg aLeg) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSDPOffer(CallLeg aLeg) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSDPUa1() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSDPUa2() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void requireCapabilities(Collection<Capability> aCapabilities) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void selectCallFlow(String aCallFlow) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void selectHeaderRulesSet(String aHeaderRulesSetName) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setCapablities(CallLeg aCallLeg, Collection<Capability> aCapabilities) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setErrorCode(int aCode) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setHeader(String aHeader, String aValue, Integer aResponse) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setHeaderRulesVariableValue(String variableName, Object variableValue) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setCallRedirectionType(CallRedirectionType callRedirectionType) {

		}

		@Override
		public void setCallDiversionCause(int i) {

		}

		@Override
		public void waitForReliable18xAcknowledgementFromUpstreamBeforeCallRedirection(boolean b) {

		}

		@Override
		public void send199UpstreamBeforeCallRedirection(boolean b) {

		}

		@Override
		public void setHistoryInfoTargetParameter(HistoryInfoTargetParam historyInfoTargetParam) {

		}

		@Override
		public void addCallDiversionSpoofingHistoryInfoEntries(int i) {

		}

		@Override
		public void setOriginalCalledPartyTreatmentForHistoryInfoHeader(OriginalCalledPartyTreatment originalCalledPartyTreatment) {

		}

		@Override
		public void addHistoryInfoEntries(List<String> list) throws ScifException {

		}

		@Override
		public void setRawMessageContent(CallLeg aLeg, RawMessageAffinity anAffinity, SipRawMessageContent aRawContent)
				throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setRouteSet(List<Address> aRouteSet) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setSDP(String aSdp) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setSDPAnswer(CallLeg aLeg, String aSDP) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setSDPOffer(CallLeg aLeg, String aSDP) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setTemporaryRouteSet(List<Address> aRouteSet) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setErrorCode(int arg0, String arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setPrivacyForHistoryInfoEntries(boolean val1,boolean val2) {

		}
		
	}
	
	class SipCallUserTest implements CallUser {

		private TestCall testCall;
		
		public SipCallUserTest(){
			SIPRequest sipRequest;
			try {
				sipRequest = (SIPRequest) sipFactory.createMessageFactory().createRequest(getInitialINVITE());
				testCall = new TestCall(sipRequest);
			} catch (PeerUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@Override
		public void callAnswered(Cause aCause, CallLeg aNewLeg) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEarlyAnswered(CallLeg aNewLeg) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEnd(Cause aCause) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callPoll(ScifEvent anEvent) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	public class TestTerminalCallLeg implements TerminalCallLeg{

		String name="TestTerminalCallLeg";
		Address leg =null;
		
		public TestTerminalCallLeg(){
			leg = new Address();
			leg.setUser("+40723111000");
			leg.setScheme(Address.Scheme.SIP);
			leg.setServer("domain.com");
		}
		
		@Override
		public Address getLegAddress() {
			// TODO Auto-generated method stub
			
			return leg;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return name;
		}

		@Override
		public boolean isEarlyMedia() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void release(Cause arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setName(String arg0) {
			// TODO Auto-generated method stub
			name = arg0;
			
		}

		@Override
		public String getLegId() {
			// TODO Auto-generated method stub
			return null;
		}		
	}
	
	class TestCommonParameterSet implements CommonParameterSet {

		@Override
		public long getCallAttempTime() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getCallConnectedTime() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getCallReleaseTime() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getConnectionTimeout() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public <T extends Enum<T>> T getEnumServiceCallType(Class<T> arg0) throws ScifException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getErrorCode() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public MediaType getMediaType() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public NetworkCallType getNetworkCallType() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getNetworkTimeZoneOffSet() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Address getRedirectingParty() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T extends ServiceCallType> T getServiceCallType(Class<T> arg0) throws ScifException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getServiceKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public SwitchingPoint getSwitchingPoint() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void pollRinging(boolean arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setActivityTestInterval(int arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setCallingParty(Address arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setConnectionTimeout(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public <T extends Enum<T>> void setEnumServiceCallType(T arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setErrorCode(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setMessageInterceptor(MessageInterceptor arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setRedirectingParty(Address arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setRootCdr(Cdr arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setServiceCallType(ServiceCallType arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setServiceKey(String arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setSwitchingPoint(SwitchingPoint arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public <T extends InformationalEvent> void subscribePollEvent(Class<T> arg0, Object... arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deleteCdr() throws ScifException {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private Map<String, Object> prepareJsonResponse(){
		Map<String, Object> rawMessage=new HashMap<>();
		List<String> headerRules = new ArrayList<>();
		headerRules.add("rule1");
		headerRules.add("rule2");
		rawMessage.put(JsonSCIFResponse.HEADER_RULES,headerRules);
		
		
		Map<String, Object> events = new HashMap<>();
		events.put("SuccessResponsePollEvent", null);
		events.put("RawContentPollEvent", "test/test");
		events.put("InfoPollEvent", null);
		
		rawMessage.put(JsonSCIFResponse.EVENTS,events);
		
		Map<String,Object> headerVars = new HashMap<>();
		headerVars.put("histinfo", "mama");
		rawMessage.put(JsonSCIFResponse.HEADER_VARS,headerVars);
		
		rawMessage.put(JsonSCIFResponse.HEADER_SELECT,"none");	

		List<Map<String, Object>> timers = new ArrayList<>(); 
		Map<String, Object> timer1 = new HashMap<>();
		timer1.put(JsonSCIFResponse.TIMEOUT, 1000);
		timer1.put(JsonSCIFResponse.TIMER_NAME, "testTimer");
		timers.add(timer1);
		//rawMessage.put(JsonSCIFResponse.TIMERS,timers);

		List<String> caps = new ArrayList<>(); 
		caps.add("FORKING");
		caps.add("PEM");
		rawMessage.put(JsonSCIFResponse.CAPABILITIES,caps);

		List<Map<String,Object>>ringingTones =  new ArrayList<>();
		Map<String, Object> ringtone1 = new HashMap<>();
		ringtone1.put(JsonSCIFResponse.UI_ANN, "Comfort1");
		ringtone1.put(JsonSCIFResponse.ANN_TYPE, "CONNECT");
		ringingTones.add(ringtone1);
		rawMessage.put(JsonSCIFResponse.RINGINGTONES,ringingTones);
		
		Map<String,Object> action = new HashMap<>();
		action.put(JsonSCIFResponse.ACTION_TYPE, 1);
		action.put(JsonSCIFResponse.URIs, "tel:+40723111000");
		
		rawMessage.put(JsonSCIFResponse.ACTION,action);
		
		return rawMessage;
	}
	
	class TestApplicationSession implements ApplicationSession {

		@Override
		public void addSession(ProtocolSession arg0, String arg1) throws ImscException, IllegalStateException {
			// TODO Auto-generated method stub
			
		}


		@Override
		public Object getAttribute(String arg0) throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Iterator<String> getAttributeNames() throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getCreationTime() throws IllegalStateException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public ImsletContext getImsletContext() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getLastAccessedTime() throws IllegalStateException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ProtocolSession getSession(String arg0) throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Iterator<ProtocolSession> getSessions(String arg0) throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Iterator<String> getSessionsNames() throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getSessionsNb() throws IllegalStateException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Iterator<ImsletTimer> getTimers() throws IllegalStateException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void invalidate() throws IllegalStateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isPersistent() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void merge(ApplicationSession arg0) throws ImscException, IllegalStateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void removeAttribute(String arg0) throws IllegalStateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void removeSession(ProtocolSession arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void rename(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setAttribute(String arg0, Object arg1) throws IllegalStateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setExpires(int arg0) throws IllegalStateException, IllegalArgumentException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setName(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setPersistent(boolean arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deliveryLock() throws IllegalStateException, InterruptedException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deliveryUnlock() throws ImscException, IllegalStateException {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Test
	public void testRegExp(){

	}
}
