package com.eitg.quasar.occp.sip.servicelogic;

import java.text.SimpleDateFormat;
import java.util.*;

import com.eitg.quasar.nexus.middleware.utils.Commons;
import com.eitg.quasar.occp.degraded.DegradedModeThread;
import com.eitg.quasar.occp.metrics.MetricsServer;
import com.eitg.quasar.occp.sip.utils.TimerInformation;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.events.RingingPollEvent;
import com.hp.opencall.ngin.scif.events.SdpAnswerEvent;
import com.hp.opencall.ngin.scif.events.sip.*;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.timer.TimerListener;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.apache.commons.pool2.SwallowedExceptionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.TelecomUtils;
import com.eitg.quasar.occp.sip.utils.TimerInformation.TIMER_TYPE;
import com.eitg.quasar.occp.utils.JsonSCIFResponseCallStart;
import com.eitg.quasar.occp.utils.JsonCall;
import com.eitg.quasar.occp.utils.JsonSCIFRequest;
import com.eitg.quasar.occp.utils.JsonSCIFResponse;
import com.eitg.quasar.occp.utils.JsonSCIFResponseLegAction;
import com.eitg.quasar.occp.utils.JsonSCIFResponseCallPoll;
import com.eitg.quasar.occp.utils.JsonSCIFTimerListener;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.resources.RawMediaCallLeg;
import com.hp.opencall.ngin.timer.Timer;
import gov.nist.javax.sip.header.AcceptLanguageList;

public class SipCallUser implements CallUser
 {
	
	//public static WeakHashMap<Call, String> calls = new WeakHashMap<>();
	class MaxCallDurationListener implements TimerListener {

		private Call call ;
		private SipCallUser callUser;

		public MaxCallDurationListener(Call pCall, SipCallUser sipCallUser){
			this.call=pCall;
			this.callUser = sipCallUser;
		}

		@Override
		public void timeout(Timer timer) {
			_log.debug("Executing MaxCallDurationListener timer ..., pending call answered {}",this.callUser.callIsAnswered);
			if( timer!=null ) {
				timer.cancel();
			}

			//
			if( this.callUser.callIsAnswered){
				//cancel outgoing leg
				CallLeg[] callLegs = this.call.getLegs();
				_log.debug("Available call legs:{} size:{}",callLegs,callLegs.length);
				for( CallLeg leg : callLegs ){
					if( !leg.equals(this.callUser.getUpstreamLeg())){
						try {
							_log.debug("Releasing leg:{}",leg);
							this.callUser.getEvents().add("leg.max_call_duration");
							leg.release(Cause.DISCONNECTED);

						} catch (Exception e){
							_log.error("Exception on release leg",e);
						}
					}
				}
			}
		}
	}

	
	private static final Logger _log = LoggerFactory.getLogger(SipCallUser.class.getName());

	private JsonSession jsonSession=null;
	private JsonSCIFRequest jsonCall=null;
	
	public static final String QUEUE_INGRESS="app_events";
	public static String QUEUE_EGRESS="sip_events";

	private String queue=null ;
	
	/**
	 * The call object and the application session for this callUser
	 */
	private Call call;
	private ApplicationSession appSession;
	private long timeDiff = 0;
	private SipRawBytesMediaUser sipMediaUser;
	private RawMediaCallLeg rawMediaLeg =null;
	String ingressQueue ="";
	String sipCallId=null;
	private CallLeg upstreamLeg=null;
	private boolean fireAndForget=false;
	private boolean overload=false;
	private boolean pendingForwardCall=false;
	protected boolean callIsAnswered=false;
	private Integer maxCallDuration=0;

	public void setMaxCallDuration(int value){
		this.maxCallDuration=value;
	}

	public void setPendingForwardCall(){
		this.pendingForwardCall=true;
	}

	public boolean isPendingForwardCall(){
		return this.pendingForwardCall;
	}

	private Map<String,Object> syncPollEvents= new HashMap<>();
	
	public SipCallUser(){
		long currentTimeMs = Calendar.getInstance().getTimeInMillis();
		this.call=null;
		this.appSession=null;
	}

	public static synchronized void  setEgressQueue(String queueName){
		_log.debug("Set egress queue:{}",queueName);
		QUEUE_EGRESS = queueName;
	}

	public void setQueue(String queue){
		this.queue=queue;
	}

	private List<String> events = new LinkedList<>();

	public List<String> getEvents(){
		return events;
	}
	public void pushEvent(String event){
		events.add(event);
	}

	
	public SipCallUser(Call aCall, ApplicationSession anAppSession) {
		// get current UTC time in milliseconds since epoch
		long currentTimeMs = Calendar.getInstance().getTimeInMillis();

		//Call jsonCall = new JsonCall(aCall);
		//aCall = JsonCall.swap(jsonCall, (jsonCall=aCall));
		
		// Store the call and the application session in class variables.
		call = aCall;
		appSession = anAppSession;
		appSession.setAttribute("scifCallUser", this);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(anAppSession.getCreationTime());
		timeDiff = currentTimeMs - anAppSession.getCreationTime();
		_log.info("SipCallUser constructor, creation time:{}, " , anAppSession.getCreationTime() );

		jsonSession = new JsonSession(appSession);
		jsonCall = new JsonSCIFRequest(call,"");
		sipMediaUser = new SipRawBytesMediaUser(jsonCall, jsonSession, this);
		
	
		ingressQueue = "app_"+jsonCall.getSIPCallId();
		//calls.put(aCall, aCall.getId());


		try {
			_log.debug("RawCallPollEvents:"+SipServiceImslet.rawCallPollEvents);
			if( SipServiceImslet.rawCallPollEvents!=null ){
//				String subscriberEvents[]={"application/vnd.etsi.pstn+xml"};
				String subscriberEvents[]= new String[SipServiceImslet.rawCallPollEvents.size()];
				SipServiceImslet.rawCallPollEvents.toArray(subscriberEvents);
				_log.debug("Subscribing to RawContentPollEvent ...:{}",subscriberEvents);

				CommonParameterSet commonParameterSet = call.getParameterSet(CommonParameterSet.class);
				commonParameterSet.subscribePollEvent(RawContentPollEvent.class, subscriberEvents);
			}
		} catch (Exception e){
			e.printStackTrace();
			_log.error("Exception subscribing to RawContent event",e);
		}
	}

	// ************************ callStart ************************************
	public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
		if(queue==null){
			queue = QUEUE_EGRESS;
		}
		// Log the call Id and the cause for reaching Start state.
		_log.debug("callStart NEW invoked with cause:{}, callleg:{}, contact:{} ", aCause  , aFirstLeg , aCalledParty);

		if( upstreamLeg==null){
			_log.debug("Set upstream leg ..., all the other legs are downstream");
			upstreamLeg = aFirstLeg;
		}

		String counter = "tas.sip.callStart."+aCause ;
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.CALL_USER, this);
		
		if( aCause!=Cause.NONE ){
			_log.debug("Cancel pending forward call for cause:{}",aCause);
			this.pendingForwardCall=false;
		}
		
		Long lstartTime = Calendar.getInstance().getTimeInMillis();
		try{

			this.callIsAnswered=false;
			//prepare JSON message
			Map<String,Object> rawMessage = jsonCall.getJsonCallStart(aCause, aFirstLeg, aCalledParty, jsonSession);

			rawMessage.put("events-stack",events);
			events.add((String)rawMessage.get("event-name"));


			//apply overload for call start only
			if( aCause==Cause.NONE){
				if(DegradedModeThread.inOverload==true || DegradedModeThread.inDegraded ){
					overload=true;
					_log.error("Enter to overload, do nothing call-id:{} firstleg:{} contact:{}",call.getId(),aFirstLeg.toString(),aCalledParty.toString());

					String counterOverload = "tas.sip.overload" ;
					MetricsServer.get().counterIncrement(counterOverload);

//					Object result = SipServiceImslet.getTypeScriptProcessor().runScript(rawMessage);
//					_log.debug("SOM result:{}",result);

//					JsonSCIFResponse resp = JsonSCIFResponse.getResponse((Map<String,Object>)result,eventData);
//					resp.execute();
//					setOverload();
					SipParameterSet sipParameterSet = (SipParameterSet)call.getParameterSet(SipParameterSet.class);
					if( sipParameterSet!=null){
						sipParameterSet.setErrorCode(DegradedModeThread.sipErrorCode);
					}
					call.abortCall(Cause.REJECTED);
					return ;
				}
			}


			if( overload ){
				_log.error("Overload unexpected callStart event call-id:{}",call.getId());
				return ;
			}


			
			
			ObjectMapper mapper = new ObjectMapper();
			String json=null;
			json = mapper.writeValueAsString(rawMessage);
			
			_log.debug("JSON:{}",json);

			if( json!=null && fireAndForget==false){
				//send this to redis queue
				_log.debug("Sending JSON message to redis ...");
				RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);

				boolean status = RedisPool.get().sendToQueue(rawMessage, queue);
			}
		} catch (Exception e){
			_log.error("Exception",e);
		} finally {
			jsonCall.incrementEventId();
			_log.info("Execution time, cause:{}, spent time:{}",aCause, (Calendar.getInstance().getTimeInMillis()-lstartTime));
		}
	}

	// This service does not rely on an early media feature
	public void callEarlyAnswered(CallLeg aNewLeg) {
		String counter = "tas.sip.callEarlyAnswered" ;
		MetricsServer.get().counterIncrement(counter);

		this.pendingForwardCall=false;
		if( overload ){
			_log.debug("EarlyAnswered - do nothing");
			return ;
		}
		if( queue==null ){
			queue=QUEUE_EGRESS;
		}
		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLEARLYANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, this);
		
		try{
			// _log.info("Call early-answered");
			Map<String, Object> rawEarlyAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg);
			_log.info(" rawEarlyAnswered:{}",rawEarlyAnswered);
			rawEarlyAnswered.put("events-stack",events);
			events.add((String)rawEarlyAnswered.get("event-name"));


			if (aNewLeg instanceof RawMediaCallLeg){
				rawMediaLeg = (RawMediaCallLeg) aNewLeg;
				_log.debug("MRF leg, set media user:{}",rawMediaLeg.toString());
				
				sipMediaUser.setRawMediaCallLeg(rawMediaLeg);
				rawMediaLeg.setRawBytesMediaUser(sipMediaUser);
			}

			if( fireAndForget==false) {
				RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);
				boolean status = RedisPool.get().sendToQueue(rawEarlyAnswered, queue);
			}
		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callAnswered(Cause aCause, CallLeg aNewLeg) {
		this.callIsAnswered=true;
		String counter = "tas.sip.callAnswered."+aCause ;
		MetricsServer.get().counterIncrement(counter);

		this.pendingForwardCall=false;
		if( overload ){
			_log.debug("Answered - do nothing while in overload");
			return ;
		}
		if( queue==null){
			queue = QUEUE_EGRESS;
		}
		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, this);
		
		try{
			_log.info("callAnswered cause:{}, leg:{}",aCause, aNewLeg);
			Map<String, Object> rawAnswered = jsonCall.getAnswered(aCause, aNewLeg);
			_log.info(" rawAnswered:{}",rawAnswered);

			rawAnswered.put("events-stack",events);
			events.add((String)rawAnswered.get("event-name"));

			if (aNewLeg instanceof RawMediaCallLeg){
				rawMediaLeg = (RawMediaCallLeg) aNewLeg;
				_log.debug("MRF leg, set media user:{}",rawMediaLeg.toString());
				
				sipMediaUser.setRawMediaCallLeg(rawMediaLeg);
				rawMediaLeg.setRawBytesMediaUser(sipMediaUser);
			}

			if( fireAndForget==false ) {
				RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);
				boolean status = RedisPool.get().sendToQueue(rawAnswered, queue);
			}
			if( this.maxCallDuration>0){
				_log.debug("Arm max call duration timer :{}",maxCallDuration);
				MaxCallDurationListener maxCallDurationListener = new MaxCallDurationListener(this.call, this);
				Timer timer = TelecomUtils.createTimer(maxCallDurationListener, maxCallDuration*1000,
						this.getApplicationSession(),
						TimerInformation.TIMER_TYPE.ASYNCHRONOUS_CALL);
				//set max call duration
				this.maxCallDuration=0;
			}



		} finally {
			jsonCall.incrementEventId();
		}
	}
		

	public void callPoll(ScifEvent anEvent) {
		String counter = "tas.sip.callPoll."+ anEvent.getClass().getSimpleName() ;
		MetricsServer.get().counterIncrement(counter);

		if( overload ){
			//do nothing
			_log.error("Overload - unexpected callPoll:{}",anEvent.toString());
			return;
		}

		if(queue==null){
			queue=QUEUE_EGRESS;
		}
		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLPOLL);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.POLLEVENT, anEvent);
		eventData.put(JsonSCIFResponse.CALL_USER, this);

		String pollEventClass = anEvent.getClass().getSimpleName();
		_log.debug("SyncPollEvents: poll event class:{} {} ",pollEventClass,syncPollEvents);


		if( pollEventClass.equalsIgnoreCase("SDPAnswerAction")){
			_log.debug("Disable pendingforwardcall ...  - no answer timer.");
			this.pendingForwardCall=false;
		}

		_log.debug("Syncpoll events:{} handle it as synchronous:{}",syncPollEvents,syncPollEvents.containsKey(pollEventClass));
		if( syncPollEvents.containsKey(pollEventClass)){
			Map<String,Object> action = (Map<String,Object>)syncPollEvents.get(pollEventClass);

			if( !action.containsKey("multiple")){
				syncPollEvents.remove(pollEventClass);
			}
			if(anEvent instanceof RawContentPollEvent ){
				_log.debug("Handling callPoll in synchronous manner, action:{}",syncPollEvents);
				RawContentPollEvent rawContentPollEvent = (RawContentPollEvent) anEvent;
				//syncPollEvents.clear();
				if( action!=null ){
					String actionType=(String)action.get("action");
					_log.debug("Handling event synchronously");
					if( actionType.equalsIgnoreCase("accept")){
						rawContentPollEvent.accept();
						return;
					} else if (actionType.equalsIgnoreCase("reject")){
						rawContentPollEvent.reject(Cause.REJECTED);
						return;
					}
				}
			} else if (anEvent instanceof SIP18xInformationalEvent){
				SIP18xInformationalEvent sip18xInformationalEvent = (SIP18xInformationalEvent) anEvent;

				//syncPollEvents.clear();
				if( action!=null ){
					String actionType=(String)action.get("action");
					_log.debug("Handling event synchronously");
					if( actionType.equalsIgnoreCase("accept")){

						return;
					} else if (actionType.equalsIgnoreCase("reject")){
						try {
							sip18xInformationalEvent.getLeg().release(Cause.REJECTED);
						} catch (Exception e){
							_log.error("Exception",e);
						}
						return;
					}
				}
			}  else if (anEvent instanceof RingingPollEvent){
				_log.debug("Ringing poll event come ...");
				this.pendingForwardCall=false;

				if( action!=null ){
					String actionType=(String)action.get("action");
					_log.debug("Handling event synchronously");
					if( actionType.equalsIgnoreCase("accept")){
						((RingingPollEvent) anEvent).accept();
						return;
					} else if (actionType.equalsIgnoreCase("reject")){
						((RingingPollEvent) anEvent).reject(Cause.REJECTED);
						return;
					} else if (actionType.equalsIgnoreCase("forward")){
						try {
							((RingingPollEvent) anEvent).forward();
						} catch (Exception e){
							_log.error("Forward Exception",e);
						}
						return;
					}
				}

				return ;
			}  else if (anEvent instanceof SIPRingingPollEvent){
				this.pendingForwardCall=false;
				if( action!=null) {
					String actionType=(String)action.get("action");
					SIPRingingPollEvent sipRingingPollEvent = (SIPRingingPollEvent) anEvent;
					if( actionType.equalsIgnoreCase("forward")) {
						try {
							sipRingingPollEvent.forward();
							return ;
						} catch (Exception e) {
							_log.error("Exception", e);
						}
					} else if ( actionType.equalsIgnoreCase("accept")) {
						sipRingingPollEvent.accept();
						return ;
					}  else if ( actionType.equalsIgnoreCase("reject")) {
						sipRingingPollEvent.reject(Cause.REJECTED);
						return ;
					}
				}
			}
		}

		try{
			_log.info("callPoll event:{}",anEvent);
			Map<String, Object> rawPollMessage = jsonCall.getJsonPollEvent(anEvent);

			rawPollMessage.put("events-stack",events);
			events.add((String)rawPollMessage.get("event-name"));

			_log.debug("Poll raw message:{}",rawPollMessage);

			if( fireAndForget==false ) {
				RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);
				boolean status = RedisPool.get().sendToQueue(rawPollMessage, queue);
			}


			//TO BE  removed
//			if( anEvent instanceof  SuccessResponsePollEvent){
//				//to be removed, armed on call poll
//				this.callIsAnswered=true;
//				this.pendingForwardCall=false;
//				if( this.maxCallDuration>0){
//					_log.debug("Arm max call duration timer :{} callAnswered:{}",maxCallDuration,this.callIsAnswered);
//					MaxCallDurationListener maxCallDurationListener = new MaxCallDurationListener(this.call, this);
//					Timer timer = TelecomUtils.createTimer(maxCallDurationListener, maxCallDuration*1000,
//							this.getApplicationSession(),
//							TimerInformation.TIMER_TYPE.ASYNCHRONOUS_CALL);
//					//set max call duration
//					//this.maxCallDuration=0;
//				}
//			}
			
		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callEnd(Cause aCause) {
		String counter = "tas.sip.callEnd."+ aCause;
		MetricsServer.get().counterIncrement(counter);
		this.pendingForwardCall=false;

		if( overload ){
			//do nothing
			_log.debug("Overload -callEnd - do nothing cause:{}",aCause);
			return;
		}

		if(queue==null){
			queue=QUEUE_EGRESS;
		}
		try{
			_log.info("CallUser callEnd cause:{} fireAndForget:{}", aCause ,fireAndForget);
			if( fireAndForget==false) {
				Map<String, Object> rawEnd = jsonCall.getCallEnd(aCause);
				rawEnd.put("events-stack",events);
				events.add((String)rawEnd.get("event-name"));

				_log.info(" rawEnd:{}", rawEnd);
				boolean status = RedisPool.get().sendToQueue(rawEnd, queue);
			}
		} finally {
			jsonCall.incrementEventId();
			
			//remove events from parking slot
			RedisProcessorThread.remove(jsonCall.getSIPCallId());
		}
		
	}


	public String getCallId() {
		
		return jsonCall.getSIPCallId();
	}

	
	/**
	 * method used to test on local loop
	 */
	private void prepareResponseCSNone(String callId, Cause cause){
		_log.debug("prepareResponseCSNone callId:{} cause:{}",callId,cause);
		//put message on queue
		Map<String, Object> rawMessage=new HashMap<>();
		List<String> headerRules = new ArrayList<>();
		headerRules.add("rule1");
		headerRules.add("rule2");
		rawMessage.put(JsonSCIFResponse.HEADER_RULES,headerRules);
		
		
		Map<String, Object> events = new HashMap<>();
		events.put("SuccessResponsePollEvent", null);
		events.put("RawContentPollEvent", "test/test");
		events.put("InfoPollEvent", null);
		
		rawMessage.put(JsonSCIFResponse.EVENTS,events);
		
		Map<String,Object> headerVars = new HashMap<>();
		headerVars.put("histinfo", "mama");
		rawMessage.put(JsonSCIFResponse.HEADER_VARS,headerVars);
		
		rawMessage.put(JsonSCIFResponse.HEADER_SELECT,"none");	

//		List<Map<String, Object>> timers = new ArrayList<>(); 
//		Map<String, Object> timer1 = new HashMap<>();
//		timer1.put(JsonSCIFResponse.TIMEOUT, 1000);
//		timer1.put(JsonSCIFResponse.TIMER_NAME, "testTimer");
//		timers.add(timer1);
		//rawMessage.put(JsonSCIFResponse.TIMERS,timers);

		List<String> caps = new ArrayList<>(); 
		caps.add("FORKING");
		caps.add("PEM");
		rawMessage.put(JsonSCIFResponse.CAPABILITIES,caps);

		List<Map<String,Object>>ringingTones =  new ArrayList<>();
		Map<String, Object> ringtone1 = new HashMap<>();
		ringtone1.put(JsonSCIFResponse.UI_ANN, "Comfort1");
		ringtone1.put(JsonSCIFResponse.ANN_TYPE, "CONNECT");
		ringingTones.add(ringtone1);
		rawMessage.put(JsonSCIFResponse.RINGINGTONES,ringingTones);
		
		Map<String,Object> action = new HashMap<>();
		
		if( cause==Cause.NONE){
			action.put(JsonSCIFResponse.ACTION_TYPE, 2);
		} else {
			action.put(JsonSCIFResponse.ACTION_TYPE, 0);
			action.put(JsonSCIFResponse.ERRORCODE, Cause.INTERNAL.ordinal());
		}
		
		rawMessage.put(JsonSCIFResponse.ACTION,action);
		
		RedisPool.get().sendToQueue(rawMessage, QUEUE_INGRESS);
	}
	
	/**
	 * method used to test on local loop
	 */
	private void prepareResponsePoll(String callId){
		//put message on queue
		Map<String, Object> rawMessage=new HashMap<>();
		List<String> headerRules = new ArrayList<>();
		headerRules.add("rule1");
		headerRules.add("rule2");
		rawMessage.put(JsonSCIFResponse.HEADER_RULES,headerRules);
		
		
		Map<String, Object> events = new HashMap<>();
		events.put("SuccessResponsePollEvent", null);
		events.put("RawContentPollEvent", "test/test");
		events.put("InfoPollEvent", null);
		
		rawMessage.put(JsonSCIFResponse.EVENTS,events);
		
		Map<String,Object> headerVars = new HashMap<>();
		headerVars.put("histinfo", "mama");
		rawMessage.put(JsonSCIFResponse.HEADER_VARS,headerVars);
		
		rawMessage.put(JsonSCIFResponse.HEADER_SELECT,"none");	

		Map<String,Object> action = new HashMap<>();
		action.put(JsonSCIFResponse.ACTION_TYPE, "accept");
		
		rawMessage.put(JsonSCIFResponse.ACTION,action);
		
		RedisPool.get().sendToQueue(rawMessage, QUEUE_INGRESS);
	}
	
	private void prepareCallAnswered(String callId){
		Map<String, Object> rawMessage = new HashMap<>();
		Map<String,Object> action = new HashMap<>();
		
		action.put(JsonSCIFResponse.ACTION_TYPE, 3);
		action.put("legAction", "performMediaOperation");
		
		Map<String, Object> mediaOperation = new HashMap<>();
		mediaOperation.put("Content-Type", "application/msml+xml");
		mediaOperation.put("Content", "tralalala");
		action.put("performMediaOperation", mediaOperation);
		
		rawMessage.put(JsonSCIFResponse.ACTION,action);
		
		RedisPool.get().sendToQueue(rawMessage, QUEUE_INGRESS);
		
	}


	public void setCall(Call newCall) {
		this.call= newCall;
	}

	public void initialize(Call aCall, ApplicationSession anAppSession, String sipCallId){
		call = aCall;
		appSession = anAppSession;
		appSession.setAttribute("scifCallUser", this);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(anAppSession.getCreationTime());

		jsonSession = new JsonSession(appSession);
		jsonCall = new JsonSCIFRequest(call, sipCallId);
		sipMediaUser = new SipRawBytesMediaUser(jsonCall, jsonSession, this);


		ingressQueue = "app_"+jsonCall.getSIPCallId();
		//calls.put(aCall, aCall.getId());
	}

	 public void initialize(Call aCall, String sipCallId){
		 call = aCall;
		 appSession = null;



		 jsonSession = new JsonSession(appSession);
		 jsonCall = new JsonSCIFRequest(call, sipCallId);

		 jsonCall.setSipCallId(sipCallId);
		 sipMediaUser = new SipRawBytesMediaUser(jsonCall, jsonSession, this);


		 ingressQueue = "app_"+jsonCall.getSIPCallId();
		 //calls.put(aCall, aCall.getId());
	 }

	public CallLeg getUpstreamLeg(){
		return this.upstreamLeg;
	}

	public Call getCall(){
		return this.call;
	}

	public JsonSession getJsonSession(){
		return this.jsonSession;
	}

	public void setFireAndForget(boolean value){
		_log.debug("setFireAndForget:{}",value);
		this.fireAndForget = value;
	}

	private void setOverload(){
		if( RedisPool.getTasGw()!=null ) {
			Map<String, Object> overload = (Map<String, Object>)RedisPool.getTasGw().get("overload");
			Integer errorCode = (Integer) overload.get("error-code");
			SipParameterSet sipPset = (SipParameterSet)call.getParameterSet(SipParameterSet.class);
			if( errorCode!=null && sipPset!=null){
				sipPset.setErrorCode(errorCode);
			}
			call.abortCall(Cause.INTERNAL);
		}
	}

	public ApplicationSession getApplicationSession(){
		return this.appSession;
	}

	public void setSyncPollEvents(Map<String,Object> values){
		_log.debug("Set syncPollEvents:{}",values);
		if( this.syncPollEvents==null) {
			this.syncPollEvents = values;
		} else {
			this.syncPollEvents.putAll(values);
		}
	}

	public boolean isCallIsAnswered() { return this.callIsAnswered ; };

}
