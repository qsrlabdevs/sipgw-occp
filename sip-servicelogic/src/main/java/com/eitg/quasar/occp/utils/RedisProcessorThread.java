package com.eitg.quasar.occp.utils;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.SlidingTimeWindowReservoir;
import com.eitg.quasar.occp.metrics.MetricsServer;
import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.sip.utils.TelecomUtils;
import com.eitg.quasar.occp.sip.utils.TimerInformation.TIMER_TYPE;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.timer.Timer;

/**
 * 
 * This class is used to fetch message from redis queue and issue specific commands
 *
 */
public class RedisProcessorThread extends Thread {

	private static final Logger _log = LoggerFactory.getLogger(RedisProcessorThread.class.getName());
	static Map<String, Object> parkedEvents = new ConcurrentHashMap<>();

	private static SlidingTimeWindowReservoir slidingTimeWindowReservoir = new SlidingTimeWindowReservoir(1000, TimeUnit.MILLISECONDS);
	private static Histogram executionTimer = new Histogram(slidingTimeWindowReservoir);

	public static Histogram getExecutionTimer(){
		return executionTimer;
	}

	public static void parkEvent(String callId, Map<String, Object> event){
		_log.debug("Park event for callId:{} event:{}",callId,event);
		List<Map<String, Object>> existingEvents = (List<Map<String, Object>> ) parkedEvents.get(callId);
		if( existingEvents==null || existingEvents.size()==0){
			existingEvents = new ArrayList<>();
			parkedEvents.put(callId, existingEvents);
		}
		
		existingEvents.add(event);
		event.put("parkTime", Instant.now().toEpochMilli());
		_log.debug("Add park events size:{}",parkedEvents.size());
	}

	public static void checkExpiredEvents(){
	    try {
            long now = Instant.now().toEpochMilli();
            Set<String> keys = parkedEvents.keySet();
            _log.debug("Check parkedEvents, size:{}", keys.size());
            for (String key : keys) {
                List<Map<String, Object>> event = (List<Map<String, Object>>) parkedEvents.get(key);
                if (event != null) {
                    for (int i = 0; i < event.size(); i++) {
                        Map<String, Object> item = event.get(i);
                        if (item != null) {
                            Long oldTime = (Long) item.get("parkTime");
                            _log.debug("Old time:{}", oldTime);
                            if (oldTime != null && (now - oldTime) > 10000) {
                                //remove item from queue
                                event.remove(item);
                            }
                        }
                    }
                }
                if (event.size() == 0) {
                    //remove item from parked events;
					_log.error("Removing old event:{}",key);
                    parkedEvents.remove(key);
                }
            }
        } catch (Exception e){
	        _log.error("Exception",e);
        }
	}
	
	private String ingressQueue;
	private static CleanParkedEvents cleanUpThread=null;
	
	public RedisProcessorThread(String ingressQueue){
		_log.debug("Instantiate new processor thread");
		this.ingressQueue=ingressQueue;

		synchronized (parkedEvents) {
			if (cleanUpThread == null) {
				cleanUpThread = new CleanParkedEvents();
				cleanUpThread.start();
			}

		}


//		cleanUpThread = new CleanParkedEvents();
//		cleanUpThread.start();
	}
	
	@Override
	public void run(){
		while(true){
			String callId="";
			Map<String, Object> rawResponse=null;
			try {
				if( RedisPool.get().getDB()==null ){
					reconnect();
					continue;
				}
				rawResponse = RedisPool.get().getFromQueue(ingressQueue);
				if (rawResponse != null) {
					//fetch callid
					callId = (String) rawResponse.get(JsonSCIFRequest.CallID);
					//
					String eventName = (String) rawResponse.get("eventname");
					Integer eventId = (Integer) rawResponse.get("id");

					Long timestamp = (Long) rawResponse.get("timestamp");
					if( timestamp==null ){
						timestamp = (Long) rawResponse.get("eventtime");
						if( timestamp==null)
							timestamp=Instant.now().toEpochMilli();
					}
					Long now = Instant.now().toEpochMilli();


					_log.info("Parked events size:{} key:{}",parkedEvents.size(),callId);
					if(callId==null){
						_log.debug("Continue to fetch next action, ignore this one");
						continue;
					}
					Object parkedEvent = parkedEvents.get(callId);
					_log.debug("Parked event:{}", parkedEvent);
					//fetch event from parked events,
					//search for event id

					Boolean fireAndForget = (Boolean) rawResponse.get(JsonSCIFResponse.FIRE_FORGET);
					if( fireAndForget==null){
						fireAndForget=false;
					}

					_log.debug("Execution time session: {} diff: {}",callId,(now-timestamp));
					executionTimer.update(now-timestamp);

					long execTime = Calendar.getInstance().getTimeInMillis()-timestamp;
					String histogramName="tas.sip.time";
					MetricsServer.get().setTimer(histogramName,execTime);
					String histogramGeneric="execution.time";

					MetricsServer.get().setTimer(histogramGeneric,execTime);
					executionTimer.update(execTime);
					_log.debug("Set timer:{} session:{} value:{}",histogramName,callId, execTime);


					if (parkedEvent != null) {
						List<Map<String, Object>> events = (List<Map<String, Object>>) parkedEvent;
						int idx = 0;
						boolean found = false;
						Map<String,Object> event=null;
						for( int i=0; i< events.size();i++){
							_log.debug("Charging parked id :{} with eventid: {}",events.get(i).get("id"),eventId);
							if( (Integer)events.get(i).get("id")==eventId){
								event = events.get(i);
								idx=i;
							}
						}
						if( event==null ){
							if( events.size()>0) {
								event = events.get(events.size() - 1);
							}
						} else {
							events.remove(idx);
						}
						//for (Map<String, Object> event : events) {
							//if (((Integer) event.get("id")) == eventId) {
								//found event
								_log.debug("Found event:{}", event);
								JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, event);

								JsonSession session = (JsonSession) event.get(JsonSCIFResponse.SESSION);

								if( timestamp!=null && (now-timestamp)>10000 ){
									_log.error("Skip processing message, too old :{}",rawResponse);
								} else {
									Object callObj = event.get(JsonSCIFResponse.CALL);
									if( session!=null && session.getApplicationSession()!=null && callObj instanceof Call) {
										Call call = (Call) callObj;
										//create fake timer which fires imediately
										JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(call, resp);
										_log.debug("Arming timer ...");
										Timer timer = TelecomUtils.createTimer(timerListener, 0, session.getApplicationSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
									} else {
										_log.debug("Execute method inline");
										resp.execute();
									}
								}
//							resp.execute();
								//found = true;
								//remove item from list
								//break;
							//}
							//idx++;
						//}
//						if (found) {
//							events.remove(idx);
//						} else {
//							//handle event non parked
//							JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, null);
//
//						}
					} else {
						if( timestamp!=null && (now-timestamp)>10000 ){
							_log.error("Skip processing message, too old diff:{} :{}",(now-timestamp),rawResponse);
							continue;
						}

						_log.debug("Parked event null, check eventName:{}",eventName);
						if( eventName==null){
							_log.info("Invalid eventName :{} {}",eventName,rawResponse);
							continue;
						}
						if( eventName.equalsIgnoreCase("makeCall")){
							_log.debug("Create new call ...");
							//this is out of the blue event
							//handling out of the blue events

							JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse);
							resp.execute();
							//create fake timer which fires imediately
							//JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(resp.call, resp);
							//_log.debug("Arming timer ...");
							//Timer timer = TelecomUtils.createTimer(timerListener, 0, (ApplicationSession) SipServiceImslet.getInstance().getScifImsletApplicationSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
						} else if (eventName.equalsIgnoreCase("makeSession")){
							_log.debug("Create new session ...");
							JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse);
							resp.execute();
						} else {
							JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, null);

//							if( !fireAndForget ){
//								//return error to flow
//								Map<String,Object> error = SIPJsonFactory.getErrorEvent(rawResponse,"park");
//								String queue = SipCallUser.QUEUE_EGRESS;
//
//								_log.debug("Sending error queue:{} event:{}",queue,rawResponse);
//
//								RedisPool.get().sendToQueue(error, queue);
//							}
						}
					}
				}
			} catch (Exception e){
				e.printStackTrace();
				_log.error("Exception callid:{} rawResponse:{}",callId,rawResponse,e);
			}
		}
	}

	public static void remove(String sipCallId) {
		_log.debug("Remove event:{}, parkedEvents size:{}",sipCallId,parkedEvents.size());
		parkedEvents.remove(sipCallId);
	}

	public void reconnect(){
		try {
			_log.error("Reconnecting to redis {} ... ", Parameters.get().getRedisUrl());
			RedisPool.get().initialize(Parameters.get().getRedisUrl(), Parameters.get().getRedisPoolSize(), 1000);
			if (RedisPool.get().getDB() == null) {
				Thread.sleep(1000);
			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
	}

	private class CleanParkedEvents extends Thread {
		@Override
		public void run(){
			while(true) {
				try {
					checkExpiredEvents();
					Thread.sleep(10000);
				} catch (Exception e){
					_log.error("Exception",e);
				}
			}
		}
	}

	public static Integer getParkedEventsSize(){
		return parkedEvents.size();
	}
}
