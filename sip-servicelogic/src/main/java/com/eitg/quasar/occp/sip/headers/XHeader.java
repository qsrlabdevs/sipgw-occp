package com.eitg.quasar.occp.sip.headers;


import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sip.header.ExtensionHeader;
import java.text.ParseException;

public class XHeader extends gov.nist.javax.sip.header.ParametersHeader
        implements ExtensionHeader {

    private static final Logger _log = LoggerFactory.getLogger(XHeader.class.getName());
    /**
     * Default Constructor
     */
    public XHeader(String headerName) {

        super(headerName);
    }

    public StringBuilder encodeBody(StringBuilder encoding) {
        _log.info("Encode body parameters:{}",this.parameters.toString());
        //System.out.println("Encoding :"+encoding.toString());
        //System.out.println("encode body ... parameters:"+ this.getParameters().toString());
        //return  (new StringBuilder(this.getParameters().toString()));
        return encoding;
    }

    public void setValue(String value) throws ParseException {
        throw new ParseException(value, 0);
    }

}

