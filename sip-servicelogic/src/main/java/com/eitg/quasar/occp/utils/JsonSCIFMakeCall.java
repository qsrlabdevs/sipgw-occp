package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class JsonSCIFMakeCall extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFMakeCall.class.getName());
	private SipParameterSet sipPset=null;
	private CommonParameterSet commPset=null;


	public JsonSCIFMakeCall(Map<String, Object> rawMessage){
		_log.debug("Processing make call action ...");
		Long diff = getTimeDiff(rawMessage);

		String callId = (String)rawMessage.get(SIPJsonFactory.CALLID);

		//fetch queue
		String queue = (String)rawMessage.get("ingressQueue");

		SipCallUser callUser  =(SipCallUser)SipServiceImslet.getInstance().createCall(callId);
		callUser.setQueue(queue);
		call = callUser.getCall();
		JsonSession session = callUser.getJsonSession();

		Boolean fireAndForget = (Boolean) rawMessage.get(JsonSCIFResponse.FIRE_FORGET);
		if( fireAndForget!=null && callUser!=null){
			callUser.setFireAndForget(fireAndForget);
		}

		sipPset = call.getParameterSet(SipParameterSet.class);
		commPset = call.getParameterSet(CommonParameterSet.class);

		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

		try {
			if( rawMessage!=null){
				_log.debug("Starting to set CEC and Header rules");
				//setCEC(rawMessage,sipPset,commPset,sipCallUser);
				setHeaderRules(rawMessage, sipPset, commPset, session);
				addHeaders(rawMessage,sipPset);


				try{
					//6. capabilities
					capabilities=(List<String>)rawMessage.get(CAPABILITIES);
					if( capabilities!=null){
						Collection<SipParameterSet.Capability> outCaps = new ArrayList<>();
						for( String cap : capabilities){
							SipParameterSet.Capability caps = SipParameterSet.Capability.valueOf(SipParameterSet.Capability.class, cap);
							outCaps.add(caps);
						}
						sipPset.setCapablities(null, outCaps);
						_log.debug("Outgoing leg capabilities:{}",outCaps);
					}
				} catch (Exception e){
					e.printStackTrace();
				}

				//7. network ringing tone
				ringingTones = (List<Map<String,Object>>) rawMessage.get(RINGINGTONES);
				//7.1 set new P-Charging-Vector

				//7.2 set history-info

				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);

				List<Map<String,Object>> headers = (List<Map<String,Object>>)rawMessage.get("addHeaders");
				_log.debug("Adding headers:{}",headers);
				for (Map<String, Object> jsonHeader : headers) {
					String name = (String)jsonHeader.get("header");
					String value = (String)jsonHeader.get("value");
					sipPset.setHeader(name, value, null);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.error("Exception",e);
		}


	}
	
	@Override
	public void execute() {
		_log.debug("executing ...");
		//create new call object

		//fetch destination address
		try {
			Contact contact = processAction(action, sipPset);
			String from = (String)action.get("from");
			Address fromAddr = null;
			if( from!=null){
				fromAddr = new Address(from);
			}
			call.makeCall(fromAddr,contact);

		} catch(Exception e){
			_log.error("Exception",e);
		}

		_log.debug("executing ... - DONE");
	}

}
