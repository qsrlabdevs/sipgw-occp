package com.eitg.quasar.occp.sip.headers;


import gov.nist.core.NameValue;
import gov.nist.javax.sip.header.SIPHeader;
import gov.nist.javax.sip.parser.FromParser;
import gov.nist.javax.sip.parser.Lexer;
import gov.nist.javax.sip.parser.ParametersParser;
import gov.nist.javax.sip.parser.TokenTypes;

import java.text.ParseException;


public class XHeaderParser
        extends ParametersParser implements TokenTypes {

    private String headerName;

    public XHeaderParser(String value) {
        super(value);

    }

    protected XHeaderParser(Lexer lexer) {

        super(lexer);

    }

    public void setHeaderName(String value){
        this.headerName=value;
    }


    public SIPHeader parse() throws ParseException {

        if (debug)
            dbg_enter("parse");
        try {
            //headerName(TokenTypes.P_VECTOR_CHARGING);
            XHeader privateHeader = new XHeader(this.headerName);

            try {
                while (lexer.lookAhead(0) != '\n') {
                    this.parseParameter(privateHeader);
                    this.lexer.SPorHT();
                    char la = lexer.lookAhead(0);
                    if (la == '\n' || la == '\0')
                        break;
                    this.lexer.match(';');
                    this.lexer.SPorHT();
                }

            } catch (ParseException ex) {
                throw ex;
            }


            super.parse(privateHeader);
            return privateHeader;
        } finally {
            if (debug)
                dbg_leave("parse");
        }
    }

    protected void parseParameter(XHeader privateHeader) throws ParseException {

        if (debug)
            dbg_enter("parseParameter");
        try {
            NameValue nv = this.nameValue('=');
            privateHeader.setParameter(nv);
        } finally {
            if (debug)
                dbg_leave("parseParameter");
        }



    }



}


