package com.eitg.quasar.occp.sip.servicelogic;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallUser;
import com.hp.opencall.ngin.scif.CallUserFactory;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.imscapi.imslet.ApplicationSession;

/**
 * ServiceFactory allows to create {@link ServiceFactory} service call objects.
 */
public class SipUserFactory implements CallUserFactory {
	/**
	 * A constant holding the name of this service.
	 */
	public static final String NAME = "SipService";
	

	public SipUserFactory() {
	}
	
	public CallUser createUser(Object anExecutionContext, Call aCall) throws ScifException {
		ApplicationSession anAppSession = (ApplicationSession) anExecutionContext;
		return new SipCallUser(aCall, anAppSession);
	}

	public String getName() {
		return NAME;
	}
}
