package com.eitg.quasar.occp.utils;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sip.PeerUnavailableException;
import javax.sip.SipFactory;
import javax.sip.header.ContentTypeHeader;
import javax.sip.header.Header;
import javax.sip.message.Message;

import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.eitg.quasar.occp.sip.servicelogic.SipSessionUser;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.events.RingingPollEvent;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import gov.nist.javax.sip.message.SIPMessage;
import gov.nist.javax.sip.message.SIPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.TelecomUtils;
import com.eitg.quasar.occp.sip.utils.TimerInformation.TIMER_TYPE;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.ngin.scif.events.sip.InfoPollEvent;
import com.hp.opencall.ngin.scif.events.sip.RawContentPollEvent;
import com.hp.opencall.ngin.scif.events.sip.SuccessResponsePollEvent;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.EarlyMediaAuth;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.Capability;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.NrtPrompt;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.NrtPromptSpec;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.resources.IvrContact;
import com.hp.opencall.ngin.scif.resources.MediaOperation;
import com.hp.opencall.ngin.scif.resources.RawMediaCallLeg;
import com.hp.opencall.ngin.scif.resources.RawMrfContact;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerListener;

/**
 * 
 * This message is received from business logic and it contains
 * - header rules subscribed to
 * - header rules vars
 * - events subscribed to
 * - header rules ruleset
 * - timers
 * - capabilities
 * - ringing tone
 * - action
 * 		- type: 0-Abort, 1-forwardCall, 2-MRF
 * 			ABORT:
 * 				- ErrorCode
 * 				- Cause
 * 			ForwardCall
 * 				- uri
 * 				- legName
 * 			MRF:
 * 				- earlyMedia : true/false
 * 				- legName
 */
public abstract class JsonSCIFResponse {
	
	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponse.class.getName());
	
	Map<String, Object> rawMessage;
	Call call;
	
	String rawJson=null;
	List<String> headerRules;
	Map<String,Object> events;
	Map<String,Object> headerRulesVars;
	String headerRuleSelected=null;
	List<Map<String, Object>> timers;
	List<String> capabilities;
	List<String> upstreamCapabilities;
	List<String> requireCapabilities;
	List<Map<String,Object>> ringingTones;
	Map<String,Object> action;
	List<Map<String,Object>> addHeaders;
	
	
	public static final String HEADER_RULES="headerrules";
	public static final String DEL_HEADER="delheader";
	public static final String HEADER_VARS="headerrulevar";
	public static final String REG_HEADER_VAR="registerHeaderVar";

	public static final String HEADER_SELECT="headerrulesselect";
	public static final String EVENTS="events";
	public static final String UBSUB_EVENTS="unsubscribeEvents";
	public static final String TIMERS="timers";
	public static final String TIMEOUT = "timeout";
	public static final String TIMER_NAME = "name";
	public static final String CAPABILITIES = "capabilities";
	public static final String REQUIRE_CAPABILITIES = "requireCapabilities";
	public static final String UP_CAPABILITIES = "upstreamCapabilities";
	public static final String RINGINGTONES = "ringingtones";
	public static final String UI_ANN = "anno_name";
	public static final String ANN_TYPE = "anno_type";
	public static final String ACTION = "action";	
	public static final String ACTION_TYPE="type";
	public static final String ERRORCODE="errorcode";
	public static final String REASON="reason";
	public static final String CAUSE="cause";
	public static final String URIs="uri";
	public static final String EARLYMEDIA="earlymedia";
	public static final String ADD_HEADER = "addHeaders";
	public static final String RAW_CONTENT = "content";
	
	
	public static final String SuccessReponseActionAccept="accept";
	public static final String SuccessReponseActionForward="forward";
	public static final String SuccessReponseActionReject="reject";

	private static final String LEG_NAME = "legname";
	public static final String LEAVE_TYPE = "leaveType";

	public static final String LEG_ACTION = "legaction";
	public static final String LEG_RELEASE = "release";
	public static final String LEG_PERFORMMEDIAOPERATION = "performMediaOperation";
	public static final String LEG_REJECTMEDIAOPERATION = "rejectMediaOperation";

	public static final String CONTENT_TYPE = "ContentType";
	public static final String CONTENT = "Content";

	public static final String EVENTNAME = "eventname";

	public static final String CALL = "call";

	public static final String SESSION = "session";
	public static final String CALL_USER = "calluser";
	public static final String SESSION_USER = "sessionuser";
	public static final String FIRE_FORGET = "fireAndForget";

	public static final String LEG = "leg";

	public static final String POLLEVENT = "pollEvent";
	public static final String CEC_VALUE = "cec";
	public static final String CEC_UPSTREAM = "upstream";
	public static final String CEC_DOWNSTREAM = "downstream";
	public static final String CEC_Q850 = "q850";
	public static final String CEC_Cause = "cause";
	public static final String CEC_Text = "text";
	public static final String CEC_SIP = "sip";
	private static final String TASIP="TASIP";

	public static final String HeaderVar_NewRequestURI="newRequestURI";

	private static Map<String, String> pollEventsClass = new HashMap<String, String>() {{
		put("RingingPollEvent", "com.hp.opencall.ngin.scif.events.RingingPollEvent");
	}};

	private long startTime=Calendar.getInstance().getTimeInMillis();

	enum ActionType {
		AbortCall,
		ForwardCall,
		ConnectMrf,
		LeaveCall
	}
	
	protected Long getTimeDiff(Map<String, Object> rawMessage){
		Long startTime = (Long)rawMessage.get(JsonSCIFRequest.StartTime);
		Long stopTime = Calendar.getInstance().getTimeInMillis();
		Long diff=new Long(-1);
		if( startTime!=null && stopTime!=null){
			diff = stopTime-startTime;
		}
		return diff;
	}
	
	public JsonSCIFResponse(){
		
	}
	
	public JsonSCIFResponse(Call aCall, Map<String, Object> rawMessage, JsonSession session, CallLeg leg){
		Long diff = getTimeDiff(rawMessage);
		_log.debug("Processing response for leg :{}, timediff:{}, rawMessage:",leg,diff,rawMessage);
		call=aCall;
		SipParameterSet sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);


		action = (Map<String,Object>) rawMessage.get(ACTION);
		if( action!=null){
			//get action type
			Integer type = (Integer) action.get(ACTION_TYPE);
			String legAction = (String) action.get(LEG_ACTION);
			if(type!=null && type ==3 && legAction!=null) {
				//this is leg action
				processLegAction(action, leg);
			}
		}
	}

	protected void setHeaderRules(Map<String, Object> rawMessage, SipSessionParameterSet sipPset) {
		//1. set header rules variables subscribed to, these will be sent in next event
		headerRules = (List<String>) rawMessage.get(HEADER_RULES);

		try{
			//3. header rules variables
			headerRulesVars = (Map<String,Object>) rawMessage.get(HEADER_VARS);
			_log.debug("set header rules variables:{} ...",headerRulesVars);
			if(headerRulesVars!=null ){
				for(String key : headerRulesVars.keySet()){
					sipPset.setHeaderRulesVariableValue(key, headerRulesVars.get(key));
				}
			}
		} catch (Exception e){
			_log.error("Exception",e );
		}

		//4. header rule selected
		try{
			headerRuleSelected = (String)rawMessage.get(HEADER_SELECT);
			_log.debug("select header rule:{} ...",headerRuleSelected);
			if(headerRuleSelected!=null )
				sipPset.selectHeaderRulesSet(headerRuleSelected);
		}catch (Exception e){
			_log.error("Exception",e);
		}
	}



	protected void setHeaderRules(Map<String, Object> rawMessage, SipParameterSet sipPset, CommonParameterSet commPset, JsonSession session){
		//1. set header rules variables subscribed to, these will be sent in next event 
		headerRules = (List<String>) rawMessage.get(HEADER_RULES);
		//2. fetch events
		events = (Map<String,Object>) rawMessage.get(EVENTS);
		_log.debug("set events:{} ...",events);
		
		if( events!=null){
			for(String key : events.keySet()){
				try {

					String className=pollEventsClass.get(key);
					if( className==null){
						className="com.hp.opencall.ngin.scif.events.sip."+key;
					}

					_log.debug("Loading class name:{}",className);
					Class myClass = Thread.currentThread().getContextClassLoader().loadClass(className);
					commPset.subscribePollEvent(myClass, events.get(key));

				} catch (ClassNotFoundException e) {
					_log.error("Exception",e);
				}						
			}					
		}


		//unsubscribe events
		events = (Map<String,Object>) rawMessage.get(UBSUB_EVENTS);
		if( events!=null){
			for(String key : events.keySet()){
				try {
					String className=pollEventsClass.get(key);
					if( className==null){
						className="com.hp.opencall.ngin.scif.events.sip."+key;
					}
					_log.debug("Loading class name:{}",className);
					Class myClass = Thread.currentThread().getContextClassLoader().loadClass(className);
					commPset.unsubscribePollEvent(myClass);
				} catch (ClassNotFoundException e) {
					_log.error("Exception",e);
				}
			}
		}
		
		try{
			//3. header rules variables
			headerRulesVars = (Map<String,Object>) rawMessage.get(HEADER_VARS);
			_log.debug("set header rules variables:{} ...",headerRulesVars);
			if(headerRulesVars!=null ){
				for(String key : headerRulesVars.keySet()){
					String value = (String) headerRulesVars.get(key);
					value = value.replaceAll(TASIP, Parameters.get().getTasIp());
					sipPset.setHeaderRulesVariableValue(key, value);
				}
			}
		} catch (Exception e){
			_log.error("Exception",e );
		}

		//register header rules
		_log.debug("JSON session :{}",session);
		if( session!=null) {
			try {
				List<String> headerVar = (List<String>) rawMessage.get(REG_HEADER_VAR);
				if( headerVar==null){
					headerVar=new ArrayList<>();
					headerVar.add("HeaderRule_Warning");
					headerVar.add("HeaderRule_StatusCode");
					headerVar.add("HeaderRule_ZTECause");
				}
				_log.debug("Set header variables:{}",headerVar);
				session.setHeaderVariables(headerVar);
			} catch (Exception e) {
				_log.error("Exception", e);
			}
		}
		
		//4. header rule selected
		try{
			headerRuleSelected = (String)rawMessage.get(HEADER_SELECT);		
			_log.debug("select header rule:{} ...",headerRuleSelected);
			if(headerRuleSelected!=null )
				sipPset.selectHeaderRulesSet(headerRuleSelected);
		}catch (Exception e){
			_log.error("Exception",e);
		}

		if( session!=null) {
			try {
				//5. timers selected
				timers = (List<Map<String, Object>>) rawMessage.get(TIMERS);
				_log.debug("set timers:{} ...", timers);
				if (timers != null) {
					for (Map<String, Object> timer : timers) {
						Integer timeout = (Integer) timer.get(TIMEOUT);
						String name = (String) timer.get(TIMER_NAME);
						TimerListener timerListener = new JsonSCIFTimerListener(call);
						Timer noAnswerTimer = TelecomUtils.createTimer(timerListener, timeout, session.getApplicationSession(), TIMER_TYPE.NO_ANSWER);
					}
				}
			} catch (Exception e) {
				_log.error("Exception",e);
			}
		}


		//set raw content if needed

	}


	protected void setCEC(Map<String, Object> rawMessage, SipParameterSet sipPset, CommonParameterSet commPset, SipCallUser sipCallUser){
		Map<String, Object> cec = (Map<String, Object>)rawMessage.get(CEC_VALUE);
		_log.debug("Set CEC:{}",cec);

		if( cec!=null){
			Map<String, Object> upstreamCEC = (Map<String, Object>)cec.get(CEC_UPSTREAM);

			if( upstreamCEC!=null && upstreamCEC.size()>0){
				_log.debug("Upstream leg:{}",sipCallUser.getUpstreamLeg());
				CEC lUpstreamCEC = sipPset.getCEC(sipCallUser.getUpstreamLeg());
				if( lUpstreamCEC==null){
					lUpstreamCEC = new CEC() {
						SipErrorCause sec=null;
						Q850ErrorCause qec=null;
						@Override
						public SipErrorCause getSipErrorCause() {
							return sec;
						}

						@Override
						public Q850ErrorCause getQ850ErrorCause() {
							return qec;
						}

						@Override
						public List<ErrorCause> getOtherErrorCauses() {
							return null;
						}

						@Override
						public void setSipErrorCause(SipErrorCause sipErrorCause) {
							this.sec = sipErrorCause;
						}

						@Override
						public void setQ850ErrorCause(Q850ErrorCause q850ErrorCause) {
							this.qec = q850ErrorCause;
						}

						@Override
						public void addOtherErrorCause(ErrorCause errorCause) {

						}
					};
					sipPset.setCEC(lUpstreamCEC,sipCallUser.getUpstreamLeg());
				}

				Map<String, Object> q850Value = (Map<String, Object>)upstreamCEC.get(CEC_Q850);
				if(lUpstreamCEC!=null && q850Value!=null){
					Integer lCause = (Integer)q850Value.get(CEC_Cause);
					String lText = (String)q850Value.get(CEC_Text);
					if( lCause==null){
						lUpstreamCEC.setQ850ErrorCause(null);
					} else {
						_log.debug("UP CEC set Q850:{}",q850Value);
						lUpstreamCEC.setQ850ErrorCause(new Q850ErrorCause() {
							@Override
							public Integer getProtocolCause() {
								return lCause;
							}

							@Override
							public String getReasonText() {
								return lText;
							}

							@Override
							public List<GenericParam> getGenericParameters() {
								return null;
							}

						});
					}
				}
				Map<String, Object> sipValue = (Map<String, Object>)upstreamCEC.get(CEC_SIP);
				if(lUpstreamCEC!=null && sipValue!=null){
					Integer lCause = (Integer)sipValue.get(CEC_Cause);
					String lText = (String)sipValue.get(CEC_Text);

					if( lCause==null){
						lUpstreamCEC.setSipErrorCause(null);
					} else {
						_log.debug("UP CEC set SIP:{}",sipValue);
						lUpstreamCEC.setSipErrorCause(new SipErrorCause() {
							@Override
							public Integer getProtocolCause() {
								return lCause;
							}

							@Override
							public String getReasonText() {
								return lText;
							}

							@Override
							public List<GenericParam> getGenericParameters() {
								return null;
							}
						});

					}
				}
			}
			Map<String, Object> downstreamCEC = (Map<String, Object>)cec.get(CEC_DOWNSTREAM);
			if( downstreamCEC!=null && downstreamCEC.size()>0){
				//get downstream legs
				CallLeg[] legs = call.getLegs();
				for( CallLeg leg : legs){
					if( leg.equals(sipCallUser.getUpstreamLeg())){
						continue;
					}
					_log.debug("Downstream leg:{}",leg);
					CEC lCec = sipPset.getCEC(leg);

					if( lCec==null){
						_log.debug("No default CEC defined for leg:{}",lCec);
						lCec = new CEC() {
							SipErrorCause sec=null;
							Q850ErrorCause qec=null;
							@Override
							public SipErrorCause getSipErrorCause() {
								return sec;
							}

							@Override
							public Q850ErrorCause getQ850ErrorCause() {
								return qec;
							}

							@Override
							public List<ErrorCause> getOtherErrorCauses() {
								return null;
							}

							@Override
							public void setSipErrorCause(SipErrorCause sipErrorCause) {
								this.sec = sipErrorCause;
							}

							@Override
							public void setQ850ErrorCause(Q850ErrorCause q850ErrorCause) {
								this.qec = q850ErrorCause;
							}

							@Override
							public void addOtherErrorCause(ErrorCause errorCause) {

							}
						};


						//set CEC for leg
						sipPset.setCEC(lCec, leg);
					}
					Map<String, Object> q850Value = (Map<String, Object>)downstreamCEC.get(CEC_Q850);
					if( q850Value!=null){
						_log.debug("DOWN CEC Q850:{}",q850Value);
						Integer lCause = (Integer)q850Value.get(CEC_Cause);
						String lText = (String)q850Value.get(CEC_Text);
						if(lCause==null){
							lCec.setQ850ErrorCause(null);
						} else {
							lCec.setQ850ErrorCause(new Q850ErrorCause() {
								@Override
								public Integer getProtocolCause() {
									return lCause;
								}

								@Override
								public String getReasonText() {
									return lText;
								}

								@Override
								public List<GenericParam> getGenericParameters() {
									return null;
								}
							});
						}
					}
					Map<String, Object> sipValue = (Map<String, Object>)downstreamCEC.get(CEC_SIP);
					if( sipValue!=null){
						Integer lCause = (Integer)sipValue.get(CEC_Cause);
						String lText = (String)sipValue.get(CEC_Text);

						if( lCause==null){
							lCec.setSipErrorCause(null);
						} else {
							_log.debug("DOWN CEC SIP:{}",sipValue);
							lCec.setSipErrorCause(new SipErrorCause() {
								@Override
								public Integer getProtocolCause() {
									return lCause;
								}

								@Override
								public String getReasonText() {
									return lText;
								}

								@Override
								public List<GenericParam> getGenericParameters() {
									return null;
								}
							});

						}

					}
				}
			}
		}
	}
	
	protected void setRingingTones(List<Map<String,Object>> pRingingTones, SipParameterSet sipPset, Contact contact){
		_log.debug("setRingingTones {}",pRingingTones);
		sipPset.enable181(false);
		// set To callCenterDN
		NrtPromptSpec[] prompts =null;
		int index=0;
		prompts = new NrtPromptSpec[pRingingTones.size()];
		
		for( Map<String, Object> tone : pRingingTones){
			String uiAnn = (String)tone.get(UI_ANN);	
			String type = (String) tone.get(ANN_TYPE);
			MediaOperation lPrompt = new MediaOperation(SipServiceImslet.NAME + "/" + uiAnn);
			_log.debug("Set tone:{} type:{}",uiAnn,type);
			NrtPrompt prompt = NrtPrompt.valueOf(type);
			prompts[index] = new NrtPromptSpec(prompt, lPrompt);
			index++;
		}

		_log.debug("Prompts:"+prompts+" length:"+prompts.length);
		sipPset.resetNetworkRingTone();

		try{
			IvrContact lIvr = ScifFactory.instance().getResourceContact(call, IvrContact.class, "MsmlMrfServer");
			sipPset.setNetworkRingTone(lIvr, null, prompts);
			
			if( contact instanceof TerminalContact){
				TerminalContact termContact = (TerminalContact) contact;
				termContact.setProperty(SipParameterSet.CONTACT_NETWORK_RING_TONE , Boolean.TRUE); 				
			}
		}catch(Exception e){
			_log.error("error getting mrfServer" + e.getLocalizedMessage());
		}		
	}
	
	protected Contact processAction(Map<String,Object> action, SipParameterSet sipPset){
		Contact ret=null;
		Integer type = (Integer)action.get(ACTION_TYPE);
		Integer errorCode = (Integer) action.get(ERRORCODE);
		String cause = (String) action.get(CAUSE);
		String reason = (String) action.get(REASON);
		Cause ntwkCause = Cause.NONE;
		if( cause!=null)
			ntwkCause= Cause.valueOf(cause);
		Object uriObj = (Object) action.get(URIs);

		Boolean earlyMedia = (Boolean) action.get(EARLYMEDIA);
		String legName = (String) action.get(LEG_NAME);
		String leaveType = (String) action.get(LEAVE_TYPE);
		//action type can be:
		//0 - abort calls
		//1 - forwardCall
		//2 - connect to announcement
		//3 - leave call
		switch(type){
		case 0:
			_log.debug("Sending abort call errorCode:{}, cause:{} reason:{}",errorCode,ntwkCause,reason);
			//this is abort call,
			if( errorCode!=null) {
				sipPset.setErrorCode(errorCode);
			}
			if( ntwkCause!=null ) {
				call.abortCall(ntwkCause);
			} else {
				call.abortCall(Cause.NONE);
			}

			break;
		case 1:
			if( uriObj instanceof  String){
				Address address = new Address((String)uriObj);
				TerminalContact termContact = new TerminalContact(address);
				ret = termContact;
			} else if ( uriObj instanceof List){
				List<String> uris = (List<String>) uriObj;
				ContactList contactList = new ContactList();

				for(String uri: uris){
					Address address = new Address((String)uri);
					TerminalContact termContact = new TerminalContact(address);
					contactList.addContact(termContact);
				}
				ret = contactList;
			}
			_log.debug("Sending forward call contact:{}",ret);
			break;
		case 2:
			RawMrfContact mrfContact=null;
			try {
				mrfContact = ScifFactory.instance().getResourceContact(
							call,
							RawMrfContact.class,
							"RawMrfServer");
				
				_log.debug("Play announcements earlyMedia:{}, legName:{}",earlyMedia,legName);
				if( earlyMedia!=null && earlyMedia==true){
					mrfContact.setEarlyMedia(true);
					mrfContact.setProperty(SipParameterSet.CONTACT_EARLY_MEDIA_AUTH, EarlyMediaAuth.SENDONLY);					
				}
				if( legName!=null){
					mrfContact.setLegName(legName);
				} else {
					mrfContact.setLegName("term");
				}
				
				_log.debug("MrfContact:"+mrfContact.toString());
				ret = mrfContact;
			} catch (ScifException e) {
				// TODO Auto-generated catch block
				_log.error("Cannot fetch MRF contact:"+e.getMessage());
			}			
			_log.debug("Sending forward call to IVR contact:{}",ret);
			break;

			case 3:
				if( uriObj instanceof  String){
					Address address = new Address((String)uriObj);
					TerminalContact termContact = new TerminalContact(address);
					ret = termContact;
				} else if ( uriObj instanceof List){
					List<String> uris = (List<String>) uriObj;
					ContactList contactList = new ContactList();

					for(String uri: uris){
						Address address = new Address((String)uri);
						TerminalContact termContact = new TerminalContact(address);
						contactList.addContact(termContact);
					}
					ret = contactList;
				}
				if( leaveType.equalsIgnoreCase("proxy")){
					_log.debug("Set leaveCall - proxy mode");
					sipPset.setLeaveCallProxyBehavior(true);
				}
				_log.debug("Sending leave call contact:{}",ret);
				break;
			case 4:
				if( uriObj instanceof  String){
					Address address = new Address((String)uriObj);
					TerminalContact termContact = new TerminalContact(address);
					ret = termContact;
				} else if ( uriObj instanceof List){
					List<String> uris = (List<String>) uriObj;
					ContactList contactList = new ContactList();

					for(String uri: uris){
						Address address = new Address((String)uri);
						TerminalContact termContact = new TerminalContact(address);
						contactList.addContact(termContact);
					}
					ret = contactList;
				}
				break;
		}
		
		return ret;
	}
	
	protected void processLegAction(Map<String, Object> action, CallLeg leg){
		_log.debug("legAction:{} leg:{}",action,leg);
		//is assumed that leg is of type RawMedia
		if( leg instanceof RawMediaCallLeg){
			RawMediaCallLeg rawMedia = (RawMediaCallLeg) leg;
			String legAction = (String) action.get(LEG_ACTION);
			Map<String, Object> legActionMap = (Map<String, Object> )action.get(legAction);
			_log.debug("actionLeg:{} map:{}",legAction,legActionMap);
			if( legActionMap!=null){
				if( legAction.equalsIgnoreCase(LEG_RELEASE)){
					_log.debug("Action - RELEASE leg ...");
					//fetch cause
					String cause = (String)legActionMap.get(CAUSE);
					Cause relCause = Cause.NONE;
					if( cause!=null){
						relCause = Cause.valueOf(cause);
					}
					try {
						_log.debug("releasing leg:{}",rawMedia.toString());
						leg.release(relCause);
					} catch (ScifException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (legAction.equalsIgnoreCase(LEG_PERFORMMEDIAOPERATION)){
					_log.debug("Performing media operation ...");
					String contentType = (String)legActionMap.get(CONTENT_TYPE);
					String content = (String)legActionMap.get(CONTENT);
					
					ContentTypeHeader lContentTypeHeader;
					try {
					    lContentTypeHeader = (ContentTypeHeader) SipFactory.getInstance().createHeaderFactory()
						    .createHeader("Content-Type", contentType);

					    _log.debug("->sendMediaRequest() created ContentTypeHeader value:" + lContentTypeHeader);
					    SipRawMessageContent lSipRawMessageContent = new SipRawMessageContent(lContentTypeHeader, content);

					    rawMedia.performMediaOperation(lSipRawMessageContent);
					} catch (PeerUnavailableException | ParseException | ScifException e) {
					    _log.error("Error on play ann:"+e.getMessage());
					}
				}	
			}
		} else {
			String legAction = (String) action.get(LEG_ACTION);
			Map<String, Object> legActionMap = (Map<String, Object> )action.get(legAction);
			_log.debug("Default callLeg actionLeg:{} map:{}",legAction,legActionMap);

			String cause = (String)legActionMap.get(CAUSE);
			Cause relCause = Cause.NONE;
			if( cause!=null){
				relCause = Cause.valueOf(cause);
			}
			try {
				_log.debug("releasing leg:{}",leg.toString());
				leg.release(relCause);
			} catch (ScifException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				_log.error("Release Exception",e);
			}
		}
	}
	



	public abstract void execute();

	static JsonSCIFResponse getResponse(Map<String, Object> rawResponse) {
		_log.debug("rawResponse:{} eventData:{}",rawResponse);

		JsonSCIFResponse resp=null;

		String eventName = (String) rawResponse.get(EVENTNAME);
		Integer eventId = (Integer) rawResponse.get(JsonSCIFRequest.EventId);


		if( eventName.equalsIgnoreCase(JsonSCIFRequest.MAKECALL)){
			resp = new JsonSCIFMakeCall(rawResponse);
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.MAKESESSION)){
			resp = new JsonSCIFMakeSession(rawResponse);
		}
		return resp;
	}


	public static JsonSCIFResponse getResponse(Map<String, Object> rawResponse, Map<String, Object> eventData){
		_log.debug("rawResponse:{} eventData:{}",rawResponse,eventData);


		JsonSCIFResponse resp=null;

		try {
			String eventName = (String) rawResponse.get(EVENTNAME);
			Integer eventId = (Integer) rawResponse.get(JsonSCIFRequest.EventId);

			Object callObj = eventData.get(CALL);
			Call call=null;
			Session sipSession=null;
			if( callObj instanceof Call) {
				call = (Call) callObj;
			} else if( callObj instanceof Session){
				sipSession=(Session) callObj;
			}
			JsonSession session = (JsonSession) eventData.get(SESSION);

			SipCallUser sipCallUser = (SipCallUser) eventData.get(JsonSCIFResponse.CALL_USER);
			SipSessionUser sipSessionUser = (SipSessionUser) eventData.get(JsonSCIFResponse.SESSION_USER);
			_log.debug("callUser:{}", sipCallUser);

			Boolean fireAndForget = (Boolean) rawResponse.get(FIRE_FORGET);
			if (fireAndForget != null && sipCallUser != null) {
				sipCallUser.setFireAndForget(fireAndForget);
			}

			if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLSTART)) {
				resp = new JsonSCIFResponseCallStart(call, rawResponse, session, sipCallUser);
			} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLPOLL)) {
				ScifEvent pollEvent = (ScifEvent) eventData.get(POLLEVENT);
				resp = new JsonSCIFResponseCallPoll(call, rawResponse, session, sipCallUser, pollEvent);
			} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLANSWERED) ||
					eventName.equalsIgnoreCase(JsonSCIFRequest.CALLEARLYANSWERED) ||
					eventName.equalsIgnoreCase(JsonSCIFRequest.MEDIAOPERATION)) {
				CallLeg leg = (CallLeg) eventData.get(LEG);
				resp = new JsonSCIFResponseLegAction(call, rawResponse, session, sipCallUser, leg);
			} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.SESSIONSTART) ){
				resp = new JSONSCIFSessionResponse(rawResponse, sipSessionUser);
			}
		} catch (Exception e){
			_log.error("Exception processing message:{}",rawResponse,e);
			_log.error("Exception",e);
		}
		
		return resp;
	}


	protected void removeHeaders(Map<String,Object> rawMessage, SipParameterSet sipParameterSet){
		List<String> headers = (List<String>)rawMessage.get("removeHeaders");
		_log.debug("Removing headers:{}",headers);
		if( headers==null){
			return;
		}
		SIPRequest request = (SIPRequest) sipParameterSet.getInitialInvite();
		for( String header : headers){
			request.removeFirst(header);
		}
	}

	protected void addHeaders(Map<String,Object> rawMessage, SipParameterSet sipParameterSet){
		List<Map<String,Object>> headers = (List<Map<String,Object>>)rawMessage.get("addHeaders");
		_log.debug("Adding headers:{}",headers);
		if( headers==null){
			return;
		}

		List<Address> routeSet = new ArrayList<>();
		for(int i=0;i<headers.size();i++){
			Map<String, Object> header = headers.get(i);

			String name = (String)header.get("header");
			String value =(String) header.get("value");

			value = value.replaceAll(TASIP,Parameters.get().getTasIp());

			try {
				if (name != null && value != null) {
					if(name.equalsIgnoreCase("Route")){
						Address address = new Address(value);
						routeSet.add(address);
					} else {
						//sipParameterSet.setHeader(name, value, null);
					}
				}
			}catch(Exception e){
				_log.error("Exception",e);
			}
		}

		if( routeSet.size()>0){
			sipParameterSet.setRouteSet(routeSet);
		}

	}

	protected void addHeadersOld(Map<String,Object> rawMessage, SipParameterSet sipParameterSet){
		List<Map<String,Object>> headers = (List<Map<String,Object>>)rawMessage.get("addHeaders");
		_log.debug("Adding headers:{}",headers);
		if( headers==null){
			return;
		}

		boolean isB2BUA=true;

		Map<String,Object> action = (Map<String,Object>)rawMessage.get("action");
		if(action!=null){
			String leaveType = (String) action.get(LEAVE_TYPE);
			if( leaveType!=null && leaveType.equalsIgnoreCase("proxy")){
				isB2BUA=false;
			}
		}

		_log.debug("addHeaders isB2BUA:{}",isB2BUA);


		List<Address> routeSet = new ArrayList<>();
		List<String> historyInfo = new ArrayList<>();
		for (int i = 0; i < headers.size(); i++) {
			Map<String, Object> header = headers.get(i);

			String name = (String) header.get("header");
			String value = (String) header.get("value");

			value = value.replaceAll(TASIP, Parameters.get().getTasIp());

			try {
				if (name != null && value != null) {
					if (name.equalsIgnoreCase("Route")) {
						Address address = new Address(value);
						routeSet.add(address);
					} else{
						if( isB2BUA ) {
							if (name.equalsIgnoreCase("History-Info")) {
								historyInfo.add(value);
							} else {
								sipParameterSet.setHeader(name, value, null);
							}
						} else {
							_log.debug("Proxy mode, add header ...");
							SIPMessage message = (SIPMessage) sipParameterSet.getInitialInvite();
							try {
								Header sipHeader = SipServiceImslet.getInstance().getHeaderFactory().createHeader((String) header.get("header"), (String) header.get("value"));
								message.addFirst(sipHeader);

								_log.debug("Message addFirst:{}", header);
							} catch (Exception e1) {
								_log.error("Exception adding header", e1);
							}
						}
					}
				}
			} catch (Exception e) {
				_log.error("Exception", e);
			}
		}

		if (routeSet.size() > 0) {
			sipParameterSet.setRouteSet(routeSet);
		}
		_log.debug("Set history info:{}", historyInfo);
		if (historyInfo.size() > 0) {
			try {

				sipParameterSet.addHistoryInfoEntries(historyInfo);

				Collection<Capability> outCaps = new ArrayList<>();
				outCaps.add(Capability.HISTORY_INFO);
				sipParameterSet.setCapablities(null, outCaps);
				_log.debug("Set capabilities:{}", outCaps);
			} catch (Exception e) {
				_log.error("Exception on set HistoryInfo:{}", e);
			}
		}

	}

	protected void addHeaders(Map<String,Object> rawMessage, SipSessionParameterSet sipParameterSet){
		List<Map<String,Object>> headers = (List<Map<String,Object>>)rawMessage.get("addHeaders");
		_log.debug("Adding headers:{}",headers);
		if( headers==null){
			return;
		}


		List<Address> routeSet = new ArrayList<>();
		for(Map<String,Object> header : headers){
			String name = (String)header.get("header");
			String value =(String) header.get("value");

			try {
				if (name != null && value != null) {
					if(name.equalsIgnoreCase("Route")){
						Address address = new Address(value);
						routeSet.add(address);
					} else {
						sipParameterSet.setHeader(name, value, null);
					}
				}
			}catch(Exception e){
				_log.error("Exception",e);
			}
		}
		if( routeSet.size()>0){
			sipParameterSet.setRouteSet(routeSet);
		}

	}

	public long getTimeDiff(){
		return Calendar.getInstance().getTimeInMillis()-startTime;
	}
}
