package com.eitg.quasar.occp.sip.businesslogic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.hp.opencall.ngin.timer.Timer;

/**
 * 
 * This class it is used to periodically fetch emscli parameters
 *
 */
public class Parameters extends Thread {
	private static final Logger _log = LoggerFactory.getLogger(Parameters.class.getName());
	
	private static final String REDIS_URL = "redis.url";
	private static final String REDIS_POOL_SIZE = "redis.poolsize";
	private static final String REDIS_EGRESS_QUEUE = "redis.queue.egress";
	private static final String REDIS_INGRESS_QUEUE = "redis.queue.ingress";
	private static final String TAS_IP = "tas.ip";
	private static final String METRICS_PORT = "metrics.httpport";
	private  String sipSessionNack="";


	public void setSipSessionNack(String value){
		this.sipSessionNack = value;
	}

	public String getSipSessionNack(){
		return this.sipSessionNack;
	}


	private Integer httpPort=50000;
	private SipServiceImslet imslet;

	private int parametersRefreshTimer=30000;

	public String getRedisUrl() {
		return redisUrl;
	}

	public void setRedisUrl(String redisUrl) {
		this.redisUrl = redisUrl;
	}

	public String getRedisPoolSize() {
		return redisPoolSize;
	}

	public void setRedisPoolSize(String redisPoolSize) {
		this.redisPoolSize = redisPoolSize;
	}

	public String getRedisQueueEgress() { return this.redisQueueEgress ; }

	public String getRedisQueueIngress() { return this.redisQueueIngress ; }

	private String redisUrl;
	private String redisPoolSize;
	private String redisQueueEgress ;
	private String redisQueueIngress ;
	private String tasIp;


	public Parameters(SipServiceImslet imslet){
		this.imslet=imslet;		
	}
	
	private static Parameters instance=null;

	private static String mImsName;

	
	static public void initializeInstance(SipServiceImslet imslet){
		instance = new Parameters(imslet);
		instance.reloadConfig();
		instance.start();
	}
	
	static public  Parameters get(){
		
		return instance;
	}
	
	private void reloadConfig() {
	       reloadParameters();
	       reloadConfigFiles();
	}
	
	private void reloadConfigFiles() {
	}
	
	private void reloadParameters() {
        if( this.imslet==null){
        	return;
        }

        try {
            this.redisUrl = this.imslet.getImsletContext().getInitParameter(REDIS_URL);
            _log.debug("redisUrl:{}",redisUrl);
        } catch (Exception e) {
            _log.error("Error in loading redisUrl");
        }
        try {
            this.redisPoolSize = this.imslet.getImsletContext().getInitParameter(REDIS_POOL_SIZE);
            _log.debug("redisPoolSize:{}",redisPoolSize);
        } catch (Exception e) {
            _log.error("Error in loading redisPoolSize");
        }

		try {
			this.redisQueueEgress = this.imslet.getImsletContext().getInitParameter(REDIS_EGRESS_QUEUE);
			_log.debug("redisQueueEgress:{}",redisQueueEgress);
		} catch (Exception e) {
			_log.error("Error in loading redisQueueEgress");
		}


		try {
			this.redisQueueIngress = this.imslet.getImsletContext().getInitParameter(REDIS_INGRESS_QUEUE);
			_log.debug("redisQueueIngress:{}",redisQueueIngress);
		} catch (Exception e) {
			_log.error("Error in loading redisQueueIngress");
		}

		try {
			this.tasIp = this.imslet.getImsletContext().getInitParameter(TAS_IP);
			_log.debug("tasIp:{}",tasIp);
		} catch (Exception e) {
			_log.error("Error in loading tasIp");
		}

		try {
			this.httpPort = Integer.parseInt(this.imslet.getImsletContext().getInitParameter(METRICS_PORT));
			_log.debug("metrics port:{}",httpPort);
		} catch (Exception e) {
			_log.error("Error in loading ,metrics port");
		}



	}


	public void timeout(Timer timer) {		
        reloadConfig();

		if( timer!=null){
			timer.cancel();
		}		
	}


	@Override
	public String toString() {
		return "Parameters ";
	}


	public int getParametersRefreshTimer() {
		return parametersRefreshTimer;
	}


	public static void setImsName(String imsname) {
		mImsName = imsname; 
	}

	public static String getImsName() {
		return mImsName ;
	}


	public String getTasIp(){
		return tasIp;
	};

	public Integer getHttpPort() {
		return this.httpPort;
	}


	public void run(){
		while(!imslet.isShouldStop()){
			_log.debug("Reload config parameters");
			reloadConfig();
			
			try {
				_log.debug("Sleeping: "+parametersRefreshTimer + " seconds");
				Thread.sleep(parametersRefreshTimer*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



	
	
}
