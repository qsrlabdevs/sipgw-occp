package com.eitg.quasar.occp.utils;

import java.util.Map;

import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.ScifException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;

public class JsonSCIFResponseLegAction extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseLegAction.class.getName());
	CallLeg leg;
	SipParameterSet sipPset=null;
	
	
	public JsonSCIFResponseLegAction(Call aCall, Map<String, Object> rawMessage, JsonSession session, SipCallUser sipCallUser, CallLeg leg){
		Long diff = getTimeDiff(rawMessage);
		_log.debug("Processing response for leg :{}, timediff:{}, rawMessage:",leg,diff,rawMessage);
		this.call=aCall;
		this.leg = leg;
		
		sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		action = (Map<String,Object>) rawMessage.get(ACTION);
		setCEC(rawMessage,sipPset,commPset,sipCallUser);
	}
	
	@Override
	public void execute() {
		_log.debug("Executing ...");
		if( action!=null){
			//get action type
			Integer type = (Integer) action.get(ACTION_TYPE);
			String legAction = (String) action.get(LEG_ACTION);
			if(type!=null && type ==3 && legAction!=null) {
				//this is leg action
				processLegAction(action, leg);
			} else {
				try {
					Contact newDest = processAction(action, sipPset);
					if (newDest != null) {
						call.forwardCall(newDest);
					}
				} catch (ScifException e){
					_log.error("Exception",e);
				}
			}
		}		
		_log.debug("Executing ... - DONE");
	}

}
