package com.eitg.quasar.occp.sip.utils;

import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.sip.headers.XHeader;
import com.eitg.quasar.occp.sip.headers.XHeaderParser;
import com.eitg.quasar.occp.utils.JsonSCIFRequest;
import com.eitg.quasar.occp.utils.JsonSCIFResponse;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import gov.nist.core.NameValue;
import gov.nist.core.NameValueList;
import gov.nist.javax.sdp.MediaDescriptionImpl;
import gov.nist.javax.sdp.SessionDescriptionImpl;
import gov.nist.javax.sdp.fields.AttributeField;
import gov.nist.javax.sdp.fields.BandwidthField;
import gov.nist.javax.sdp.parser.SDPAnnounceParser;
import gov.nist.javax.sip.address.AddressFactoryImpl;
import gov.nist.javax.sip.address.SipUri;
import gov.nist.javax.sip.header.*;
import gov.nist.javax.sip.header.ims.*;
import gov.nist.javax.sip.message.MultipartMimeContent;
import gov.nist.javax.sip.message.SIPMessage;
import gov.nist.javax.sip.message.SIPRequest;
import gov.nist.javax.sip.parser.ParametersParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sip.PeerUnavailableException;
import javax.sip.SipFactory;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.address.TelURL;
import javax.sip.address.URI;
import javax.sip.header.*;
import javax.sip.message.Message;
import javax.sip.message.MessageFactory;
import javax.sip.message.Response;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;

public class SIPJsonFactory {
    //load data from JSON and instantiate as SIP message
    private static Logger log = LoggerFactory.getLogger(SIPJsonFactory.class);

    private static int callIdCounter=0;
    public static final String CALLID="callid";
    public static final String HEADER_RULES="headerrules";
    public static final String HEADER_VARS="headerrulevar";
    public static final String HEADER_SELECT="headerrulesselect";
    public static final String EVENTS="events";
    public static final String TIMERS="timers";
    public static final String TIMEOUT = "timeout";
    public static final String TIMER_NAME = "name";
    public static final String CAPABILITIES = "capabilities";
    public static final String RINGINGTONES = "ringingtones";
    public static final String UI_ANN = "anno_name";
    public static final String ANN_TYPE = "anno_type";
    public static final String ACTION = "action";
    public static final String ACTION_TYPE="type";
    public static final String ERRORCODE="errorcode";
    public static final String CAUSE="cause";
    public static final String URIs="uri";
    public static final String EARLYMEDIA="earlymedia";


    public static final String ApplicationServer="as";
    public static final String EventTime="eventtime";
    public static final String InitialLeg="initialleg";

    public static final String Content="content";
    public static final String Message="message";
    public static final String Method="method";
    public static final String Status="status";
    public static final String RUri="r-uri";
    public static final String SIP="SIP";
    public static final String CallLegs="legs";
    public static final String CallState="state";
    public static final String Call="call";
    public static final String Event="event";

    public static final String EventName="name";
    public static final String CallStart="callStart";
    public static final String CallPoll="callPoll";
    public static final String CallAnswered="callAnswered";
    public static final String CallEarlyAnswered="callEarlyAnswered";

    public static final String CS_CAUSE="cause";
    public static final String CS_LEG="leg";
    public static final String CS_CONTACT="contact";
    public static final String Name="name";
    public static final String SIPAddress="address";
    public static final String Type="type";
    public static final String SEND_ACTION="sendAction";
    public static final String OUT_CAPABILITIES= "outCapabilities";


    public static final String SuccessReponseActionAccept="accept";
    public static final String SuccessReponseActionForward="forward";
    public static final String SuccessReponseActionReject="reject";
    public static final String Request = "request";
    public static final String Response = "response";
    public static final String Body = "body";

    public static final String QUEUE = "queue";

    public static final String SessionInitialInvite="initialInvite";
    public static final String SessionSIPInitialInvite="SIPInitialInvite";
    public static final String MessageBody="message";
    public static final String MessageBodyType="messageType";
    public static final String MessageReceived="SIPMessage";
    public static final String MessageTypeReceived="SIPMessageType";

    public static final String SIPHelper="SIPHelper";
    public static final String StartTime="starttime";


    private static HeaderFactory headerFact=null;
    private static MessageFactory messageFact=null;
    private static AddressFactory addressFact=null;

    private static List<String> skipParseHeaders = new ArrayList<>() ;

    public static HeaderFactory getHeaderFactory(){
        if(headerFact ==null) {
            try {
                headerFact = SipFactory.getInstance().createHeaderFactory();
            } catch (PeerUnavailableException e) {
                e.printStackTrace();
            }
        }
        return headerFact;
    }

    public static MessageFactory getMessageFactory(){
        if(messageFact ==null) {
            try {
                messageFact = SipFactory.getInstance().createMessageFactory();
            } catch (PeerUnavailableException e) {
                e.printStackTrace();
            }
        }
        return messageFact;
    }

    public static AddressFactory getAddressFactory(){
        if(addressFact ==null) {
            try {
                addressFact = SipFactory.getInstance().createAddressFactory();
            } catch (PeerUnavailableException e) {
                e.printStackTrace();
            }
        }
        return addressFact;
    }

    public static Message getMessage(String path){
        Map<String, Object> sipMessage = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            try (InputStream io = new FileInputStream(path)) {
                sipMessage = mapper.readValue(io, HashMap.class);
            }
        } catch (Exception e){
            e.printStackTrace();
        }


        String body = getSIPMessageBodyReceived(sipMessage);
        String msgType = getSIPMessageTypeReceived(sipMessage);

        boolean request=true;
        if( msgType!=null && msgType.equalsIgnoreCase("Response")){
            request=false;
        }

        javax.sip.message.Message message =getMessage(request, body);

        Map<String, Object> sipHeaders = (Map<String, Object>)sipMessage.get("SIP");

        updateMessage(sipHeaders, message);

        return message;

    }

    public static Message jsonToMessage(String json){
        Map<String, Object> sipMessage = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            sipMessage = mapper.readValue(json, HashMap.class);
        } catch (Exception e){
            e.printStackTrace();

            return null;
        }



        String body = getSIPMessageBodyReceived(sipMessage);
        String msgType = getSIPMessageTypeReceived(sipMessage);

        boolean request=true;
        if( msgType!=null && msgType.equalsIgnoreCase("Response")){
            request=false;
        }

        javax.sip.message.Message message =getMessage(request, body);

        Map<String, Object> sipHeaders = (Map<String, Object>)sipMessage.get("SIP");

        updateMessage(sipHeaders, message);

        return message;

    }

    public static Message jsonToMessage(Map<String,Object> sipMessage){

        String body = getSIPMessageBodyReceived(sipMessage);
        String msgType = getSIPMessageTypeReceived(sipMessage);

        boolean request=true;
        if( msgType!=null && msgType.equalsIgnoreCase("Response")){
            request=false;
        }

        Map<String, Object> sipHeaders = (Map<String, Object>)sipMessage.get("SIP");

        javax.sip.message.Message message =getMessage(request, body);

//        if( message==null){
//            log.error("Couldn't instantiate message based on json, exit");
//            return message;
//        }

        updateMessage(sipHeaders, message);

        return message;

    }


    private static void updateMessage(Map<String, Object> sipHeaders, Message sipMessage){
        for( Map.Entry<String, Object> headerJson : sipHeaders.entrySet() ){
            String headerName = headerJson.getKey();
            Object valueHdr = headerJson.getValue();
            String value="";
            Map<String, Object> headerValue=null;
            if( valueHdr instanceof Map){
                headerValue = (Map<String, Object>)valueHdr;
                value = (String)headerValue.get("value");
            } else {
                continue;
            }

            if( value!=null) {
                try {
                    if( headerName.equalsIgnoreCase("R-URI") ) {
                        if( sipMessage instanceof SIPRequest) {
                            SIPRequest req = (SIPRequest) sipMessage;
                            URI uri = getAddressFactory().createURI(value);
                            req.setRequestURI(uri);
                        }
                    } else if(headerName.equalsIgnoreCase("content")) {
                        //in case there is content, then set it specifically
                        setContent(sipMessage, headerValue);
                    } else {
                        Header hdr = getHeaderFactory().createHeader(headerName, value);
                        //remove message header and replace it with the new value
                        sipMessage.removeHeader(headerName);
                        sipMessage.addHeader(hdr);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    protected static String getSIPMessageBodyReceived(Map<String, Object> message) {
        Map<String, Object> sipMessage = (Map<String, Object>) message.get("SIP");
        if( sipMessage!=null){
            sipMessage = (Map<String, Object>)sipMessage.get("message");

        }
        if(sipMessage==null){
            return null;
        }
        List<Object> bodyV = (List<Object>) sipMessage.get(Body);
        if( bodyV!=null && bodyV.size()>0) {
            return (String)bodyV.get(0);
        }
        return null;
    }

    protected static String getSIPMessageTypeReceived(Map<String, Object> message) {
        Map<String, Object> sipMessage = (Map<String, Object>) message.get("SIP");
        if( sipMessage!=null){
            sipMessage = (Map<String, Object>)sipMessage.get("message");
        }
        if(sipMessage==null){
            return null;
        }
        List<Object> bodyV = (List<Object>) sipMessage.get(Type);
        if( bodyV!=null && bodyV.size()>0) {
            return (String)bodyV.get(0);
        }
        return null;
    }


    public static Map<String, Object> sipMessageToJson(Message message){
        //build MAP object for message received
        Map<String, Object> json = new HashMap<>();
        Map<String, Object> sip = new HashMap<>();
        json.put("SIP",sip);

        Map<String, Object> msg = new HashMap<>();
        sip.put("message",msg);

        if( message instanceof javax.sip.message.Request){
            List<String> type = new ArrayList<>();
            type.add("Request");
            msg.put("type",type);
            javax.sip.message.Request sipRequest = (javax.sip.message.Request) message;
            List<String> method = new ArrayList<>();
            method.add(sipRequest.getMethod());
            msg.put("method",method);

        } else if (message instanceof javax.sip.message.Response){
            List<String> type = new ArrayList<>();
            type.add("Response");
            msg.put("type",type);

            javax.sip.message.Response sipResponse = (javax.sip.message.Response) message;
            List<String> method = new ArrayList<>();
            method.add(Integer.toString(sipResponse.getStatusCode()));
            msg.put("status",method);

        }

//        List<String> body = new ArrayList<>();
//        body.add(message.toString());
//        msg.put("body",body);

        fillSipJsonFields(message ,sip);



        return json;

    }



    public static void fillSipJsonFields(Message sipMessage, Map<String, Object> sipContainer){
        Map<String, Object> hdr = getFrom(sipMessage);
        if( hdr!=null){
            sipContainer.put("From",hdr);
        }
        hdr = getTo(sipMessage);
        if( hdr!=null){
            sipContainer.put("To",hdr);
        }
        hdr = getPCV(sipMessage);
        if( hdr!=null){
            sipContainer.put("P-Charging-Vector",hdr);
        }
        hdr = getPCFA(sipMessage);
        if( hdr!=null){
            sipContainer.put("P-Charging-Function-Address",hdr);
        }

        hdr = getPANI(sipMessage);
        log.debug("P-Access-Network-Info:{}",hdr);
        if( hdr!=null){
            sipContainer.put(PAccessNetworkInfoHeader.NAME,hdr);
        }

        hdr=getHeader( (SIPHeader) sipMessage.getHeader(PVisitedNetworkIDHeader.NAME ));
        if( hdr!=null){
            sipContainer.put(PVisitedNetworkIDHeader.NAME,hdr);
        }

        List<Map<String,Object>> listHdr= getPAI(sipMessage);
        if( listHdr!=null){
            sipContainer.put("P-Asserted-Identity",listHdr);
        }


        try {
            hdr = getRUri(sipMessage);
            if (hdr != null) {
                sipContainer.put("R-URI", hdr);
            }
        } catch (Exception e){
            log.error("Exception",e);
        }

        listHdr = getHistoryInfo(sipMessage);
        if( listHdr!=null && listHdr.size()>0){
            sipContainer.put("History-Info",listHdr);
        }

        listHdr = getDiversion(sipMessage);
        if( listHdr!=null && listHdr.size()>0){
            sipContainer.put("Diversion",listHdr);
        }

        hdr = getPServedUser(sipMessage);
        if( hdr!=null){
            sipContainer.put(PServedUser.NAME,hdr);
        }


        hdr = getHeader(sipMessage, CallIdHeader.NAME);
        if( hdr!=null){
            sipContainer.put(CallIdHeader.NAME,hdr);
        }
        hdr = getCseq(sipMessage);
        if( hdr!=null){
            sipContainer.put(CSeqHeader.NAME,hdr);
        }
        hdr = getAllow(sipMessage);
        if( hdr!=null){
            sipContainer.put(AllowHeader.NAME,hdr);
        }
        hdr = getSupported(sipMessage);
        if( hdr!=null){
            sipContainer.put(SupportedHeader.NAME,hdr);
        }
        hdr = getRequire(sipMessage);
        if( hdr!=null){
            sipContainer.put(RequireHeader.NAME,hdr);
        }
        hdr = getForking(sipMessage);
        if( hdr!=null){
            sipContainer.put("Request-Disposition",hdr);
        }
        hdr = getPEM(sipMessage);
        if( hdr!=null){
            sipContainer.put("P-Early-Media",hdr);
        }

        listHdr = getRoute(sipMessage);
        if( listHdr!=null){
            sipContainer.put(RouteHeader.NAME,listHdr);
        }

        listHdr = getRecordRoute(sipMessage);
        if( listHdr!=null){
            sipContainer.put(RecordRoute.NAME,listHdr);
        }


        hdr = getReason(sipMessage);
        if(hdr!=null){
            sipContainer.put(ReasonHeader.NAME,hdr);
        }

        hdr = getPrivacy(sipMessage);
        if(hdr!=null){
            sipContainer.put(PrivacyHeader.NAME,hdr);
        }

        hdr = getContact(sipMessage);
        if(hdr!=null){
            sipContainer.put(ContactHeader.NAME,hdr);
        }

        hdr = getExpires(sipMessage);
        if(hdr!=null){
            sipContainer.put(ExpiresHeader.NAME,hdr);
        }

        Map<String,Object> content = new HashMap<>();
        ContentTypeHeader contentType= (ContentTypeHeader) sipMessage.getHeader(ContentTypeHeader.NAME);

        //put content
        if( contentType!=null){
            content.put("Content-Type",contentType.getContentType());
            content.put("Content-SubType",contentType.getContentSubType());
            try {
                String contentBody = new String(sipMessage.getRawContent(), "UTF-8");
                if( contentType.getContentSubType()!=null && contentType.getContentSubType().contains("xml")){
                    content.put("json", getXmlToJson(contentBody));
                } else if( contentType.getContentSubType().contains("sdp")){
                    content.put("json",sdpToJson(contentBody));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            sipContainer.put("content",content);
        }


        List<Map<String,Object>> mimeContents = getContent((SIPMessage) sipMessage);
        sipContainer.put("mime",mimeContents);

        if( sipMessage instanceof  Response){
            javax.sip.message.Response resp=(javax.sip.message.Response) sipMessage;
            RSeqHeader rseq = (RSeqHeader )resp.getHeader(RSeqHeader.NAME);
            if( rseq!=null){
                Map<String, Object> rseqJson = getHeader((SIPHeader) rseq);
                sipContainer.put(RSeqHeader.NAME,rseqJson);
            }
        }

        getXHeadersToJson(sipMessage,sipContainer);

    }

    public static void getXHeadersToJson(Message sipMessage, Map<String, Object> sipContainer){
        log.debug("calling getXHeadersToJson... ");
        ListIterator it =  sipMessage.getHeaderNames();
        while( it.hasNext()){
            String header =  (String) it.next();
            log.debug("Header:{}",header);
            if( header.startsWith("X-") ||
                    header.startsWith("P-H") ||
                    header.startsWith("P-Sig-Info")){
                //parse header
                SIPHeader sipHeader = (SIPHeader) sipMessage.getHeader(header);
                log.info("Parsing header: {} {}",sipHeader.getHeaderName(),sipHeader.getValue());
                Map<String, Object> hdr=null;
                try {
                    XHeaderParser xHeaderParser = new XHeaderParser(sipHeader.getHeaderValue());
                    xHeaderParser.setHeaderName(sipHeader.getHeaderName());
                    XHeader xHeader = (XHeader) xHeaderParser.parse();
                    hdr = getHeader(xHeader);
                    //set original value
                    hdr.put("value",sipHeader.getHeaderValue());
                } catch(Exception e){
                    log.debug("Exception",e);
                }
                log.debug("Header parsed:{} object:{}",header,hdr);
                try {
                    if (hdr != null) {
                        sipContainer.put(header, hdr);
                    } else {
                        hdr = getHeader(sipHeader);
                        sipContainer.put(header, hdr);
                    }
                } catch(Exception e){
                    log.debug("Exception",e);
                }
            }
        }
    }

    public static Map<String, Object> getXmlToJson(String xml){
        log.debug("getXmlToJson xml:{}",xml);
        Map<String, Object> ret=null;

        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xml);

            ret = toMap(xmlJSONObj);
            log.debug("XMLJSON:{}",ret);
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }

    private static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static String getSessionId(String tag){
        StringBuffer builder = new StringBuffer();
        try {
            builder.append(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        builder.append("-");
        builder.append(Instant.now().getEpochSecond());
        builder.append("-");
        builder.append(tag);
        builder.append("-");
        builder.append(callIdCounter++);
        return builder.toString();
    }

    private static Map<String, Object> uriToMap(URI uri){
        Map<String, Object> map = new HashMap<>();

        map.put("scheme", uri.getScheme());
        if( uri.isSipURI() ){
            SipUri sipUri = (SipUri) uri;
            if(sipUri.getAuthority()!=null) map.put("authority", sipUri.getAuthority().toString());
            if(sipUri.getGrParam()!=null) map.put("gr", sipUri.getGrParam());
            if(sipUri.getHost()!=null) map.put("host", sipUri.getHost());
            if(sipUri.getLrParam()!=null) map.put("lr", sipUri.getLrParam());
            if(sipUri.getMAddrParam()!=null) map.put("maddr", sipUri.getMAddrParam());
            if(sipUri.getPort()!=0) map.put("port", sipUri.getPort());
            if(sipUri.getUser()!=null) map.put("user", sipUri.getUser());
//            List<Map<String, Object>> params = new ArrayList<>();

            Map<String, Object> params = new HashMap<>();

            Iterator<String> it = sipUri.getParameterNames();
            while ( it.hasNext() ){
                String param = it.next();
                String value = sipUri.getParameter(param);
//                Map<String, Object> paramJson = new HashMap<>();
//                params.add(paramJson);
//                paramJson.put(param, value);
                params.put(param,value);
            }
            if( !params.isEmpty()){
                map.put("parameters", params);
                log.debug("Parameters:{}",params);
            }

            NameValueList qheader = sipUri.getQheaders();
            Iterator<NameValue> names = qheader.iterator();
//            List<Map<String,Object>> qHeadersJson = new ArrayList<>();

            Map<String,Object> qHeadersJson = new HashMap<>();
            while(names.hasNext()){
                NameValue nvl = names.next();
//                Map<String, Object> nvlJson = new HashMap<>();
//                nvlJson.put(nvl.getName(),nvl.getValue());
//                qHeadersJson.add(nvlJson);
                qHeadersJson.put(nvl.getName(),nvl.getValue());
            }

            if( !qHeadersJson.isEmpty()){
                map.put("qheader", qHeadersJson);
            }

        } else {
            if( uri instanceof TelURL) {
                TelURL telUri = (TelURL) uri;
                if (telUri.getIsdnSubAddress() != null) map.put("isdn", telUri.getIsdnSubAddress());
                if (telUri.getPhoneNumber() != null) map.put("number", telUri.getPhoneNumber());
                if (telUri.getPostDial() != null) map.put("postDial", telUri.getPostDial());
//            List<Map<String, Object>> params = new ArrayList<>();
                Map<String, Object> params = new HashMap<>();
                Iterator<String> it = telUri.getParameterNames();
                while (it.hasNext()) {
                    String param = it.next();
                    String value = telUri.getParameter(param);
//                Map<String, Object> paramJson = new HashMap<>();
//                paramJson.put(param, value);
//                params.add(paramJson);

                    params.put(param, value);
                }
                if (!params.isEmpty()) {
                    map.put("parameters", params);
                }
            }
        }

        log.debug("SIP URI JSON headers:{}",map);
        return map;
    }

    private static Map<String, Object> addressToMap(Address sipAddress){
        Map<String,Object> map = new HashMap<>();

        if(sipAddress.getDisplayName()!=null)
            map.put("displayName", sipAddress.getDisplayName());

        map.put("uri",uriToMap(sipAddress.getURI()));
        map.put("uriValue",sipAddress.getURI().toString());
        return map;
    }

    private static HeaderFactory getHeaderFact() {
        if(headerFact ==null) {
            try {
                headerFact = SipFactory.getInstance().createHeaderFactory();
            } catch (PeerUnavailableException e) {
                log.error("Exception",e);
            }
        }
        return headerFact;
    }
    private static MessageFactory getMessageFact() {
        if(messageFact ==null) {
            try {
                messageFact = SipFactory.getInstance().createMessageFactory();
            } catch (PeerUnavailableException e) {
                log.error("Exception",e);
            }
        }
        return messageFact;
    }

    public static Header getHeader(String name, Map<String, List<Object>> sipMessage) {
        Header ret=null;
        List<Object> headerList = sipMessage.get(name);
        if( headerList!=null && headerList.size()>0) {
            //get item at position 0
            String header = (String)headerList.get(0);

            HeaderFactory fact = getHeaderFact();
            try {
//				ret = fact.createHeader(name, header);
                List headers = fact.createHeaders(header);

            } catch (ParseException e) {
                log.error("Exception",e);
            }
        }
        return ret;
    }

    public static Message getMessage(boolean request, String body) {
        Message ret=null;
        log.debug("Body:{}",body);
        if( body!=null && body.length()>0) {
            try {
                if (request) {
                    ret = getMessageFact().createRequest(body);
                } else {
                    ret = getMessageFact().createResponse(body);
                }
            } catch (ParseException e) {
                log.error("Exception", e);
            }
        }


        return ret;
    }

    public static Map<String, Object> getHeader(SIPHeader headerValue){
        if( headerValue==null){
            return null;
        }

        Map<String, Object> ret = new HashMap();

        ret.put("header", headerValue.getName());
        if( headerValue==null){
            ret.put("value", null);
        } else {
            ret.put("value", headerValue.getValue());
        }

        if( headerValue instanceof AddressParametersHeader ){
            AddressParametersHeader addr = (AddressParametersHeader) headerValue;
            ret.put("address", addressToMap(addr.getAddress()) );
        } else if ( headerValue instanceof ParametersHeader){
            Map<String,Object> params = new HashMap<>();
            ret.put("params",params);
            getParametersHeaders((ParametersHeader) headerValue, params);
        }

        return ret;
    }

    public static Map<String, Object> getHeader(Message message , String header){
        SIPHeader headerValue = (SIPHeader) message.getHeader( header );
        if( headerValue==null){
            return null;
        }
        Map<String, Object> ret = getHeader(headerValue);

        return ret;
    }

    public static Map<String, Object> getTo(Message message ){
        To headerValue = (To)message.getHeader( To.NAME);
        Map<String, Object> ret = getHeader(headerValue);
        if( headerValue==null)
            return null;


        if(headerValue.getTag()!=null) ret.put("tag", headerValue.getTag());

        return ret;
    }




    public static Map<String, Object> getContact(Message message ){
        ContactHeader headerValue = (ContactHeader)message.getHeader( ContactHeader.NAME);
        Map<String, Object> ret = getHeader((SIPHeader) headerValue);
        if( headerValue==null)
            return null;

        ret.put("expires",headerValue.getExpires());
        ret.put("qvalue",headerValue.getQValue());
        ret.put("wildcard",headerValue.isWildCard());

        return ret;
    }

    public static Map<String, Object> getFrom(Message message ){
        if( message==null) return null;
        From headerValue = (From)message.getHeader( From.NAME);
        if( headerValue==null)
            return null;
        Map<String, Object> ret = getHeader(headerValue);
        if (headerValue.getTag()!=null ) ret.put("tag", headerValue.getTag());

        return ret;
    }


    public static Map<String, Object> getPServedUser(Message message ){
        if( message==null) return null;
        PServedUser headerValue = (PServedUser)message.getHeader( PServedUser.NAME);
        if( headerValue==null)
            return null;
        Map<String, Object> ret = getHeader(headerValue);

        ret.put("registrationState",headerValue.getRegistrationState());
        ret.put("sessionCase",headerValue.getSessionCase());

        return ret;
    }

    public static Map<String, Object> getExpires(Message message ){
        if( message==null) return null;
        ExpiresHeader headerValue = (Expires)message.getHeader( ExpiresHeader.NAME);

        Map<String, Object> ret = getHeader((SIPHeader)headerValue);

        return ret;
    }

    public static Map<String, Object> getRUri(Message message ){
        if( message==null) return null;
        if( message instanceof SIPRequest ){
            SIPRequest sipR = (SIPRequest) message;
            Map<String, Object> hdrJson = new HashMap<>();
            hdrJson.put("header","RURI");
            hdrJson.put("value", sipR.getRequestURI().toString() );
            Map<String, Object> ret = uriToMap(sipR.getRequestURI());
            Map<String,Object> uri = new HashMap<>();

            hdrJson.put("address",uri);
            uri.put("uri",ret);


            return hdrJson;
        }
        return null;
    }

    public static List<Map<String, Object>> getPAI(Message message ){
        if( message==null) return null;
        List<Map<String, Object>> ret =null;
        ListIterator headers = message.getHeaders(PAssertedIdentityHeader.NAME);
        while( headers.hasNext()){
            PAssertedIdentity pai = (PAssertedIdentity)headers.next();
            Map<String, Object> paiJson = getHeader(pai);
            if (ret==null)
                ret = new ArrayList<>();
            ret.add(paiJson);
        }
        return ret;
    }

    public static void getParametersHeaders(ParametersHeader params, Map<String, Object> map){
        if( params==null){
            return;
        }
        Iterator<String> it = params.getParameterNames();
        while( it.hasNext() ) {
            String name = it.next();

            String value = params.getParameter(name);
            name = name.replaceAll("-", "_");
            map.put(name, value);
        }
    }

    public static Map<String, Object> getPCV(Message message ){
        if( message==null) return null;
        PChargingVector headerValue = (PChargingVector)message.getHeader( PChargingVectorHeader.NAME);
        if( headerValue==null){
            return null;
        }
        Map<String, Object> ret = new HashMap<>();
        ret.put("header", headerValue.getName());
        ret.put("value", headerValue.getValue());

        Map<String, Object> params = new HashMap<>();
        ret.put("params",params);
        getParametersHeaders(headerValue, params);

        return ret;
    }

    public static Map<String, Object> getPANI(Message message ){
        if( message==null) return null;


        PAccessNetworkInfoHeader headerValue = (PAccessNetworkInfoHeader) message.getHeader( PAccessNetworkInfoHeader.NAME);
        log.debug("{} - {}",PAccessNetworkInfoHeader.NAME,headerValue);

        log.debug("SIPMESSAGE:{}",message.toString());
        if( headerValue==null){
            return null;
        }

        Map<String, Object> ret = new HashMap<>();
        ret.put("header", headerValue.getName());
        ret.put("value", ((SIPHeader)headerValue).getHeaderValue());

        if( headerValue.getAccessType()!=null){
            ret.put("accessType",headerValue.getAccessType());
        }
        if( headerValue.getCGI3GPP()!=null){
            ret.put("cgi3gpp",headerValue.getCGI3GPP());
        }
        if( headerValue.getCI3GPP2()!=null){
            ret.put("cgi3gpp2",headerValue.getCI3GPP2());
        }
        if( headerValue.getDSLLocation()!=null){
            ret.put("dsLocation",headerValue.getDSLLocation());
        }
        if( headerValue.getUtranCellID3GPP()!=null){
            ret.put("cellId",headerValue.getUtranCellID3GPP());
        }


        Map<String, Object> params = new HashMap<>();
        ret.put("params",params);

        getParametersHeaders((ParametersHeader) headerValue, params);

        return ret;
    }

    public static Map<String, Object> getPCFA(Message message ){
        if( message==null) return null;
        PChargingFunctionAddresses headerValue = (PChargingFunctionAddresses)message.getHeader( PChargingFunctionAddressesHeader.NAME);

        if( headerValue==null)
            return null;
        Map<String, Object> ret = new HashMap<>();

        ret.put("header", headerValue.getName());
        ret.put("value", headerValue.getValue());
        getParametersHeaders(headerValue, ret);

        ListIterator li = headerValue.getChargingCollectionFunctionAddresses();
        List<String> ccfs = new ArrayList<>();
        while( li.hasNext() ){
            NameValue ccf = (NameValue) li.next();
            ccfs.add(ccf.getValue());
        }
        if( !ccfs.isEmpty()){
            ret.put("ccf", ccfs);
        }

        li = headerValue.getEventChargingFunctionAddresses();
        List<String> ecfs = new ArrayList<>();
        while( li.hasNext() ){
            NameValue ecf = (NameValue) li.next();
            ecfs.add(ecf.getValue());
        }
        if( !ecfs.isEmpty()){
            ret.put("ecf", ecfs);
        }

        return ret;
    }


    public static List<Map<String, Object>> getDiversion(Message message){
        log.debug("Getting Diversion content ...");
        return getAddressHeaders(message,"Diversion");
    }
    public static List<Map<String, Object>> getHistoryInfo(Message message){
        return getAddressHeaders(message,"History-Info");
//        if( message==null) return null;
//        List<Map<String, Object>> ret = new ArrayList();
//
//
//        ListIterator headers = message.getHeaders("History-Info");
//        AddressFactory addrFact = new AddressFactoryImpl();
//        if( headers!=null) {
//            while( headers.hasNext()) {
//                ExtensionHeader hiHeader = (ExtensionHeader)headers.next();
//                String[] hiHeaders = hiHeader.getValue().split(",");
//
//                for(String hiHeaderValue : hiHeaders){
//                    log.debug(hiHeaderValue);
//                    Map<String, Object> hi =new HashMap<>();
//
//
//                    try {
//                        String uriValue = hiHeaderValue;
//                        int start = uriValue.indexOf("<");
//                        if( start<0) {
//                            start=0;
//                        } else {
//                            start++;
//                        }
//                        int end = uriValue.lastIndexOf(">");
//                        if( end<0){
//                            end = uriValue.length();
//                        }
//                        uriValue = uriValue.substring(start, end);
//                        URI uri =  addrFact.createURI(uriValue);
//
//                        String indexValue = hiHeaderValue;
//                        indexValue = indexValue.substring(end+2);
//
//                        Map<String,Object> address = uriToMap(uri);
//                        hi.put("address",address);
//                        hi.put("index", indexValue);
//
//                        log.debug("URI:"+uri.toString()+" index:"+indexValue);
//                    } catch (ParseException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//
//                    hi.put("value", hiHeaderValue);
//                    hi.put("header", "History-Info");
//                    ret.add(hi);
//                }
//            }
//        }
//        return ret;
    }



    public static List<Map<String, Object>> getAddressHeaders(Message message, String headerName){
        if( message==null) return null;
        List<Map<String, Object>> ret = new ArrayList();

        ListIterator headers = message.getHeaders(headerName);
        AddressFactory addrFact = new AddressFactoryImpl();

        if( headers!=null) {
            while( headers.hasNext()) {
                ExtensionHeader hiHeader = (ExtensionHeader)headers.next();
                log.debug("Header :{}",hiHeader);
                String[] hiHeaders = hiHeader.getValue().split(",");

                for(String hiHeaderValue : hiHeaders){
                    log.debug(hiHeaderValue);
                    Map<String, Object> hi =new HashMap<>();


                    try {
                        String uriValue = hiHeaderValue;
                        int start = uriValue.indexOf("<");
                        if( start<0) {
                            start=0;
                        } else {
                            start++;
                        }
                        int end = uriValue.lastIndexOf(">");
                        if( end<0){
                            end = uriValue.length();
                        }
                        uriValue = uriValue.substring(start, end);
                        URI uri =  addrFact.createURI(uriValue);

                        String indexValue = hiHeaderValue;
                        indexValue = indexValue.substring(end+2);

                        Map<String,Object> address = uriToMap(uri);
                        hi.put("uri",uriValue);
                        hi.put("address",address);
                        hi.put("index", indexValue);

                        log.debug("URI:"+uri.toString()+" index:"+indexValue);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    hi.put("value", hiHeaderValue);
                    hi.put("header", headerName);
                    ret.add(hi);
                }
            }
        }
        return ret;
    }

    public static Map<String, Object> getCseq(Message message){
        CSeqHeader headerValue = (CSeqHeader) message.getHeader(CSeqHeader.NAME);
        if( headerValue==null){
            return null;
        }
        Map<String, Object> ret = new HashMap<>();
        ret.put("header", headerValue.getName());
        ret.put("value", headerValue.getSeqNumber());

        ret.put("method",headerValue.getMethod());
        ret.put("seqNumber",headerValue.getSeqNumber());

        return ret;
    }

    public static Map<String, Object> getAllow(Message message){
        ListIterator header = message.getHeaders(AllowHeader.NAME);
        if( header==null){
            return null;
        }

        Map<String, Object> ret = new HashMap<>();

        List<String> methods = new ArrayList<>();
        while(header.hasNext()){
            AllowHeader allow = (AllowHeader) header.next();

            methods.add(allow.getMethod());
        }
        ret.put("header",AllowHeader.NAME);
        ret.put("value",methods);

        return ret;
    }

    public static Map<String, Object> getSupported(Message message){
        ListIterator supported = message.getHeaders(SupportedHeader.NAME);
        if(supported==null){
            return null;
        }
        Map<String, Object> ret = new HashMap<>();
        List<String> option = new ArrayList<>();
        while( supported.hasNext()){
            SupportedHeader supp = (SupportedHeader)supported.next();
            option.add(supp.getOptionTag());
        }
        ret.put("value",option);
        ret.put("header",SupportedHeader.NAME);
        return ret;
    }

    public static Map<String, Object> getRequire(Message message){
        ListIterator require = message.getHeaders(RequireHeader.NAME);
        if(require==null){
            return null;
        }
        Map<String, Object> ret = new HashMap<>();
        List<String> option = new ArrayList<>();
        boolean empty=true;
        while( require.hasNext()){
            RequireHeader supp = (RequireHeader)require.next();
            option.add(supp.getOptionTag());
            empty=false;
        }
        if( !empty) {
            ret.put("value", option);
            ret.put("header", RequireHeader.NAME);
        } else {
            return null;
        }

        return ret;

    }

    public static Map<String, Object> getPEM(Message message){
        return getHeader(message, "P-Early-Media");
    }

    public static Map<String, Object> getForking(Message message){
        return getHeader(message, "Request-Disposition");
    }

    public static Map<String, Object> getReason(Message message) {
        Map<String, Object> ret = null;
        ReasonHeader reason = (ReasonHeader) message.getHeader(ReasonHeader.NAME);
        SIPHeader generic = (SIPHeader) reason;
        if( reason!=null){
            ret = new HashMap<>();
            ret.put("header",generic.getHeaderName());
            ret.put("value",generic.getHeaderValue());

            ret.put("protocol",reason.getProtocol());
            ret.put("cause",reason.getCause());
            ret.put("text",reason.getText());

        }
        return ret;

    }

    public static Map<String, Object> getPrivacy(Message message) {
        Map<String, Object> ret = null;
        PrivacyHeader value = (PrivacyHeader) message.getHeader(PrivacyHeader.NAME);
        SIPHeader generic = (SIPHeader) value;
        if( value!=null){
            ret = new HashMap<>();
            ret.put("header",generic.getHeaderName());
            ret.put("value",generic.getHeaderValue());

            ret.put("privacy",value.getPrivacy());
        }
        return ret;

    }

    public static List<Map<String, Object>> getRoute(Message message){
        ListIterator route = message.getHeaders(RouteHeader.NAME);
        if( route==null){
            return null;
        }

        List<Map<String, Object>> routeListJson = new ArrayList<>();
        while(route.hasNext()){
            RouteHeader routeH = (RouteHeader)route.next();
            SIPHeader sipHeader = (SIPHeader) routeH;
            Map<String, Object> routeJson = getHeader(sipHeader);
            routeListJson.add(routeJson);
        }
        return routeListJson;
    }


    public static List<Map<String, Object>> getRecordRoute(Message message){
        ListIterator route = message.getHeaders(RecordRoute.NAME);
        if( route==null){
            return null;
        }

        List<Map<String, Object>> routeListJson = new ArrayList<>();
        while(route.hasNext()){
            RecordRoute routeH = (RecordRoute)route.next();
            SIPHeader sipHeader = (SIPHeader) routeH;
            Map<String, Object> routeJson = getHeader(sipHeader);
            routeListJson.add(routeJson);
        }
        return routeListJson;
    }

    public static Map<String, Object> sdpToJson(String sdpStr){
        Map<String, Object> json=new HashMap<>();
        SDPAnnounceParser sdpParser = new SDPAnnounceParser(sdpStr);
        try {
            SessionDescriptionImpl sdp = sdpParser.parse();
            Vector<MediaDescriptionImpl> medias = sdp.getMediaDescriptions(false);
            List<Map<String, Object>> mediasJson = new ArrayList<>();
            for(MediaDescriptionImpl media: medias ){
                Map<String, Object> mediaJson = new HashMap<>();
                Vector<AttributeField> attrs = media.getAttributeFields();
                List<Map<String, Object>> attributesJson = new ArrayList<>();
                for( AttributeField attr : attrs ){
                    Map<String,Object> attrJson = new HashMap<>();
                    attrJson.put("name",attr.getName());
                    attrJson.put("value",attr.getValue());
                    attributesJson.add(attrJson);
                }
                mediaJson.put("attributes",attributesJson);

                //fill bandwidth fields
                Vector<BandwidthField> bandwitdh = media.getBandwidths(false);
                if( bandwitdh!=null && bandwitdh.size()>0) {
                    List<Map<String, Object>> bandsJson = new ArrayList<>();
                    for (BandwidthField field : bandwitdh) {
                        Map<String, Object> bandJson = new HashMap<>();
                        bandJson.put("value", field.getValue());
                        bandJson.put("bandwidth", field.getBandwidth());
                        bandJson.put("type", field.getType());
                        bandJson.put("bwtype", field.getBwtype());
                        bandsJson.add(bandJson);
                    }
                    mediaJson.put("bandwidth", bandsJson);
                }

                //put connection information
                if( media.getConnection()!=null){
                    Map<String, Object> connJson = new HashMap<>();
                    connJson.put("address",media.getConnection().getAddress());
                    connJson.put("networkType",media.getConnection().getNetworkType());
                    connJson.put("addressType",media.getConnection().getAddressType());
                    mediaJson.put("connection",connJson);
                }

                //put info
                if( media.getInfo()!=null) {
                    mediaJson.put("info", media.getInfo().getValue());
                }

                //put media
                if( media.getMedia()!=null){
                    Map<String, Object> medJson = new HashMap<>();
                    medJson.put("value",media.getMedia().toString());
                }

                //add media json
                mediasJson.add(mediaJson);
            }
            json.put("media",mediasJson);

            json.put("session",sdp.getSessionName().getValue());




        } catch(Exception e){
            e.printStackTrace();
        }
        return json;
    }

    public static Map<String, Object> getJsonFromFile(String path){
        Map<String, Object> sipMessage=null;

        ObjectMapper mapper = new ObjectMapper();
        try {
            try (InputStream io = new FileInputStream(path)) {
                sipMessage = mapper.readValue(io, HashMap.class);
            }
        } catch (Exception e){
            e.printStackTrace();
        }


        return sipMessage;
    }


    public static void setContent(Message sipMessage, Map<String, Object> content) {
        log.debug("SIP message:{}",sipMessage);
        log.debug("Content:{}",content);
        try {
            String contentType = (String) content.get("Content-Type");
            String contentSubType = (String) content.get("Content-SubType");
            ContentTypeHeader contentTypeHeader = (ContentTypeHeader) getHeaderFactory().createContentTypeHeader(contentType, contentSubType);

            String raw = (String) content.get("raw");

            log.debug("Set content type:{} subtype:{} raw:{}",contentType,contentSubType,raw);

            sipMessage.setContent(raw.getBytes(), contentTypeHeader);
        } catch (ParseException e) {
            log.error("Exception",e);
        }
    }

    public static void deleteHeaders(List<String> headers, Message sipMessage){
        log.debug("Deleting headers :{}",headers);
        try {
            for (String headerName : headers) {
                sipMessage.removeFirst(headerName);
            }
        }catch(Exception e){
            log.error("Exception",e);
        }
    }


    public static List<Map<String, Object>> getContent(SIPMessage message){
        List<Map<String,Object>> contents = new ArrayList<>();
        try {
            MultipartMimeContent mmContent = message.getMultipartMimeContent();
            if (mmContent != null) {
                Iterator<gov.nist.javax.sip.message.Content> it = mmContent.getContents();
                while (it.hasNext()) {
                    Map<String,Object> content = new HashMap<>();
                    gov.nist.javax.sip.message.Content mimeContent = it.next();
                    ContentTypeHeader contentType = mimeContent.getContentTypeHeader();

                    log.debug("Content type:{} subtype:{}",contentType.getContentType(),contentType.getContentSubType());
                    content.put("Content-Type",contentType.getContentType());
                    content.put("Content-SubType",contentType.getContentSubType());

                    try {
                        String contentBody = (String)mimeContent.getContent();
                        if( contentType.getContentSubType()!=null && contentType.getContentSubType().contains("xml")){
                            content.put("json", getXmlToJson(contentBody));
                        } else if( contentType.getContentSubType().contains("sdp")){
                            content.put("json",sdpToJson(contentBody));
                        } else if (contentType.getContentSubType().contains("sip")){
                            String sipMessage = (String) mimeContent.getContent();
                            try {
                                log.debug("Parse sip message:{}",sipMessage);
                                if( sipMessage.startsWith("SIP/2.0")) {
                                    javax.sip.message.Message msg = getMessageFactory().createResponse(sipMessage);
                                    fillSipJsonFields(msg,content);
                                } else {
                                    javax.sip.message.Message msg = getMessageFactory().createRequest(sipMessage);
                                    fillSipJsonFields(msg, content);
                                }

                            } catch (Exception e) {
                                log.error("Exception parsing response ...", e);
                            }
                        }

                    } catch (Exception e3){
                        log.error("Exception",e3);
                    }
                    if( content!=null) {
                        contents.add(content);
                    }

                }
            }
        } catch (Exception e){
            log.error("Exception",e);
        }
        return contents;
    }

    public static Map<String,Object> getErrorEvent(Map<String,Object> incomingEvent, String eventType){
        Map<String,Object> resp = new HashMap<>();
        Map<String, Object> callContainer = new HashMap<>();


        //1. set callID, this is unique identifying the call
        callContainer.put(JsonSCIFRequest.CallID, incomingEvent.get(JsonSCIFRequest.CallID));
        callContainer.put("session",  incomingEvent.get(JsonSCIFRequest.CallID));
        callContainer.put(JsonSCIFRequest.Queue, com.eitg.quasar.occp.sip.businesslogic.Parameters.getImsName());

        //2. metadata, related to appserver which generated this call
        callContainer.put(ApplicationServer, Parameters.getImsName());
        //3. timestamp of the event
        callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put("event-type","sip");
        if(eventType!=null ) {
            callContainer.put("event-name", "sip.error."+eventType);
        } else {
            callContainer.put("event-name", "sip.error.generic");
        }

        return resp;
    }

}
