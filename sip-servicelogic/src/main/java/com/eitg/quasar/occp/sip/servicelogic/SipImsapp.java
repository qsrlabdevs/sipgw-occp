package com.eitg.quasar.occp.sip.servicelogic;

import com.hp.opencall.imscapi.annotation.ImportFeature;
import com.hp.opencall.imscapi.annotation.ImsApplication;


@ImsApplication(name = "SipService", imslets = { "SipServiceImslet" },
		importedFeatures = {
				@ImportFeature(name = "ImscScifTjb-${SIP_CALL_PROVIDER_TECHNO}", interface_name = "com.hp.opencall.ngin.scif.impl.imsc.ImscScifFeatureSessionIntf")}
)

//@ImsApplication(name = "SipService", importedFeatures = {
//        @ImportFeature(name = "ImscScifTjb-${SIP_CALL_PROVIDER_TECHNO}", interface_name = "com.hp.opencall.ngin.scif.impl.imsc.ImscScifFeatureSessionIntf")},
//        imslets = { "SipServiceImslet" }
//
////		listeners = { @Listener(
////        listener_class = "com.hp.opencall.ngin.timer.impl.imsc.ImscTimerDispatcher") }
//
//		)

public class SipImsapp {

}
