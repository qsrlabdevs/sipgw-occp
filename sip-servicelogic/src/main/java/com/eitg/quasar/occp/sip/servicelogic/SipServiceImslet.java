package com.eitg.quasar.occp.sip.servicelogic;

import java.io.File;
import java.sql.ParameterMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.management.NotCompliantMBeanException;
import javax.sip.SipFactory;
import javax.sip.header.ContentTypeHeader;
import javax.sip.header.HeaderFactory;
import javax.sip.message.MessageFactory;

import com.eitg.quasar.occp.degraded.DegradedModeThread;
import com.eitg.quasar.occp.degraded.TypeScriptDegraded;
import com.eitg.quasar.occp.metrics.MetricsServer;
import com.hp.opencall.imscapi.annotation.Parameter;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.netserv.NetServProvider;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionProvider;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.parameters.CommonSessionParameterSet;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import gov.nist.javax.sip.message.MessageFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import com.hp.opencall.imscapi.LifecycleException;
import com.hp.opencall.imscapi.annotation.ImsletDescription;
import com.hp.opencall.imscapi.imslet.ImsletException;
import com.hp.opencall.ngin.scif.imsc.ScifImslet;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerFactory;
import com.hp.opencall.ngin.timer.TimerListener;

@ImsletDescription(
        name="SipServiceImslet"
)
public class SipServiceImslet extends ScifImslet {
	private static final Logger _log = LoggerFactory.getLogger(SipServiceImslet.class.getName());
	
	private static final Long CALL_PROVIDER_RETRY_COUNT = 5L;
	private static final Long CALL_PROVIDER_RETRY_TIMEOUT = 60 * 1000L;
	private static final Long DEFAULT_TIMEOUT = 5 * 60 * 1000L;
	private static final String SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME = "NgIn.Srv.SIP.CALL_PROVIDER_TECHNO";
	private static final String SERVICE_CALL_PROVIDER_NAME_PARAM_NAME = "NgIn.Srv.SIP.CALL_PROVIDER_NAME";

	private SipCreateCallsThread createCallsThread=null;
	
	public static volatile String ReleaseVersion = "v1_1_1";
	
	/**
	 * Represents the serial UID
	 */
	protected static final long serialVersionUID = 1L;


	boolean shouldStop=false;
	/**
	 * Represents the service's call user factory
	 */
	protected SipUserFactory userFactory = null;
	protected SipSessionUserFactory sessionFactory = null;
	private HeaderFactory headerFactory = null;

	public HeaderFactory getHeaderFactory() {
		return headerFactory;
	}

	/**
	 * Represents the tracer
	 */
	
	private CallProvider callProvider = null;
	private SessionProvider sessionProvider = null;
	private Object beanWrapper = null;

	public static final String NAME = "SipService";
	private static boolean loaded=false;

	public static int numberOfCalls=0;
	private List<RedisProcessorThread> redisProcessors;


	private static SipServiceImslet instance=null;
	private static TypeScriptDegraded tsProcessor=null;

	public static TypeScriptDegraded getTypeScriptProcessor(){
		return tsProcessor;
	}

	public static List<String> rawCallPollEvents=null;


	public SipServiceImslet() {
		// call super class's constructor
		super();

		instance=this;

		_log.debug("->SipServiceImslet() ");
	}

	public void init() {
		// call superclass's init()
		
		try {
			super.init();
		} catch (ImsletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_log.debug("->init()");
	}

	public void unlocked()   {
		// call superclasse's unlocked()

		try {
			super.unlocked();
		} catch (ImsletException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		_log.debug("->unlocked() - added CEC support");

		Long timeout = DEFAULT_TIMEOUT;

		_log.debug(SipUserFactory.NAME + " uses overload timeout: " + timeout);

		// add our bean
		String imsname = String.format("%s_%d", getImsletContext().getServiceCmpHostname(),
				getImsletContext().getServiceCmpVmId());

		userFactory = new SipUserFactory();
		sessionFactory = new SipSessionUserFactory();

		_log.debug("ScifFactory instance: " + ScifFactory.instance());
		// get the service call provider (should be CAP2) and register
		// our service factory to it
		_log.debug("Get parameter:" + SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME);
		String callProviderTechno = getImsletContext().getInitParameter(SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME);
		_log.debug(userFactory.getName() + " uses (" + callProviderTechno + ") call provider technology");
		_log.debug("Get parameter:" + SERVICE_CALL_PROVIDER_NAME_PARAM_NAME);
		String callProviderName = getImsletContext().getInitParameter(SERVICE_CALL_PROVIDER_NAME_PARAM_NAME);
		_log.debug(userFactory.getName() + " uses (" + callProviderName + ") call provider name");

		Parameters.initializeInstance(this);
		Parameters.setImsName(imsname);

		Long callProviderRetries = CALL_PROVIDER_RETRY_COUNT;
		_log.debug("Fetching call provider ...");
		while (callProviderRetries-- > 0) {
			try {
				callProvider = ScifFactory.instance().getCallProvider(callProviderTechno, callProviderName);
				if (callProvider != null) {
					_log.debug("CallProvider:" + callProvider);
					callProvider.addCallUserFactory(userFactory, null);
					break;
				} else {
					_log.debug("CallProvider: null");
				}
			} catch (ScifException e) {
				_log.error("Cannot obtain call provider, will retry", e);
			}

			try {
				Thread.sleep(CALL_PROVIDER_RETRY_TIMEOUT);
			} catch (InterruptedException e) {
				// ignore
			}
		}


		//fetch session provider
		try{
			this.sessionProvider = ScifFactory.instance().getSessionProvider(callProviderTechno, callProviderName);
			_log.debug("Session provider:{}",this.sessionProvider);

			this.sessionProvider.addSessionUserFactory(this.sessionFactory,null);

			headerFactory = SipFactory.getInstance().createHeaderFactory();

		} catch(ScifException e){
			_log.error("Exception",e);
		} catch (Exception e){
			_log.error("Exception",e);
		}

		_log.debug("Initializing redis ... {}",Parameters.get().getRedisUrl());
//		RedisPool.get().initialize(Parameters.get().getRedisUrl(), Parameters.get().getRedisPoolSize(), 1000);
		RedisPool.get().initialize(Parameters.get().getRedisUrl());
		_log.debug("Initializing redis ... {}",RedisPool.get().getDB());
		// add our bean
		//TODO - add Monitor Bean
		// startCountersLoggerThread();

//		createCallsThread = new SipCreateCallsThread(callProvider);
//		createCallsThread.start();

		_log.debug("Starting overload protection, TASGW:{}",RedisPool.getTasGw());

		try {
			Map<String, Object> tasGw = RedisPool.getTasGw();

			DegradedModeThread degradedModeThread = new DegradedModeThread();
			Map<String, Object> overload = (Map<String, Object>) RedisPool.getTasGw().get("overload");
			String sessionNonAck = (String) RedisPool.getTasGw().get("sip-session-nack");
			if( sessionNonAck!=null){
				Parameters.get().setSipSessionNack(sessionNonAck);
			}
			if (overload != null) {
				Integer maxExecutionTime = (Integer) overload.get("max-execution-time");
				Integer queueMaxLength = (Integer) overload.get("queue-max-length");
				Integer sleepTime = (Integer) overload.get("sleep-time");
				Integer errorCode = (Integer) overload.get("error-code");
				if (queueMaxLength != null) {
					DegradedModeThread.setQueueMaxLength(queueMaxLength);
				}
				if (sleepTime != null) {
					DegradedModeThread.setSleepTime(sleepTime);
				}
				if( maxExecutionTime!=null ){
					DegradedModeThread.setMaxExecutionTime(maxExecutionTime);
				}
				if( errorCode!=null ){
					DegradedModeThread.sipErrorCode=errorCode;
				}
			}

			Map<String,Object> subscribedEvents =  (Map<String, Object>) RedisPool.getTasGw().get("subscribedEvents");
			if( subscribedEvents!=null){
				rawCallPollEvents = (List<String>)subscribedEvents.get("rawCallPollEvents");
			}

			_log.debug("Added subscribed events:{}",subscribedEvents);
			_log.debug("Added rawCallPollEvents events:{}",rawCallPollEvents);



			degradedModeThread.start();
			//start overload protection thread
//			overloadProtection = new OverloadProtection();
//			if (RedisPool.getTasGw() != null) {
//				Map<String, Object> overload = (Map<String, Object>) RedisPool.getTasGw().get("overload");
//				if (overload != null) {
//					Integer queueMaxLength = (Integer) overload.get("queue-max-length");
//					Integer sleepTime = (Integer) overload.get("sleep-time");
//					if (queueMaxLength != null) {
//						OverloadProtection.setQueueMaxLength(queueMaxLength);
//					}
//					if (sleepTime != null) {
//						OverloadProtection.setSleepTime(sleepTime);
//					}
//				}
//			}
			String functionName = "";
			String libraryName = "";
			Boolean libraryReload = false;
			if (overload != null) {
				if (overload.containsKey("library-reload"))
					libraryReload = (Boolean) overload.get("library-reload");
				if (overload.containsKey("library-name"))
					libraryName = (String) overload.get("library-name");
				if (overload.containsKey("function-name"))
					functionName = (String) overload.get("function-name");

				tsProcessor = new TypeScriptDegraded(libraryName, functionName, libraryReload);
			}


//			overloadProtection.start();
		} catch (Exception e){
			_log.error("Exception",e);
		}


		redisProcessors = new ArrayList<>();
		//TODO - instantiate a pool of threads
		RedisProcessorThread thread = new RedisProcessorThread(Parameters.getImsName());
		redisProcessors.add(thread);
		thread.start();


		if(Parameters.get().getRedisQueueIngress()!=null){
			_log.debug("Start ingress redis processor queue");
			RedisProcessorThread ingress = new RedisProcessorThread(Parameters.get().getRedisQueueIngress());
			redisProcessors.add(thread);
			ingress.start();
		}

		SipCallUser.setEgressQueue(Parameters.get().getRedisQueueEgress());


		try {

		    String svc = SipUserFactory.NAME;
			_log.debug("Registering ICUI file :{}",svc);
            ScifFactory.instance().registerUiApplication("XML-FILE",svc, this);
        } catch (Exception e){
            _log.error("Couldn'd register icu file :{}",SipUserFactory.NAME);
        }




//		makeCallExample();
//		try {
//			makeSessionExample();
//		} catch(Exception e){
//			_log.error("Exception",e);
//		}

		_log.debug("Starting metrics server ...");
		MetricsServer.get().start();
		_log.debug("<-unlocked()");
	}

	public void destroy() {
		_log.debug("->destroy()");
		// stop the CDR writer thread
		shouldStop=true;
		super.destroy();
	}

	public boolean isShouldStop() {
		return shouldStop;
	}
	public Timer createTimer(TimerListener timerListener, long delay) {
		_log.debug("->createTimer()");
		return TimerFactory.instance().createTimer(getScifImsletApplicationSession(), timerListener, delay, null);
	}


	public void makeCallExample(){

		try {
			_log.debug("Making a new call example ...");

			CallUser callUser = new SipCallUser();

			Call newCall = this.callProvider.createCall(null, "mpbx", callUser, NetServProvider.Technology.SIP);
			((SipCallUser) callUser).initialize(newCall,"test");

			Address currentAddress=new Address("sip:tas@192.168.56.104:5060");
			Address remoteAddress=new Address("sip:server@192.168.56.104:6061");
			TerminalContact terminalContact = new TerminalContact(remoteAddress);


			SipParameterSet sipPset = newCall.getParameterSet(SipParameterSet.class);
			sipPset.selectHeaderRulesSet("SipServiceSpecificRulesSet");
			newCall.makeCall(currentAddress,terminalContact);

		} catch(Exception e){
			_log.error("Exception",e);
		}
	}



	public CallUser createCall(String eventId){
		_log.debug("Create new call ...");
		try {
//			ApplicationSession appSession = (ApplicationSession) this.getScifImsletApplicationSession();
			CallUser callUser = new SipCallUser();

			Call newCall = this.callProvider.createCall(null, "mpbx", callUser, NetServProvider.Technology.SIP);

			((SipCallUser) callUser).initialize(newCall, eventId);
			return callUser;
		} catch (ScifException e){
			_log.error("Exception",e);
		}

		return null;
	}

	public SessionUser createSession(String sessionId){
		SipSessionUser sessionUser = new SipSessionUser();
		try {
			Session session = this.sessionProvider.createSession(null, sessionId, sessionUser, NetServProvider.Technology.SIP);

			sessionUser.initialize(session, sessionId);

			return sessionUser;
		} catch (Exception e){
			_log.error("Exception",e);
		}
		return null;
	}

	public static SipServiceImslet getInstance(){
		return instance;
	}
}
