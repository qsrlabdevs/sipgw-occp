package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.servicelogic.SipSessionUser;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sip.header.ContentTypeHeader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class JsonSCIFMakeSession extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFMakeSession.class.getName());
	private SipParameterSet sipPset=null;
	private SipSessionParameterSet sessionPset=null;
	private CommonParameterSet commPset=null;

	private Session session;


	public JsonSCIFMakeSession(Map<String, Object> rawMessage){
		_log.debug("Processing make session action ...");
		Long diff = getTimeDiff(rawMessage);

		String callId = (String)rawMessage.get(SIPJsonFactory.CALLID);

		String queue = (String)rawMessage.get("ingressQueue");

		SessionUser sessionUser  = SipServiceImslet.getInstance().createSession(callId);
		Boolean fireAndForget = (Boolean) rawMessage.get(JsonSCIFResponse.FIRE_FORGET);

		if( fireAndForget!=null ){
			_log.debug("Session set fire and forget:{}",fireAndForget);
			((SipSessionUser)sessionUser).setFireAndForget(fireAndForget);
		}

		((SipSessionUser) sessionUser).setQueue(queue);

		this.session = ((SipSessionUser)sessionUser).getSession();


		sipPset = session.getSessionParameterSet(SipParameterSet.class);
		sessionPset = session.getSessionParameterSet(SipSessionParameterSet.class);
		commPset = session.getSessionParameterSet(CommonParameterSet.class);

		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

		try {
			if( rawMessage!=null){
				setHeaderRules(rawMessage, sessionPset);
				addHeaders(rawMessage, sessionPset);

				setSessionContent(rawMessage,sessionPset);
				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);

				String sessionType = (String)action.get("sessionType");

				List<Map<String,Object>> headers = (List<Map<String,Object>>)rawMessage.get("addHeaders");
				_log.debug("Adding headers:{}",headers);
				for (Map<String, Object> jsonHeader : headers) {
					String name = (String)jsonHeader.get("header");
					String value = (String)jsonHeader.get("value");
					sessionPset.setHeader(name, value, null);
				}


				sessionPset.setInitialOutgoingRequestType(SipSessionParameterSet.SipRequestType.valueOf(sessionType));

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.error("Exception",e);
		}
	}

	protected void setSessionContent(Map<String, Object> rawMessage, SipSessionParameterSet sipPset) {
		List<Map<String,Object>> content = (List<Map<String,Object>>)rawMessage.get(RAW_CONTENT);
		if( content==null){
			return;
		}
		for( Map<String,Object> item: content){
			String contentType = (String)item.get("content-type");
			String contentSubType = (String)item.get("content-subtype");
			String body = (String)item.get("body");

			if( contentType!=null && body!=null) {
				try {
					if (contentSubType==null){
						contentSubType="";
					}
					ContentTypeHeader contentTypeHeader = SipServiceImslet.getInstance().getHeaderFactory().createContentTypeHeader(contentType, contentSubType);
					SipRawMessageContent rawContent = new SipRawMessageContent(contentTypeHeader, body.getBytes());

					sipPset.setRawMessageContent(null, rawContent);

				} catch (Exception e) {
					_log.error("Exception", e);
				}
			}
		}
	}

	@Override
	public void execute() {
		_log.debug("executing ...");
		//create new call object

		//fetch destination address
		try {
			Contact contact = processAction(action, sipPset);
			String from = (String)action.get("from");
			Address fromAddr = null;
			if( from!=null){
				fromAddr = new Address(from);
			}

			session.make(fromAddr,contact);
		} catch(Exception e){
			_log.error("Exception",e);
		}

		_log.debug("executing ... - DONE");
	}

}
