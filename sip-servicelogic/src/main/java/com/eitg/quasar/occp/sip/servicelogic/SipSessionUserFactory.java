package com.eitg.quasar.occp.sip.servicelogic;

import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.SessionUserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SipSessionUserFactory implements SessionUserFactory {
    private static final Logger _log = LoggerFactory.getLogger(SipSessionUserFactory.class.getName());

    @Override
    public String getName() {
        return SipUserFactory.NAME;
    }

    @Override
    public SessionUser createSessionUser(Object o, Session session) throws ScifException {
        ApplicationSession anAppSession = (ApplicationSession) o;
        return new SipSessionUser(anAppSession, session);
    }
}
