package com.eitg.quasar.occp.sip.utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet.NetworkCallType;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerFactory;
import com.hp.opencall.ngin.timer.TimerListener;

public class TelecomUtils {
	private static final Logger _log = LoggerFactory.getLogger(TelecomUtils.class.getName());

	public static Long getCurrentTimeUnixFraction(){
		return (System.currentTimeMillis()%1000l);
	}

	public static Long getCurrentTimeUnixFormat(){
		return (System.currentTimeMillis()/1000l);
	}
	public  static String removePrefix(String number){
		if(number!=null && number.startsWith("+49")){
			return number.substring(3);
		}
		return number;
	}
	public static String getCurrentSystemDateTimeSeconds(){
		_log.debug("getCurrentSystemDateTime");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		_log.debug("formated data" + dateFormat.format(date));
		return dateFormat.format(date);
	}
	public static String getCurrentSystemDateTimeSecondsMiliSeconds(){
		_log.debug("getCurrentSystemDateTime");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		_log.debug("formated data" + dateFormat.format(date));
		return dateFormat.format(date);
	}
	public static String getSystemTimeZone(){
		_log.debug("getSystemTimeZone");
		DateFormat dateFormat = new SimpleDateFormat("z");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static Timer createTimer(TimerListener timerListener, long delay,ApplicationSession appSession, com.eitg.quasar.occp.sip.utils.TimerInformation.TIMER_TYPE timerType) {
		_log.debug("->createTimer() for {} with delay: {}",timerType,delay);

		TimerInformation timerInformation=new TimerInformation();
		timerInformation.setTimerType(timerType);
		return TimerFactory.instance().createTimer(appSession, timerListener, delay, timerInformation);
	}	
	
}
