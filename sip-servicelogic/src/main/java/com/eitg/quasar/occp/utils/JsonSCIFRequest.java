package com.eitg.quasar.occp.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sip.message.Message;
import javax.sip.message.Request;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.events.sip.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.ScifEvent;
import com.hp.opencall.ngin.scif.TerminalContact;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.Capability;

import gov.nist.javax.sip.message.SIPRequest;
import gov.nist.javax.sip.message.SIPResponse;


/**
 * 
 * This class is used to transfer data from Call to JsonCall 
 * It has following structure:
 * - callid - specify callid fetched from Call-ID from incoming INVITE
 * - as - specify application server
 * - eventtime - specify event time 
 * - event:
 * 		- this is mapped to scif callStart, callPoll, callEnd, callAnswered
 * 		- name: callStart/callPoll/callEnd/callAnswered
 * 		- callStart:
 * 				- cause: - specify cause received
 * 				- leg: specify leg
 * 				- contact: specify contact
 * 		- callPoll:
 * 				- event
 * 		- callEnd:
 * 				- cause
 * 		- callAnswered:
 * 				- cause
 * 				- leg
 * - call
 * 		- legs
 * 			- capabilities
 * 		- state
 * - SIP:
 * 		- SipParameterSet
 * 			- headerRulesVariables
 * 			- errorCode
 * 			
 * 		- message
 * 			- method
 * 		
 */
public class JsonSCIFRequest {

	public static final String SIP="SIP";
	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFRequest.class.getName());
	
	private Call call;
	private CallLeg initialLeg;
	public static final String CallID="callid";
	public static final String Queue="queue";
	public static final String ApplicationServer="as";
	public static final String EventTime="eventtime";
	public static final String EventTimestamp="timestamp";
	public static final String StartTime="starttime";
	public static final String StopTime="stoptime";
	public static final String InitialLeg="initialleg";
	public static final String Capabilities="capabilities";
	public static final String Content="content";
	public static final String Message="message";
	public static final String Method="method";
	public static final String RUri="r-uri";
	public static final String CallLegs="legs";
	public static final String CallState="state";
	public static final String Call="call";
	public static final String Event="event";

	public static final String EventName="name";
	public static final String CS_CAUSE="cause";
	public static final String CS_LEG="leg";
	public static final String CS_CONTACT="contact";
	public static final String Name="name";
	public static final String Address="address";
	public static final String Type="type";

	public static final String EventId = "id";

	public static final String CALLSTART = "callStart";
	public static final String SESSIONSTART = "sessionStart";
	public static final String MAKECALL = "makeCall";
	public static final String MAKESESSION = "makeSession";

	public static final String CALLPOLL = "callPoll";

	public static final String CALLANSWERED = "callAnswered";

	public static final String CALLEARLYANSWERED = "callEarlyAnswered";
	public static final String MEDIAOPERATION = "mediaOperationNotification";

	String sipCallId=null;
	
	int eventId=0;

	public JsonSCIFRequest(Call aCall, String pSipCallId){
		call = aCall;		
		//set call id
		SipParameterSet sipPset = (SipParameterSet) call.getParameterSet(SipParameterSet.class);

		if( sipPset!=null){
			SIPRequest initialInvite = (SIPRequest)sipPset.getInitialRequest();
			if( initialInvite!=null) {
				_log.debug("Initial invite:{}", initialInvite);
				_log.debug("CallID:{}", initialInvite.getCallId().getCallId());
				if (sipCallId == null) {
					sipCallId = Parameters.getImsName()+";" + initialInvite.getCallId().getCallId();
				}
			}
		}
		if( this.sipCallId==null){
			this.sipCallId = Parameters.getImsName()+";" + pSipCallId;
		}
	}
	
	public final String getSIPCallId(){

		return sipCallId;
	}
	
	public Map<String, Object> getJsonCallStart(Cause cause, CallLeg pleg, Contact contact, JsonSession jsonSession){
		Map<String, Object> callContainer = new HashMap<>();
		if( initialLeg==null){
			initialLeg=pleg;
		}
		
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());
		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.callStart."+cause.toString());


		
		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			_log.debug("call legs:{}",legs);
			for( CallLeg leg : call.getLegs()){
				if(leg!=null) {
					Map<String, Object> jsonLeg = new HashMap<>();
					jsonLeg.put(Name, leg.getName());
					jsonLeg.put(Address, leg.getLegAddress().toString());
					legs.add(jsonLeg);
				}
			}
			callMap.put(CallLegs, legs);
		}
		
		
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);
		
		//4.1 add event 
		Map<String, Object> event = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, JsonSCIFRequest.CALLSTART);
		event.put(EventId, eventId);
		
		Map<String,Object> callStart = new HashMap<>();
		
		_log.debug("Preparing event callStart with cause:{}",cause);
		callStart.put(JsonSCIFRequest.CS_CAUSE, cause);
		if( pleg!=null)
			callStart.put(JsonSCIFRequest.CS_LEG, pleg.getLegAddress().toString());
		if( contact!=null){
			if( contact instanceof TerminalContact){
				TerminalContact termContact = (TerminalContact) contact;
				callStart.put(JsonSCIFRequest.CS_CONTACT, termContact.getAddress().toString());
			} else {
				callStart.put(JsonSCIFRequest.CS_CONTACT, contact.toString());
			}
		}
		event.put(CALLSTART,callStart);
		_log.debug("Prepared callStart:{}",callStart);
		
		_log.debug("sending event:{}",event);
		callContainer.put(Event, event);
		
		//5. set SipParameterSet
		SipParameterSet sipParamSet = (SipParameterSet) call.getParameterSet(SipParameterSet.class);



		if(sipParamSet!=null){
			//set capabilities
			Collection<Capability> capabilities= sipParamSet.getCapabilities(initialLeg);
			Map<String,Object> sipPset = new HashMap<>();
			sipPset.put(Capabilities, capabilities);
			Map<String, List<Object>> jsonMessage = new HashMap<>();
			//get initial request
			SIPRequest initial = (SIPRequest) sipParamSet.getInitialRequest();



			_log.debug("Initial request:{}",initial);

			if( initial!=null) {
				{
					List<Object> type = new ArrayList<>();
					type.add("Request");
					jsonMessage.put(Type, type);
				}

				{
					List<Object> type = new ArrayList<>();
					type.add(initial.getMethod());
					jsonMessage.put(Method, type);
				}

				_log.debug("SIP message:{}", initial.toString());
				{
					List<Object> type = new ArrayList<>();
					type.add(initial.toString());
					jsonMessage.put("body", type);
				}
				sipPset.put(Message, jsonMessage);
				callContainer.put(SIP, sipPset);

				SIPJsonFactory.fillSipJsonFields((Message) initial, sipPset);
			}

			if( jsonSession!=null && jsonSession.getHeaderRegVars()!=null){

				Map<String, Object> headerVariables = new HashMap<>();
				for (String item : jsonSession.getHeaderRegVars()) {
					try {
						Object value = sipParamSet.getHeaderRulesVariableValue(item);
						if( value!=null) {
							headerVariables.put(item, value);
						}
					} catch(Exception e){
						_log.info("Exception",e);
					}
				}
				if( headerVariables.size()>0) {
					callContainer.put("headerVariables", headerVariables);
				}
			}

		}
		
		_log.debug("CallContainer :{}",callContainer);
		
		return callContainer;
	}

	public Map<String, Object> getJsonPollEvent(ScifEvent event) {
		Map<String, Object> callContainer = new HashMap<>();

		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());

		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());
		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.callPoll."+event.getClass().getSimpleName());


		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			_log.debug("call legs:{}",call.getLegs());
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				if( leg==null )
					continue;
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, leg.getLegAddress().toString());
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);

		Map<String, Object> pollEvent = new HashMap<>();
		//4. set poll event
		callContainer.put(Event, pollEvent);

		pollEvent.put(JsonSCIFRequest.EventName, CALLPOLL);
		pollEvent.put(JsonSCIFRequest.EventId, eventId);
		Map<String, Object> callPoll = new HashMap<>();
		callPoll.put(Type, event.getClass().getSimpleName());

		pollEvent.put(CALLPOLL, callPoll);

		Map<String, Object> sipPset = new HashMap<>();
		Map<String, List<Object>> message = new HashMap<>();
		sipPset.put(Message, message);
		callContainer.put(SIP, sipPset);

		if (event instanceof SuccessResponsePollEvent) {
			callPoll.put(Name, "SuccessResponsePollEvent");
			SuccessResponsePollEvent succ = (SuccessResponsePollEvent) event;
			callPoll.put(CS_LEG, succ.getLeg().getLegAddress().toString());

			SIPResponse sipResp = (SIPResponse) succ.getResponse();
			SIPJsonFactory.fillSipJsonFields(sipResp,sipPset);

			{
				List<Object> type = new ArrayList<>();
				type.add(sipResp.toString());
				message.put("body", type);
			}


			{
				List<Object> type = new ArrayList<>();
				type.add("Response");
				message.put(Type, type);
			}

//			Iterator<SIPHeader> headers= sipResp.getHeaders();
//			//get headers
//			while( headers.hasNext()){
//				SIPHeader header = headers.next();
//				if( message.containsKey(header.getHeaderName())){
//					message.get(header.getHeaderName()).add(header.getHeaderValue());
//				} else {
//					List<Object> headerV = new ArrayList<>();
//					headerV.add(header.getHeaderValue());
//					message.put(header.getHeaderName(), headerV);
//				}
//								
//			}
//			
//			//get content, can be SDP, etc
//			MultipartMimeContent mp;
//			try {
//				mp = sipResp.getMultipartMimeContent();
//			
//				Map<String, Object> content = new HashMap<>();
//				Iterator<Content> contents = mp.getContents(); 
//				while( contents.hasNext() ){
//					Content lContent = contents.next(); 
//					content.put( lContent.getContentTypeHeader().getContentType()+"/"+lContent.getContentTypeHeader().getContentSubType() , lContent.getContent() ); 
//				}
//				if( message.containsKey(Content)){
//					message.get(Content).add( content);
//				} else {
//					List<Object> headerV = new ArrayList<>();
//					headerV.add(content);
//					message.put(Content, headerV);
//				}
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}



		} else if (event instanceof SIPRingingPollEvent){
			callPoll.put(Name,"SIPRingingPollEvent");
			SIPRingingPollEvent ringingPoll = (SIPRingingPollEvent) event;

			SIPJsonFactory.fillSipJsonFields((Message) ringingPoll.getMessage(),sipPset);

			callPoll.put(CS_LEG, ringingPoll.getLeg().getLegAddress().toString());

		} else if (event instanceof SIP18xInformationalEvent){
			callPoll.put(Name,"SIP18xInformationalEvent");
			SIP18xInformationalEvent sip18xInfo = (SIP18xInformationalEvent) event;

			SIPJsonFactory.fillSipJsonFields((Message) sip18xInfo.getMessage(),sipPset);

			callPoll.put(CS_LEG, sip18xInfo.getLeg().getLegAddress().toString());

		} else if (event instanceof SIPSdpAnswerPollEvent ){
			callPoll.put(Name,"SIPSdpAnswerPollEvent");
			SIPSdpAnswerPollEvent sdpAnswerPollEvent = (SIPSdpAnswerPollEvent) event;

			SIPJsonFactory.fillSipJsonFields((Message) sdpAnswerPollEvent.getMessage(),sipPset);

			callPoll.put(CS_LEG, sdpAnswerPollEvent.getLeg().getLegAddress().toString());

		} else if (event instanceof RawContentPollEvent){
			callPoll.put(Name, "RawContentPollEvent");
			RawContentPollEvent rawPoll = (RawContentPollEvent) event;

			SIPJsonFactory.fillSipJsonFields((Message) rawPoll.getMessage(),sipPset);

			callPoll.put(CS_LEG, rawPoll.getLeg().getLegAddress().toString());


			
		} else if (event instanceof InfoPollEvent ){
			callPoll.put(Name, "InfoPollEvent");
			InfoPollEvent infoPoll = (InfoPollEvent) event;

			SIPJsonFactory.fillSipJsonFields((Message) infoPoll.getMessage(),sipPset);

			callPoll.put(CS_LEG, infoPoll.getLeg().getLegAddress().toString());
		}

		
		
		return callContainer;
	}



	public Map<String, Object> getEarlyMediaAnswered(CallLeg aNewLeg) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());
		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.callEarlyMediaAnswered."+aNewLeg.getClass().getSimpleName());

		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, leg.getLegAddress().toString());
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);

		Map<String, Object> event = new HashMap<>();
		callContainer.put(Event, event);
		
		event.put(JsonSCIFRequest.EventName, CALLEARLYANSWERED);
		event.put(JsonSCIFRequest.EventId, eventId);
		
		Map<String, Object> callEarlyAnswered = new HashMap<>();
		//4. set poll event
		event.put(CALLEARLYANSWERED, callEarlyAnswered);		
		
		callEarlyAnswered.put(CS_LEG, aNewLeg.getLegAddress().toString());
		callEarlyAnswered.put(Type,aNewLeg.getClass().getSimpleName());
		
		return callContainer;
	}



	public Map<String, Object> getAnswered(Cause aCause, CallLeg aNewLeg) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());

		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.callAnswered."+aNewLeg.getClass().getSimpleName());

		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, leg.getLegAddress().toString());
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);

		Map<String, Object> event = new HashMap<>();
		
		//4. set event
		callContainer.put(Event, event);
		Map<String, Object> callAnswered = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, CALLANSWERED);
		event.put(JsonSCIFRequest.EventId, eventId);
		event.put(CALLANSWERED,callAnswered);
		
		callAnswered.put(CS_LEG, aNewLeg.getLegAddress().toString());
		callAnswered.put(CS_CAUSE, aCause);
		callAnswered.put(Type,aNewLeg.getClass().getSimpleName());

		return callContainer;
	}

	public Map<String, Object> getCallEnd(Cause aCause) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());
		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.callEnd");

		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				if(leg!=null && leg.getName()!=null && leg.getLegAddress()!=null) {
					Map<String, Object> jsonLeg = new HashMap<>();
					jsonLeg.put(Name, leg.getName());
					jsonLeg.put(Address, leg.getLegAddress().toString());
					legs.add(jsonLeg);
				}
			}
			callMap.put(CallLegs, legs);
		}
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);

		Map<String, Object> callEnd = new HashMap<>();
		//4. set poll event
		callContainer.put(Event, callEnd);
		
		callEnd.put(JsonSCIFRequest.EventName, "callEnd");
		callEnd.put(JsonSCIFRequest.EventId, eventId);
		Map<String,Object> earlyAnswered = new HashMap<>();
		earlyAnswered.put(CS_CAUSE, aCause);
		
		
		return callContainer;	
	}
	
	public Map<String, Object> getMediaOperationNotification(SipRawMessageContent content, Message message) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, sipCallId);
		callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Instant.now().toEpochMilli());
		callContainer.put(EventTimestamp, Instant.now().toEpochMilli());
		callContainer.put(StartTime, Instant.now().toEpochMilli());
		callContainer.put("event-type","sip");
		callContainer.put("event-name","sip.mediaOperationNotification.*");

		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, leg.getLegAddress().toString());
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);


		Map<String, Object> event = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, "mediaOperationNotification");
		event.put(EventId, eventId);
		//4. set poll event
		callContainer.put(Event, event);
		
		
		Map<String,Object> monEvent = new HashMap<>();
		event.put("mediaOperationNotification", monEvent);
		
		if( message instanceof SIPResponse ){
		    SIPResponse lSipResp = (SIPResponse) message;
		    event.put(JsonSCIFRequest.Type, lSipResp.getStatusCode());
		    monEvent.put("status", lSipResp.getStatusCode());
			monEvent.put("cause", Integer.toString(lSipResp.getStatusCode()) );
		    
		    try {
				monEvent.put("Content",lSipResp.getMessageContent());
			} catch (UnsupportedEncodingException e) {
				_log.error("Exception",e);
			}
		} else {
			Request lSipReq = (Request) message;
			event.put(JsonSCIFRequest.Type, lSipReq.getMethod());
		    String lInfoContent = new String(lSipReq.getRawContent(),StandardCharsets.UTF_8);
		    _log.debug("Content:"+ lInfoContent);
		    monEvent.put("Content", lInfoContent);
			monEvent.put("cause", lSipReq.getMethod());
		}

		//fill SIP part
		Map<String, Object> sip = new HashMap<>();
		callContainer.put("SIP",sip);

		SIPJsonFactory.fillSipJsonFields(message,sip);

		return callContainer;	
	}

	public Call getCall(){
		return this.call;
	}

	public ApplicationSession getApplicationSession() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void incrementEventId(){
		eventId++;
		_log.debug("Incremented eventid :{}",eventId);
	}

	public int getEventId() {
		return eventId;
	}

	public void setSipCallId(String value){
		this.sipCallId=value;
	}
}
