package com.eitg.quasar.occp.metrics;

import io.prometheus.client.dropwizard.samplebuilder.CustomMappingSampleBuilder;
import io.prometheus.client.dropwizard.samplebuilder.MapperConfig;
import io.prometheus.client.dropwizard.samplebuilder.SampleBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PrometheusUtils {
    public static SampleBuilder countersBuilder(){
        MapperConfig tasPing = new MapperConfig();
        tasPing.setMatch("tas.appserver.*");
        tasPing.setName("appServer");
        {
            Map<String, String> labels = new HashMap<>();
            tasPing.setLabels(labels);
            labels.put("name","${0}");
        }



        MapperConfig callStart = new MapperConfig();
        callStart.setMatch("tas.sip.callStart.*");
        callStart.setName("callStart");
        {
            Map<String, String> labels = new HashMap<>();
            callStart.setLabels(labels);
            labels.put("cause","${0}");
        }



        MapperConfig callPoll = new MapperConfig();
        callPoll.setMatch("tas.sip.callPoll.*");
        callPoll.setName("callPoll");
        {
            Map<String, String> labels = new HashMap<>();
            callPoll.setLabels(labels);
            labels.put("cause","${0}");
        }



        MapperConfig callAnswered = new MapperConfig();
        {
            callAnswered.setMatch("tas.sip.callAnswered.*");
            callAnswered.setName("callAnswered");
            Map<String, String> labels = new HashMap<>();
            callAnswered.setLabels(labels);
            labels.put("cause","${0}");
        }


        MapperConfig callEarlyAnswered = new MapperConfig();
        callEarlyAnswered.setMatch("tas.sip.callEarlyAnswered");
        callEarlyAnswered.setName("callEarlyAnswered");

        MapperConfig parkedEvents = new MapperConfig();
        parkedEvents.setMatch("tas.sip.parkedEvents");
        parkedEvents.setName("parkedEvents");




        MapperConfig callEnd = new MapperConfig();
        callEnd.setMatch("tas.sip.callEnd.*");
        callEnd.setName("callEnd");
        {
            Map<String, String> labels = new HashMap<>();
            callEnd.setLabels(labels);
            labels.put("cause", "${0}");
        }


        MapperConfig configTimeRes = new MapperConfig();
        configTimeRes.setMatch("tas.sip.time");
        configTimeRes.setName("response_time");
        /*{
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            configTimeRes.setLabels(lab);
        }*/

        MapperConfig tasResponse = new MapperConfig();
        tasResponse.setMatch("tas.sip.resp.*");
        tasResponse.setName("response");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasResponse.setLabels(lab);
        }

        MapperConfig tasSessStart = new MapperConfig();
        tasSessStart.setMatch("tas.sip.sessionStarted.*.*");
        tasSessStart.setName("sessionStarted");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("method", "${0}");
            lab.put("cause", "${1}");
            tasSessStart.setLabels(lab);
        }

        MapperConfig tasSessAnsw = new MapperConfig();
        tasSessAnsw.setMatch("tas.sip.sessionAnswered.*");
        tasSessAnsw.setName("sessionAnswered");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasSessAnsw.setLabels(lab);
        }

        MapperConfig tasSessEnd = new MapperConfig();
        tasSessEnd.setMatch("tas.sip.sessionEnded.*");
        tasSessEnd.setName("sessionEnded");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasSessEnd.setLabels(lab);
        }

        return new CustomMappingSampleBuilder(Arrays.asList(tasPing,callStart,callAnswered,callEarlyAnswered,callPoll,callEnd,configTimeRes,tasResponse,tasSessStart,tasSessAnsw,tasSessEnd));
    }
}
