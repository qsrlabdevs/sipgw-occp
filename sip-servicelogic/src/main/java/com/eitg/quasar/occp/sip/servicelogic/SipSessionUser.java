package com.eitg.quasar.occp.sip.servicelogic;

import com.eitg.quasar.occp.metrics.MetricsServer;
import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.utils.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.resources.RawMediaCallLeg;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionLeg;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SipSessionUser implements SessionUser {

    private static final Logger _log = LoggerFactory.getLogger(SipSessionUser.class.getName());

    private ApplicationSession applicationSession;
    private Session session=null;

    public static String QUEUE_EGRESS="sip_events";
    String queue = null;
    JsonSessionRequest jsonCall = null;
    JsonSession jsonSession = null;

    private boolean fireAndForget=false;
    private boolean autoAnswerCallStart=false;

    public void setQueue(String queue){
        _log.debug("Set egress queue:{}",queue);
        this.queue=queue;
    }

    public SipSessionUser(ApplicationSession applicationSession, Session session){
        _log.debug(">>> SipSessionUser");
        this.applicationSession=applicationSession;
        this.session = session;

        jsonCall = new JsonSessionRequest(this, this.session, Parameters.getImsName()+ ";" + this.session.getId());
        jsonSession = new JsonSession(this.applicationSession);
    }

    public SipSessionUser(){
        _log.debug(">>> SipSessionUser");
        this.applicationSession=null;
        this.session = null;

        jsonCall = new JsonSessionRequest(this, null, null);
        jsonSession = new JsonSession(this.applicationSession);
    }

    @Override
    public void started(Cause cause, SessionLeg sessionLeg, Contact contact) {
        _log.debug("Session user started, cause:{} leg:{} contact:{}",cause,sessionLeg,contact);

        Map<String, Object> eventData = new HashMap<>();
        eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);
        eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
        eventData.put(JsonSCIFResponse.CALL, session);
        eventData.put(JsonSCIFResponse.SESSION, jsonSession);
        eventData.put(JsonSCIFResponse.SESSION_USER, this);

        Long lstartTime = Calendar.getInstance().getTimeInMillis();
        String eventName = null;

        SipSessionParameterSet sipParamSet = session.getSessionParameterSet(SipSessionParameterSet.class);
        String sipMethod = "*";
        if( sipParamSet!=null ){
            if(sipParamSet.getInitialRequest()!=null){
                sipMethod = sipParamSet.getInitialRequest().getMethod();
            }
        }

        String counter = "tas.sip.sessionStarted."+sipMethod+"."+cause ;
        MetricsServer.get().counterIncrement(counter);

        try{
            //prepare JSON message
            Map<String,Object> rawMessage = jsonCall.getJsonSessionStart(sessionLeg,cause,contact);
            eventName =  (String)rawMessage.get("event-name");

            ObjectMapper mapper = new ObjectMapper();
            String json=null;
            json = mapper.writeValueAsString(rawMessage);

            _log.debug("JSON:{}",json);


            if( json!=null && fireAndForget==false){
                //send this to redis queue
                _log.debug("Sending JSON message to redis ...");
                RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);

                if( queue==null){
                    queue=Parameters.get().getRedisQueueEgress();
                }
                boolean status = RedisPool.get().sendToQueue(rawMessage, queue);
            }
        } catch (Exception e){
            _log.error("Exception",e);
        } finally {
            jsonCall.incrementEventId();
            _log.info("Execution time, cause:{}, spent time:{}",cause, (Calendar.getInstance().getTimeInMillis()-lstartTime));
        }


        _log.debug("Check event name:{} regexp:{}",eventName,Parameters.get().getSipSessionNack());
        if( eventName!=null && eventName.length()>0 && eventName.matches(Parameters.get().getSipSessionNack())) {
            _log.debug("Do not ack event, wait for flow ACK");
        } else {
            try {
                fireAndForget=true;
                session.accept();
                RedisProcessorThread.remove(jsonCall.getSIPCallId());

            } catch (Exception e) {
                _log.error("Exception", e);
            }
        }
    }

    @Override
    public void answered(Cause cause, SessionLeg sessionLeg) {
        _log.debug("Session user answered, cause:{} leg:{}",cause,sessionLeg);

        String counter = "tas.sip.sessionAnswered."+cause ;
        MetricsServer.get().counterIncrement(counter);

        Map<String, Object> eventData = new HashMap<>();
        eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLANSWERED);
        eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
        eventData.put(JsonSCIFResponse.CALL, session);
        eventData.put(JsonSCIFResponse.SESSION, jsonSession);
        eventData.put(JsonSCIFResponse.LEG, sessionLeg);
        eventData.put(JsonSCIFResponse.CALL_USER, this);

        try{
            _log.info("answered cause:{}, leg:{}",cause, sessionLeg);
            Map<String, Object> rawAnswered = jsonCall.getAnswered(cause, sessionLeg);
            _log.info(" rawAnswered:{}",rawAnswered);

            if( fireAndForget==false) {
                RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);
                if (queue == null) {
                    queue = Parameters.get().getRedisQueueEgress();
                }
                boolean status = RedisPool.get().sendToQueue(rawAnswered, queue);
            }

        } finally {
            jsonCall.incrementEventId();
        }
    }

    @Override
    public void ended(Cause cause) {
        _log.debug("Session user ended, cause:{}",cause);
        String counter = "tas.sip.sessionEnded."+cause ;
        MetricsServer.get().counterIncrement(counter);
        try{
            _log.info("Session callEnd cause:{} fireAndForget:{}", cause ,fireAndForget);
            if( fireAndForget==false) {
                Map<String, Object> rawEnd = jsonCall.getEnd(cause);
                _log.info(" rawEnd:{}", rawEnd);
                if (queue == null) {
                    queue = Parameters.get().getRedisQueueEgress();
                }
                boolean status = RedisPool.get().sendToQueue(rawEnd, queue);
            }
        } finally {
            jsonCall.incrementEventId();

            //remove events from parking slot
            RedisProcessorThread.remove(jsonCall.getSIPCallId());
        }

    }

    public void initialize(Session session, String sipCallId){
        this.session = session;
        this.applicationSession=null;

        jsonCall = new JsonSessionRequest(this, this.session, sipCallId);
        jsonSession = new JsonSession(this.applicationSession);

    }

    public void setSession(Session session){
        this.session=session;
    }
    public Session getSession(){
        return this.session;
    }

    public void setFireAndForget(boolean value){
        _log.debug("Session setFireAndForget:{}",value);
        this.fireAndForget=value;
    }
}
