package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.servicelogic.SipSessionUser;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sip.header.ContentTypeHeader;
import java.util.List;
import java.util.Map;

public class JSONSCIFSessionResponse extends JsonSCIFResponse {

    private static final Logger _log = LoggerFactory.getLogger(JSONSCIFSessionResponse.class.getName());
    private SipParameterSet sipPset=null;
    private SipSessionParameterSet sessionPset=null;
    private CommonParameterSet commPset=null;

    private Session session=null;

    public JSONSCIFSessionResponse(Map<String, Object> rawMessage, SessionUser pSessionUser){
        _log.debug("Processing make session action ...");

        Long diff = getTimeDiff(rawMessage);
        String callId = (String)rawMessage.get(SIPJsonFactory.CALLID);
        String queue = (String)rawMessage.get("ingressQueue");

        SessionUser sessionUser  = pSessionUser;
        Boolean fireAndForget = (Boolean) rawMessage.get(JsonSCIFResponse.FIRE_FORGET);

        if( fireAndForget!=null ){
            _log.debug("Session set fire and forget:{}",fireAndForget);
            ((SipSessionUser)sessionUser).setFireAndForget(fireAndForget);
        }

        ((SipSessionUser) sessionUser).setQueue(queue);

        this.session = ((SipSessionUser)sessionUser).getSession();


        sipPset = session.getSessionParameterSet(SipParameterSet.class);
        sessionPset = session.getSessionParameterSet(SipSessionParameterSet.class);
        commPset = session.getSessionParameterSet(CommonParameterSet.class);

        _log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

        try {
            if( rawMessage!=null){
                setHeaderRules(rawMessage, sessionPset);
                addHeaders(rawMessage, sessionPset);
                setSessionContent(rawMessage,sessionPset);
                //8. action: forward call
                action = (Map<String,Object>) rawMessage.get(ACTION);

                String sessionType = (String)action.get("sessionType");
            }
        } catch (Exception e) {
            _log.error("Exception",e);
        }
    }

    protected void setSessionContent(Map<String, Object> rawMessage, SipSessionParameterSet sipPset) {
        List<Map<String,Object>> content = (List<Map<String,Object>>)rawMessage.get(RAW_CONTENT);
        if( content==null){
            return;
        }
        for( Map<String,Object> item: content){
            String contentType = (String)item.get("content-type");
            String contentSubType = (String)item.get("content-subtype");
            String body = (String)item.get("body");

            if( contentType!=null && body!=null) {
                try {
                    if (contentSubType==null){
                        contentSubType="";
                    }
                    ContentTypeHeader contentTypeHeader = SipServiceImslet.getInstance().getHeaderFactory().createContentTypeHeader(contentType, contentSubType);
                    SipRawMessageContent rawContent = new SipRawMessageContent(contentTypeHeader, body.getBytes());

                    sipPset.setRawMessageContent(null, rawContent);

                } catch (Exception e) {
                    _log.error("Exception", e);
                }
            }
        }
    }

    @Override
    public void execute() {
        _log.debug("executing ...");

        if( action==null){
            return;
        }
        //create new call object
        Integer type = (Integer)action.get(ACTION_TYPE);
        if( type==null ){
            return ;
        }
        Integer errorCode = (Integer) action.get(ERRORCODE);
        String cause = (String) action.get(CAUSE);
        String reason = (String) action.get(REASON);


        //fetch destination address
        try {
            switch(type){
                case 0:
                    //reject session
                    this.session.abort(Cause.valueOf(cause));
                    break;
                case 1:
                    //accept
                    this.session.accept();
                    break;
            }
        } catch(Exception e){
            _log.error("Exception",e);
        }

        _log.debug("executing ... - DONE");
    }

}
