package com.eitg.quasar.occp.degraded;

import com.eitg.quasar.nexus.middleware.typescript.libraries.Loader;
import com.eitg.quasar.nexus.middleware.utils.Commons;
import com.eitg.quasar.occp.utils.RedisPool;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptEngine;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

public class TypeScriptDegraded {
    private static final Logger log = LoggerFactory.getLogger(TypeScriptDegraded.class.getName());

    private String tsLibraryName = "";
    private String tsFunctionName = "";

    private Map<String,Object> myConfigSectionForMyScript = new HashMap<>();
    private Boolean libraryReload=true;
    private AtomicBoolean loadingScript = new AtomicBoolean(false);

    private static Stack<ScriptEngine> scriptEngines=new Stack<>();



    public TypeScriptDegraded(String library, String function, Boolean libraryReload){

        this.tsLibraryName=library;
        this.tsFunctionName=function;
        this.libraryReload = libraryReload;
        myConfigSectionForMyScript.put("log",log);

        log.debug("TypeScriptDegraded library:{} function:{}, reload:{}",this.tsLibraryName,this.tsFunctionName,this.libraryReload);
    }

    public Object runScript(Map<String,Object> event){
        Map<String,Object> ret = null;

        ScriptEngine scriptEngine = null;
        Object result = null;
        try {
            // only one thread enters here to upload the script

            if (!scriptEngines.isEmpty()) {
                scriptEngine = scriptEngines.pop();
            }

            if (libraryReload == true) {
                if (loadingScript.compareAndSet(false, true)) {
                    log.debug("Reload function:{} library:{}",tsFunctionName, tsLibraryName);
                    Object appdb = RedisPool.get().getAppDb();
                    String libraryScriptCode = Loader.retrieveLibraryScriptJsCode(appdb, tsLibraryName);
                    log.debug("Recompile :{}",libraryScriptCode);
                    scriptEngine = Loader.compileScript(libraryScriptCode);
                }
            }

            //loadingScript.set(false);
            DegradedModeThread.DegradedType type = DegradedModeThread.inOverload ?
                    DegradedModeThread.DegradedType.OVERLOAD : DegradedModeThread.DegradedType.DEGRADED;


            Map<String,Object> session = new HashMap<>();
            session.put("type",type);
            session.put("log",log);
            session.put("queueLength",DegradedModeThread.currentLength);
            Object somResult = Loader.invokeFunction(scriptEngine, tsFunctionName, session, event, null);


            if (somResult != null) {
                result = Commons.translateScriptObject((ScriptObjectMirror) somResult);
            }
            log.debug("Result:{}", result);
        } catch (Exception e){
            log.error("Exception",e);
        } finally {
            if(scriptEngine!=null){
                scriptEngines.push(scriptEngine);
            }
        }

        return result;
    }
}
