package com.eitg.quasar.occp.sip.servicelogic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallProvider;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.ScifFactory;
import com.hp.opencall.ngin.scif.TerminalContact;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;

public class SipCreateCallsThread extends Thread {

	private static final Logger _log = LoggerFactory.getLogger(SipCreateCallsThread.class.getName());
	
	private CallProvider callProvider;
	public SipCreateCallsThread(CallProvider callProvider ){
		try {
			this.callProvider = ScifFactory.instance().getCallProvider(SipParameterSet.TECHNOLOGY, null);
		} catch (ScifException e) {
			_log.error("Exception",e);
		}
	}
	
	@Override
	public void run(){
		_log.debug("SipCreateCallsThread running, fetch calls from queue and try to instantiate new ones ...");
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		while(true){
			//fetch message from queue
			SipCallUser sipCallUser = new SipCallUser();
			try {
				Call newCall = callProvider.createCall(null, SipUserFactory.NAME, sipCallUser, null);
				sipCallUser.setCall(newCall);
				_log.debug("State:{}",newCall.getState());
				
				newCall.makeCall(new Address("sip:occp@192.168.182.128:5260"), new TerminalContact("sip:msml@192.168.182.128:6162"));
				_log.debug("makeCall - State:{}",newCall.getState());
				
				
				
//				newCall.forwardCall(new TerminalContact("sip:msml@192.168.182.128:6162"));
				_log.debug("newCall:{}",newCall);
				
				Thread.sleep(1000);
			} catch (ScifException | InterruptedException e) {
				_log.error("Exception",e);
			}			
//		}
			
		while(true){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
