package com.eitg.quasar.occp.sip.businesslogic;

import java.util.ArrayList;
import java.util.List;

import com.hp.opencall.imscapi.imslet.ApplicationSession;

/**
 * 
 * This class holds session related parameters as instructed by business logic application
 *
 */
public class JsonSession  {

	//stores list of header rules variables, make then available to business logic application
	List<String> headerRegVars;
	ApplicationSession appSession;
	
	public JsonSession(ApplicationSession session){
		headerRegVars = new ArrayList<>();
		appSession = session;
	}
	
	public List<String> getHeaderRegVars(){
		return headerRegVars;
	}

	public void setHeaderVariables(List<String> list){
		if( list==null ){
			return ;
		}
		for(String item: list){
			headerRegVars.add(item);
		}
	}
	
	public ApplicationSession getApplicationSession(){
		return appSession;
	}
}
