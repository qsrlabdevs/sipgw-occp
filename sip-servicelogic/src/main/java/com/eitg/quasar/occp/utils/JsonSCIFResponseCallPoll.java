package com.eitg.quasar.occp.utils;

import java.util.Map;

import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.PollEvent;
import com.hp.opencall.ngin.scif.ScifEvent;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;

public class JsonSCIFResponseCallPoll extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseCallPoll.class.getName());
	
	private PollEvent poll;
	private String type;
	
	public JsonSCIFResponseCallPoll(Call aCall, Map<String, Object> rawMessage, JsonSession session, SipCallUser sipCallUser, ScifEvent pollEvent){
		_log.debug("Processing response for callPoll ...");
		Long diff = getTimeDiff(rawMessage);
		call=aCall;
		SipParameterSet sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
	
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);
		
		if( rawMessage!=null){
			setHeaderRules(rawMessage, sipPset, commPset, session);
			setCEC(rawMessage,sipPset,commPset,sipCallUser);
			//process poll action
			action = (Map<String,Object>) rawMessage.get(ACTION);
			type = (String )action.get(ACTION_TYPE);
			_log.debug("Action type:{}",type);
			
			poll = (PollEvent) pollEvent;
		}
	}
	
	@Override
	public void execute() {
		_log.debug("executing ...");
		if( poll!=null){
			if( type.equalsIgnoreCase(SuccessReponseActionAccept)){
				poll.accept();
			} else if (type.equalsIgnoreCase(SuccessReponseActionReject)) {
				poll.reject(Cause.NONE);
			} else {
				try {
					poll.forward();
				} catch (ScifException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}	
		_log.debug("executing ... - DONE");
	}

}
