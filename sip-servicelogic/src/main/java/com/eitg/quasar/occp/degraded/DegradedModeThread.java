package com.eitg.quasar.occp.degraded;

import com.eitg.quasar.occp.metrics.MetricsServer;
import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class DegradedModeThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(DegradedModeThread.class.getName());

    public enum DegradedType {
        DEGRADED,
        OVERLOAD
    }

    public static boolean inOverload=false;
    public static boolean inDegraded=false;
    public static int queueMaxLength=10;
    public static long currentLength=0;
    private static int sleepTime=1000;
    private static int maxExecutionTime=0;
    public static int sipErrorCode=500;

    public static void setSleepTime(int value){
        sleepTime=value;
    }

    public static void setQueueMaxLength(int value){
        queueMaxLength=value;
    }

    public static void setMaxExecutionTime(int value){
        maxExecutionTime=value;
    }

    @Override
    public void run (){
        while(true){
            log.debug("Running degraded mode thread ...");
            inOverload=false;
            inDegraded=false;
            try {
                log.debug("Checking queue length sleep:{} ...",sleepTime);
                try(Jedis resource = RedisPool.get().getDB().getResource()){
                    Long queueLength = resource.llen(Parameters.get().getRedisQueueEgress());
                    currentLength = queueLength;
                    log.debug("Queue:{} size:{} maxLength:{} ",Parameters.get().getRedisQueueEgress(),queueLength,queueMaxLength);
                    if( queueLength>=queueMaxLength){
                        log.error("Enter to overload, length:{} max length:{}",queueLength,queueMaxLength);
                        inOverload=true;
                    } else {
                        inOverload=false;
                    }
                }

                //get execution timer time
                log.debug("Checking degraded mode, execution time:{} maxTime:{}",
                        RedisProcessorThread.getExecutionTimer().getSnapshot().get75thPercentile(),
                        maxExecutionTime);
                if(RedisProcessorThread.getExecutionTimer()!=null && maxExecutionTime!=0 &&
                        RedisProcessorThread.getExecutionTimer().getSnapshot().get75thPercentile()>maxExecutionTime){
                    log.error("Set in degraded mode, execution time:{} maxTime:{}",
                            RedisProcessorThread.getExecutionTimer().getSnapshot().get75thPercentile(),
                            maxExecutionTime);
                    inDegraded=true;
                }

                log.debug("Parked events size:{}",RedisProcessorThread.getParkedEventsSize());

                String counter = "tas.sip.parkedEvents";
                MetricsServer.get().counterIncrement(counter);

                Thread.sleep(sleepTime);
            } catch (JedisConnectionException je) {
                log.error("JedisConnectionException",je);
                inDegraded=true;
            } catch (Exception e){
                log.error("Exception",e);
                inDegraded=true;
            }
        }

    }

}
