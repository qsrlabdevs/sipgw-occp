package com.eitg.quasar.occp.sip.servicelogic;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.sip.PeerUnavailableException;
import javax.sip.SipFactory;
import javax.sip.header.ContentTypeHeader;
import javax.sip.message.Message;
import javax.sip.message.Request;
import javax.sip.message.Response;

import com.eitg.quasar.occp.metrics.MetricsServer;
import com.eitg.quasar.occp.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.eitg.quasar.occp.sip.utils.TelecomUtils;
import com.eitg.quasar.occp.sip.utils.TimerInformation.TIMER_TYPE;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.parameters.SipRawMessageContent;
import com.hp.opencall.ngin.scif.resources.RawMediaCallLeg;
import com.hp.opencall.ngin.scif.resources.RawMediaCallLeg.RawBytesMediaUser;
import com.hp.opencall.ngin.timer.Timer;

public class SipRawBytesMediaUser implements RawBytesMediaUser {

	private static final Logger _log = LoggerFactory.getLogger(SipRawBytesMediaUser.class.getName());
	
    private static final String MSML_PLAY_RESULT_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    	    + " <msml version=\"1.1\">\n"
    	    + "  <result response=\"200\">\n" + "  </result>"
    	    + " </msml>";
    
	private JsonSCIFRequest jsonCall;
	private JsonSession jsonSession;
	private RawMediaCallLeg rawMediaLeg;
	private SipCallUser sipCallUser;
	
	
	public SipRawBytesMediaUser(JsonSCIFRequest jsonCall, JsonSession jsonSession, SipCallUser sipCallUser){
		this.jsonCall = jsonCall;
		this.jsonSession = jsonSession;
		this.sipCallUser = sipCallUser;
	}
	
	public void setRawMediaCallLeg(RawMediaCallLeg value){
		rawMediaLeg= value;
	}
	
	@Override
	public void mediaOperationNotification(SipRawMessageContent content, Message message) {
		String counter = "tas.sip.mediaOperationNotification" ;
		MetricsServer.get().counterIncrement(counter);

		_log.debug("mediaOperationNotification content:{} message:{}",content, message);
		
		if( message instanceof Request){
			//respond with 200OK for this INFO
			Request lSipReq = (Request) message;
		    try {
		    	ContentTypeHeader lContentTypeHeader = (ContentTypeHeader) SipFactory.getInstance()
					.createHeaderFactory().createHeader("Content-Type", "application/msml+xml");
	
				if( lSipReq.getRawContent()!=null){
					SipRawMessageContent lSipRawMessageContent = new SipRawMessageContent(lContentTypeHeader, MSML_PLAY_RESULT_RESPONSE);
					rawMediaLeg.performMediaOperation(lSipRawMessageContent);
				}
		    } catch (PeerUnavailableException e1) {
				_log.error("Error on sending 200 OK [INFO]:"+e1.getMessage());
		    } catch (ParseException e1) {
				_log.error("Error on sending 200 OK [INFO]:"+e1.getMessage());
		    } catch (ScifException e) {
				_log.error("Error on sending 200 OK [INFO]:"+e.getMessage());			
		    }
		}
		
		Map<String, Object> mediaOpNotif = jsonCall.getMediaOperationNotification(content, message);
	    //no need to wait for something
	    boolean status =  RedisPool.get().sendToQueue(mediaOpNotif, SipCallUser.QUEUE_EGRESS);

		_log.debug("Exit mediaOperationNotification.");

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.MEDIAOPERATION);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, jsonCall.getCall());
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, rawMediaLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, sipCallUser);

		RedisProcessorThread.parkEvent(jsonCall.getSIPCallId(), eventData);

//	    String ingressQueue = "app_"+jsonCall.getSIPCallId();
//
//	    Map<String,Object> rawResponse = RedisPool.get().getFromQueue(ingressQueue);
//	    if( rawResponse!=null){
//	    	JsonSCIFResponse response = new JsonSCIFResponseLegAction(jsonCall.getCall() , rawResponse, jsonSession , rawMediaLeg);
//
//			JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(jsonCall.getCall(), response);
//			_log.debug("Arming timer ...");
//			Timer timer = TelecomUtils.createTimer(timerListener, 0, jsonSession.getApplicationSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
//
//	    }
	}

	@Override
	public void mediaOperationRejected(int cause, Response response) {
		String counter = "tas.sip.mediaOperationRejected" ;
		MetricsServer.get().counterIncrement(counter);

		_log.debug("mediaOperationRejected cause:{} response:{}",cause, response);
	}

}
