package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.sip.businesslogic.Parameters;
import com.eitg.quasar.occp.sip.utils.SIPJsonFactory;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.session.Session;
import com.hp.opencall.ngin.scif.session.SessionLeg;
import com.hp.opencall.ngin.scif.session.SessionUser;
import com.hp.opencall.ngin.scif.session.parameters.SipSessionParameterSet;
import gov.nist.javax.sip.message.SIPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sip.message.Message;
import java.util.*;

public class JsonSessionRequest {

    public static final String SIP="SIP";
    private static final Logger _log = LoggerFactory.getLogger(JsonSessionRequest.class.getName());

    private Session session;
    private SessionLeg initialLeg;
    public static final String CallID="callid";
    public static final String Queue="queue";
    public static final String ApplicationServer="as";
    public static final String EventTime="eventtime";
    public static final String StartTime="starttime";
    public static final String StopTime="stoptime";
    public static final String InitialLeg="initialleg";
    public static final String Capabilities="capabilities";
    public static final String Content="content";
    public static final String Message="message";
    public static final String Method="method";
    public static final String RUri="r-uri";
    public static final String CallLegs="legs";
    public static final String CallState="state";
    public static final String Call="call";
    public static final String Event="event";

    public static final String EventName="name";
    public static final String CS_CAUSE="cause";
    public static final String CS_LEG="leg";
    public static final String CS_CONTACT="contact";
    public static final String Name="name";
    public static final String Address="address";
    public static final String Type="type";

    public static final String EventId = "id";

    public static final String CALLSTART = "callStart";
    public static final String SESIONSTART = "sessionStart";
    public static final String MAKECALL = "makeCall";

    public static final String CALLPOLL = "callPoll";

    public static final String CALLANSWERED = "callAnswered";

    public static final String CALLEARLYANSWERED = "callEarlyAnswered";
    public static final String MEDIAOPERATION = "mediaOperationNotification";

    String sipCallId=null;

    int eventId=0;

    public JsonSessionRequest(SessionUser user, Session session, String psipCallId){
        this.session=session;
        if( psipCallId!=null) {
            this.sipCallId = psipCallId.replaceAll("#", "-");
        } else {
            this.sipCallId=psipCallId;
        }
    }

    public Map<String,Object> getJsonSessionStart(SessionLeg leg, Cause cause, Contact contact){

        SipSessionParameterSet sipParamSet = session.getSessionParameterSet(SipSessionParameterSet.class);
        String sipMethod = "*";
        if( sipParamSet!=null ){
            if(sipParamSet.getInitialRequest()!=null){
                sipMethod = sipParamSet.getInitialRequest().getMethod();
            }
        }

        Map<String,Object> jsonObj = new HashMap<>();
        jsonObj.put("event-name","sip.sessionStart."+sipMethod);
        jsonObj.put("event-type","occp");
        jsonObj.put(CallID, sipCallId);
        jsonObj.put("session",this.sipCallId);

        jsonObj.put(Queue, Parameters.getImsName());

        //2. metadata, related to appserver which generated this call
        jsonObj.put(ApplicationServer, Parameters.getImsName());
        //3. timestamp of the event
        jsonObj.put(EventTime, Calendar.getInstance().getTimeInMillis());
        jsonObj.put(StartTime, Calendar.getInstance().getTimeInMillis());

        //4. call container
        Map<String, Object> callMap = new HashMap<>();
        if( session.getSessionLegs()!=null){
            List<Map<String, Object>> legs = new ArrayList<>();
            _log.debug("call legs:{}",legs);
            for( SessionLeg item : session.getSessionLegs() ){
                if(leg!=null) {
                    Map<String, Object> jsonLeg = new HashMap<>();
                    jsonLeg.put(Name, leg.getName());
                    jsonLeg.put(Address, leg.getLegAddress().toString());
                    legs.add(jsonLeg);
                }
            }
            callMap.put(CallLegs, legs);
        }

        //5. session start
        callMap.put(CallState, session.getState());
        jsonObj.put(Call, callMap);

        //4.1 add event
        Map<String, Object> event = new HashMap<>();
        event.put(JsonSCIFRequest.EventName, JsonSCIFRequest.SESSIONSTART);
        event.put(EventId, eventId);

        Map<String,Object> callStart = new HashMap<>();

        _log.debug("Preparing event callStart with cause:{}",cause);
        callStart.put(JsonSCIFRequest.CS_CAUSE, cause);
        if( leg!=null)
            callStart.put(JsonSCIFRequest.CS_LEG, leg.getLegAddress().toString());
        if( contact!=null){
            if( contact instanceof TerminalContact){
                TerminalContact termContact = (TerminalContact) contact;
                callStart.put(JsonSCIFRequest.CS_CONTACT, termContact.getAddress().toString());
            } else {
                callStart.put(JsonSCIFRequest.CS_CONTACT, contact.toString());
            }
        }
        event.put(SESIONSTART,callStart);
        _log.debug("Prepared callStart:{}",callStart);

        _log.debug("sending event:{}",event);
        jsonObj.put(Event, event);



        if( sipParamSet !=null ){
            //get initial request
            SIPRequest initial = (SIPRequest) sipParamSet.getInitialRequest();
            Map<String,Object> sipPset = new HashMap<>();
            Map<String, List<Object>> jsonMessage = new HashMap<>();
            _log.debug("Initial request:{}",initial);

            if( initial!=null) {
                {
                    List<Object> type = new ArrayList<>();
                    type.add("Request");
                    jsonMessage.put(Type, type);
                }

                {
                    List<Object> type = new ArrayList<>();
                    type.add(initial.getMethod());
                    jsonMessage.put(Method, type);
                }

                _log.debug("SIP message:{}", initial.toString());
                {
                    List<Object> type = new ArrayList<>();
                    type.add(initial.toString());
                    jsonMessage.put("body", type);
                }
                sipPset.put(Message, jsonMessage);
                jsonObj.put(SIP, sipPset);

                SIPJsonFactory.fillSipJsonFields((javax.sip.message.Message) initial, sipPset);
            }
        }

        return jsonObj;
    }

    public int getEventId(){
        return this.eventId;
    }

    public String getSIPCallId(){
//        if( sipCallId==null ){
//            sipCallId = Parameters.getImsName() + ";" + this.session.getId();
//        }
        return this.sipCallId;
    }

    public Map<String, Object> getAnswered(Cause aCause, CallLeg aNewLeg) {
        Map<String, Object> callContainer = new HashMap<>();

        //1. set callID, this is unique identifying the call
        callContainer.put(CallID, sipCallId);
        callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
        callContainer.put(Queue, Parameters.getImsName());

        //2. metadata, related to appserver which generated this call
        callContainer.put(ApplicationServer, Parameters.getImsName());
        //3. timestamp of the event
        callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put("event-type","sip");
        callContainer.put("event-name","sip.sessionAnswered."+aNewLeg.getClass().getSimpleName());

        //4. call container
        Map<String, Object> callMap = new HashMap<>();
        if( session.getSessionLegs()!=null){
            List<Map<String, Object>> legs = new ArrayList<>();
            for( CallLeg leg : session.getSessionLegs()){
                Map<String,Object> jsonLeg = new HashMap<>();
                jsonLeg.put(Name, leg.getName());
                jsonLeg.put(Address, leg.getLegAddress().toString());
                legs.add(jsonLeg);
            }
            callMap.put(CallLegs, legs);
        }
        callMap.put(CallState, session.getState());
        callContainer.put(Call, callMap);

        Map<String, Object> event = new HashMap<>();

        //4. set event
        callContainer.put(Event, event);
        Map<String, Object> callAnswered = new HashMap<>();
        event.put(JsonSCIFRequest.EventName, CALLANSWERED);
        event.put(JsonSCIFRequest.EventId, eventId);
        event.put(CALLANSWERED,callAnswered);

        callAnswered.put(CS_LEG, aNewLeg.getLegAddress().toString());
        callAnswered.put(CS_CAUSE, aCause);
        callAnswered.put(Type,aNewLeg.getClass().getSimpleName());

        return callContainer;
    }


    public Map<String, Object> getEnd(Cause aCause) {
        Map<String, Object> callContainer = new HashMap<>();

        //1. set callID, this is unique identifying the call
        callContainer.put(CallID, sipCallId);
        callContainer.put("session", sipCallId);
//		callContainer.put(Queue, "app_"+sipCallId);
        callContainer.put(Queue, Parameters.getImsName());

        //2. metadata, related to appserver which generated this call
        callContainer.put(ApplicationServer, Parameters.getImsName());
        //3. timestamp of the event
        callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
        callContainer.put("event-type","sip");
        callContainer.put("event-name","sip.sessionEnd");

        //4. call container
        Map<String, Object> callMap = new HashMap<>();
        if( session.getSessionLegs()!=null){
            List<Map<String, Object>> legs = new ArrayList<>();
            for( CallLeg leg : session.getSessionLegs()){
                Map<String,Object> jsonLeg = new HashMap<>();
                jsonLeg.put(Name, leg.getName());
                jsonLeg.put(Address, leg.getLegAddress().toString());
                legs.add(jsonLeg);
            }
            callMap.put(CallLegs, legs);
        }
        callMap.put(CallState, session.getState());
        callContainer.put(Call, callMap);

        Map<String, Object> callEnd = new HashMap<>();
        //4. set poll event
        callContainer.put(Event, callEnd);

        callEnd.put(JsonSCIFRequest.EventName, "sessionEnd");
        callEnd.put(JsonSCIFRequest.EventId, eventId);

        return callContainer;
    }

    public void  incrementEventId(){
        this.eventId++;
    }
}
