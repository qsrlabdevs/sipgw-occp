package com.eitg.quasar.occp.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.eitg.quasar.occp.sip.servicelogic.SipCallUser;
import com.eitg.quasar.occp.sip.servicelogic.SipServiceImslet;
import com.eitg.quasar.occp.sip.utils.TelecomUtils;
import com.eitg.quasar.occp.sip.utils.TimerInformation;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.events.sip.SIPRingingPollEvent;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerListener;
import gov.nist.javax.sip.header.SIPHeader;
import gov.nist.javax.sip.message.SIPMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.sip.businesslogic.JsonSession;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.Capability;

import javax.sip.header.Header;

public class JsonSCIFResponseCallStart extends JsonSCIFResponse {



	class NoAnswerTimerListener implements TimerListener {

		private Call call ;
		private SipCallUser callUser;
		private boolean noActivity=false;


		public NoAnswerTimerListener(Call pCall, SipCallUser sipCallUser, boolean noActivity){
			_log.debug("Arm NoAnswerTimerListener ... session:{}",sipCallUser.getCallId());
			this.call=pCall;
			this.callUser = sipCallUser;
			this.noActivity=noActivity;
		}

		@Override
		public void timeout(Timer timer) {
			_log.debug("Executing noanswer timer ..., noActivity:{} pending forward call:{} callIsAnswered:{} session:{}",this.noActivity,this.callUser.isPendingForwardCall(), this.callUser.isCallIsAnswered(), this.callUser.getCallId());
			if( timer!=null ) {
				timer.cancel();
			}

			//
			if( (this.noActivity && this.callUser.isPendingForwardCall()) ||
					(!this.noActivity && this.callUser.isCallIsAnswered())){
				//cancel outgoing leg
				CallLeg[] callLegs = this.call.getLegs();
				_log.debug("Available call legs:{} size:{}",callLegs,callLegs.length);
				for( CallLeg leg : callLegs ){
					if( !leg.equals(this.callUser.getUpstreamLeg())){
						try {
							_log.debug("Releasing leg:{}",leg);
							this.callUser.getEvents().add("leg.timeout");
							leg.release(Cause.NO_ANSWER);

						} catch (Exception e){
							_log.error("Exception on release leg",e);
						}
					}
				}
			}
		}
	}

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseCallStart.class.getName());
	private SipParameterSet sipPset=null;
	private SipCallUser callUser=null;

	
	public JsonSCIFResponseCallStart(Call aCall, Map<String, Object> rawMessage, JsonSession session, SipCallUser sipCallUser){
//		super(aCall, rawMessage, session);
		
		Long diff = getTimeDiff(rawMessage);
		call =aCall;
		sipPset = call.getParameterSet(SipParameterSet.class);
		this.callUser = sipCallUser;
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

		try {
			if( rawMessage!=null){
				_log.debug("Starting to set CEC and Header rules");
				setCEC(rawMessage,sipPset,commPset,sipCallUser);
				setHeaderRules(rawMessage, sipPset, commPset, session);
				removeHeaders(rawMessage, sipPset);
				addHeaders(rawMessage, sipPset);

				if(rawMessage.containsKey("syncPollEvents")){
					Object value = rawMessage.get("syncPollEvents");
					if( value instanceof Map) {
						sipCallUser.setSyncPollEvents((Map<String, Object>) value);
					}
				}

				try{
					//6. capabilities
					capabilities=(List<String>)rawMessage.get(CAPABILITIES);
					if( capabilities!=null){
						Collection<Capability> outCaps = new ArrayList<>();
						for( String cap : capabilities){
							Capability caps = Capability.valueOf(Capability.class, cap);
							outCaps.add(caps);					
						}
						sipPset.setCapablities(null, outCaps);
						_log.debug("Outgoing leg capabilities:{}",outCaps);
					}
				} catch (Exception e){
					_log.error("Exception - set downstream capabilities",e);
				}

				try{
					//7. capabilities
					upstreamCapabilities=(List<String>)rawMessage.get(UP_CAPABILITIES);
					if( upstreamCapabilities!=null){
						Collection<Capability> outCaps = new ArrayList<>();
						for( String cap : upstreamCapabilities){
							Capability caps = Capability.valueOf(Capability.class, cap);
							outCaps.add(caps);
						}
						sipPset.setCapablities(sipCallUser.getUpstreamLeg(), outCaps);
						_log.debug("Incoming leg capabilities:{}",outCaps);
					}
				} catch (Exception e){
					_log.error("Exception - set upstream capabilities",e);
				}

				try{
					//7. capabilities
					requireCapabilities=(List<String>)rawMessage.get(REQUIRE_CAPABILITIES);
					if( requireCapabilities!=null){
						Collection<Capability> capList = new ArrayList<>();
						for( String cap : requireCapabilities){
							Capability caps = Capability.valueOf(Capability.class, cap);
							capList.add(caps);
						}
						sipPset.requireCapabilities(capList);
						_log.debug("Require capabilities:{}",capList);
					}
				} catch (Exception e){
					_log.error("Exception - set upstream capabilities",e);
				}



				try{
					//7. add headers
					addHeaders=(List<Map<String,Object>>)rawMessage.get(ADD_HEADER);
					_log.debug("Add headers:{}",addHeaders);
					boolean isB2BUA=true;

					Map<String,Object> action = (Map<String,Object>)rawMessage.get("action");
					if(action!=null){
						String leaveType = (String) action.get(JsonSCIFResponse.LEAVE_TYPE);
						if( leaveType!=null && leaveType.equalsIgnoreCase("proxy")){
							isB2BUA=false;
						}
					}

					_log.debug("addHeaders isB2BUA:{}",isB2BUA);

					if( addHeaders!=null){

						if( isB2BUA){
							List<String> historyInfo = new ArrayList<>();
							for (Map<String, Object> jsonHeader : addHeaders) {
								String name = (String)jsonHeader.get("header");
								String value = (String)jsonHeader.get("value");
								if (name.equalsIgnoreCase("History-Info")) {
									historyInfo.add(value);
								} else {
									sipPset.setHeader(name, value, null);
								}
							}
							if( historyInfo.size()> 0){
								try {
									sipPset.addHistoryInfoEntries(historyInfo);

									Collection<Capability> outCaps = new ArrayList<>();
									outCaps.add(Capability.HISTORY_INFO);
									sipPset.setCapablities(null, outCaps);
									_log.debug("Set capabilities:{}", outCaps);
								} catch (Exception e) {
									_log.error("Exception on set HistoryInfo:{}", e);
								}
							}
						} else {
							for (Map<String, Object> jsonHeader : addHeaders) {
								//sipPset.setHeader((String)jsonHeader.get("header"),(String)jsonHeader.get("value"),null);
								SIPMessage message = (SIPMessage) sipPset.getInitialInvite();
								try {
									Header header = SipServiceImslet.getInstance().getHeaderFactory().createHeader((String) jsonHeader.get("header"), (String) jsonHeader.get("value"));
									message.addFirst(header);

									_log.debug("Message addFirst:{}", header);
								} catch (Exception e1) {
									_log.error("Exception adding header", e1);
								}
							}
						}
					}
				} catch (Exception e){
					_log.error("Exception - add headers",e);
				}
				//7. network ringing tone
				ringingTones = (List<Map<String,Object>>) rawMessage.get(RINGINGTONES);
				//7.1 set new P-Charging-Vector
				
				//7.2 set history-info
				
				
				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);
				
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.error("Exception",e);
		}
	}

	@Override
	public void execute() {
		_log.debug("Executing ... ");
		try{
			Contact contact = processAction(action, sipPset);
			//check action type

			Integer type = (Integer)action.get(ACTION_TYPE);

			Integer noAnswerTime = (Integer) action.get("no-answer-timer");
//			if( noAnswerTime==null ){
//				noAnswerTime = new Integer(10);
//			}

			Integer noActivityLeg = (Integer) action.get("no-activity-downstreamleg");



			Integer maxCallDuration = (Integer) action.get("max-call-duration");
			if( maxCallDuration!=null ){
				this.callUser.setMaxCallDuration(maxCallDuration);
			}


			if( contact!=null){
				if( type!=3) {
					if (ringingTones != null && ringingTones.size() > 0) {
						setRingingTones(ringingTones, sipPset, contact);
					}
					_log.debug("Forward call to:{}", contact);

					this.callUser.setPendingForwardCall();

					if( contact instanceof TerminalContact){
						sipPset.setHeaderRulesVariableValue(JsonSCIFResponse.HeaderVar_NewRequestURI, ((TerminalContact)contact).getAddress().toString());
					}
					call.forwardCall(contact);
					if( noActivityLeg!=null ){
						_log.info("Enabling no-activity timer ...");
						NoAnswerTimerListener noAnswerTimerListener = new NoAnswerTimerListener(call,this.callUser,true );
						Timer timer = TelecomUtils.createTimer(noAnswerTimerListener, noActivityLeg*1000,
								this.callUser.getApplicationSession(),
								TimerInformation.TIMER_TYPE.ASYNCHRONOUS_CALL);
					}

					if( noAnswerTime!=null ){
						_log.info("Enabling no-answer timer ...");
						NoAnswerTimerListener noAnswerTimerListener = new NoAnswerTimerListener(call,this.callUser,false );
						Timer timer = TelecomUtils.createTimer(noAnswerTimerListener, noAnswerTime*1000,
								this.callUser.getApplicationSession(),
								TimerInformation.TIMER_TYPE.ASYNCHRONOUS_CALL);
					}


				} else if (type==3){
					_log.debug("Calling leaveCall to: {}",contact.toString());

					sipPset.selectHeaderRulesSet("SipServiceSpecificRulesSet");
					if( contact instanceof TerminalContact){
						sipPset.setHeaderRulesVariableValue(JsonSCIFResponse.HeaderVar_NewRequestURI, ((TerminalContact)contact).getAddress().toString());
					}

					_log.debug("Header rules, selecting SipServiceSpecificRulesSet set proxyRURI:{}",contact.toString());

					call.leaveCall(Cause.DONE,contact);
				}
			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
		_log.debug("Executing ... - DONE");
	}
}
